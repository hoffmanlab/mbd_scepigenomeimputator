#!/usr/bin/env bash
set -o nounset -o pipefail -o errexit

FTP_NCBI_AGP_URL=ftp://ftp.ncbi.nlm.nih.gov/genomes/Homo_sapiens/Assembled_chromosomes/agp/
AGP_BASE_CHR_NAME=hs_ref_GRCh38.p7_chr # e.g. 1.agp.gz
OUTPUT_DIR=input_data/assembly
mkdir -p $OUTPUT_DIR

# Get all chromosome AGPs
# Filter out lines starting with '#' (comments
# Concatenate all files into one gzipped agp file
# curl ${FTP_NCBI_AGP_URL}${AGP_BASE_CHR_NAME}{{1..22},{X,Y}}.agp.gz \
#     | zcat \
#     | sed '/^#/d' \
#     | gzip > $OUTPUT_DIR/hs_ref_GRCh38.p7.agp.gz

wget ${FTP_NCBI_AGP_URL}${AGP_BASE_CHR_NAME}{{1..22},{X,Y}}.agp.gz
# rename "s/${AGP_BASE_CHR_NAME}//" *agp.gz
# find -name "*agp.gz" | sed 's;\./\(.*\(chr.*\)\);\1 \2;' | xargs -L 1 mv
mv *.agp.gz ${OUTPUT_DIR}
