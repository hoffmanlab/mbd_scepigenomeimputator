# 2017-10-04 Analysis
Three files of three different cell lines from ENCODE: 

- ENCFF001WNL: K562 cells from https://www.encodeproject.org/experiments/ENCSR000EPC/ 

- ENCFF771DML: H1-HESC cells from https://www.encodeproject.org/experiments/ENCSR000EMU/ 

- ENCFF653HTC: GM12878 cells from https://www.encodeproject.org/experiments/ENCSR000EMT/ 

All from John Stamatoyannopoulos, UW lab, part of the same project (ENCODE). K562 has an audit warning "Insufficient Read Length/Depth" 

Using this as part of my analysis to limit called scATAC peaks to see clustering
