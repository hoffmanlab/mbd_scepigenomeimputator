# CAGE Peaks
A symlink to where I'm keeping the CAGE peaks from the other analysis 

The peaks come from http://fantom.gsc.riken.jp/5/datafiles/latest/extra/CAGE_peaks/ 

The FANTOM5 annotation files come from the FANTOM5 biomart annotation: 

1) http://biomart.gsc.riken.jp/ 

2) Click on one of the "Samples" links on the left 

3) Select all Attributes 

4) Select Go 

5) Click download data at the top right 

6) Repeat 2-5 for the other fantom5 phase 

The human sample ontologies was obtained from http://fantom.gsc.riken.jp/5/datafiles/latest/basic/HumanSamples2.0.sdrf.xlsx 

I also e-mailed FANTOM about trying to get ENCODE biosample_term_name for these samples, but was only given ff-phase2-170801.obo.txt, which isn't too helpful

## From the FANTOM5 Readme:
~~~
hg19.cage_peak_phase1and2combined_coord.bed.gz - CAGE peaks coordinates for human samples
hg19.cage_peak_phase1and2combined_ann.txt.gz - CAGE peak based table with annotated peaks for human samples
hg19.cage_peak_phase1and2combined_tpm.osc.txt.gz - CAGE peak based expression table (RLE normalized) for human samples [see edgeR documentation for further details about normalization method]
hg19.cage_peak_phase1and2combined_counts.osc.txt.gz - CAGE peak based expression table for human samples
hg19.cage_peak_phase1and2combined_tpm_ann.osc.txt.gz - CAGE peak based expression table (RLE normalized) for human with annotation
hg19.cage_peak_phase1and2combined_counts_ann.osc.txt.gz - CAGE peak based expression table for human with annotation
hg19.cage_peak_phase1and2combined_relative_expr.txt.gz - CAGE peak based relative expression (log10 expression relative to median) for human
~~~

### hg19.cage_peak_phase1and2combined_tpm_ann.osc.txt.gz
The tpm file has 1836 columns (one per sample), but make sure to pass sep="\t" when loading it in R 

It has 203642 rows, 1836 of which are comments describing the sample (These samples are described in the header comments at the top) 

I've separated the comments into a new file, header.txt

This takes 5GB of ram to load into R, so plan accordingly 

### Parsing the data

1) First combined both fantom5 phases via cat (cat fantom5-phase1.txt fantom5-phase2.txt >fantom5-combined.tsv)

2) then used parser.R to combine it with the human sample ontologies (HumanSamples2.0.sdrf.tsv) 

Which resulted in fantom5-mapped.tsv
