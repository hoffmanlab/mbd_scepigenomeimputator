# ChIP-seq stuff
All of the files were downloaded from ENCODE, and renamed as to their contents 

The file naming scheme is important for later downstream processing 

A549_RNAall.bigWig was created via 
~~~
bigWigMerge A549_RNAminus.bigWig A549_RNAplus.bigWig A549_RNAall.bigWig
~~~
