#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

while read url; do
    OUT=`basename $url`
    wget $url -O $OUT
done <to_download_FetalKidney.tsv
