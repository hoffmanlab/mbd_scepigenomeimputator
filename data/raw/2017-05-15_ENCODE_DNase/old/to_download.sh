#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

while read i; do
    wget $i
done <to_download.txt
