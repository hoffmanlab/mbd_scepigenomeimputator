#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

#python to_download.py --file_type bigWig --allow-archived to_download.json experiment_mapping.tsv >download_url.txt

# Take unique entries from experiment_mapping.tsv
sort -u -k2,2 experiment_mapping.tsv >experiment_mapping2.tsv
mv experiment_mapping2.tsv experiment_mapping.tsv
#wget -nc --random-wait --wait=60 -i download_url.txt


