#!/usr/bin/env python
import argparse
import json
import os.path
import requests
import sys

import pandas

# CONSTANTS ####################################################################
# Various special words used to navigate through the input .json
ENCODE_TOP_LEVEL = '@graph'
ENCODE_DATASET = 'accession'
ENCODE_BIOSAMPLE = 'biosample_term_name'
ENCODE_BIOSAMPLE_SYNONYMS = 'biosample_synonyms'
BIOSAMPLE_SEP = '|'

# Where to download files from
ENCODE_URL = 'https://www.encodeproject.org/'

# PARSE ARGS ###################################################################
def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Parses file names from ENCODE json files. For a description of
    the fields in the json files (including options for the arguments below)
    see https://www.encodeproject.org/help/getting-started/status-terms/
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('json', help='Input ENCODE experiment json file')
    parser.add_argument('out', help='Output summary to write')
    parser.add_argument(
        '--assembly',
        default='hg19',
        help='Genome assembly to cut down to'
        )
    parser.add_argument(
        '--file_type',
        default='bed narrowPeak',
        help='Output file type to search for (bigBed narrowPeak, bednarrowPeak, etc.)'
        )
    parser.add_argument(
        '--allow-archived',
        action='store_true',
        help='Whether or not to allow archived files to be matched'
        )
    parser.add_argument(
        '--summary-only',
        action='store_true',
        help='Disables downloading matching files'
        )
    return vars(parser.parse_args())

# MAIN #########################################################################
def main():
    vars = parse_args()
    experiments = None
    with open(vars['json']) as in_file:
        experiments = json.load(in_file)

    with open(vars['out'], 'w') as out_file:
        out_file.write("dataset\tcell.line\texperiment\n")
        for experiment in experiments[ENCODE_TOP_LEVEL]:
            dataset = experiment[ENCODE_DATASET]

            # Combine the biosample and its synonyms for maximum mappability
            biosample = experiment[ENCODE_BIOSAMPLE]
            biosample_synonyms = experiment[ENCODE_BIOSAMPLE_SYNONYMS]
            if len(biosample_synonyms) > 0:
                biosample = biosample + "|" + BIOSAMPLE_SEP.join(biosample_synonyms)

            for data_file in experiment['files']:
                # Ensure that this is the type of file the user is searching for
                if 'assembly' not in data_file or data_file['assembly'] != vars['assembly']:
                    continue
                if ('file_type' not in data_file or
                        data_file['file_type'] != vars['file_type']):
                    continue
                if (data_file['status'] != 'released' and
                        not (vars['allow_archived'] and
                        data_file['status'] == 'archived')):
                    continue

                # Download files if prompted to
                if not vars['summary_only']:
                    filename = data_file['href'].split('/')[-1]
                    url = data_file['href']
                    if not url.startswith('http'):
                        url = ENCODE_URL + url

                    print(url)
                    ## Don't redownload files
                    #if not os.path.isfile(filename):
                    #    response = requests.get(
                    #            url,
                    #            stream=True
                    #            )
                    #    response.raise_for_status()
                    #    with open(filename, 'wb') as download:
                    #        for block in response.iter_content(1024):
                    #            download.write(block)

                out_file.write("\t".join([dataset, biosample, data_file['accession']]))
                out_file.write("\n")

if __name__ == "__main__":
    sys.exit(main())

