# ENCODE DNase-seq files

## old/
Contains DNase-seq files obtained... somehow. Since I'm missing the URL that generated the to_download.txt file in this folder, I have no idea what my search terms were

## new/
URL used to generate the files is:
~~~
https://www.encodeproject.org/search/?type=Experiment&assay_title=DNase-seq&limit=all&replicates.library.biosample.donor.organism.scientific_name=Homo+sapiens&files.file_type=bigWig&format=json&frame=embedded
~~~

The file URLs are first downloaded to a .txt file, then I use ```wget -nc --random-wait --wait=60 -i <download_urls.txt>``` to download them
