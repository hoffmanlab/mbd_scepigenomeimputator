segway_gm12878.bed is the Segway annotation for GM12878, in a .bed format. This is the _training output_ of my data
ENCFF573QZL.bigWig and ENCFF573QZL.bedGraph are single cell RNA-seq runs for a GM12878 control. This is one of the _two types_ of _training input_ of my data
