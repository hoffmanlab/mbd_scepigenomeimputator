# scRNA-Seq data (aggregated)
https://www.encodeproject.org/experiments/ENCSR000AJG/
Signal of all reads, biological replicate 2

# scRNA-Seq data (individual)
https://www.encodeproject.org/experiments/ENCSR767SOH/
ENCFF382FEN: Signal of all reads, actually a single cell this time
ENCFF614KHA: transcript quantifications
