#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

# Parse through the really strange naming scheme

ROWS=`cut -f 2 validation_samples.tsv`
for i in ${ROWS[@]}; do
    SAMPLE_ID=`grep "$i" mapping.tsv | cut -f 1`
    SAMPLE_ID=${SAMPLE_ID##0}
    cd samples_of_interest
    ln -s ../fantom5_elasticnet/fantom5_elasticnet.${SAMPLE_ID}.csv ${i}.csv
    cd ..
done
