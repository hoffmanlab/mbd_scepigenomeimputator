Downloaded all of the predicted networks from http://yiplab.cse.cuhk.edu.hk/jeme/fantom5\_elasticnet.zip
Citation: http://dx.doi.org/10.1038/ng.3950 Reconstruction of enhancer-target networks in 935 samples of human primary cells, tissues and cell lines Nature Genetics 49(10):1428-1436, (2017)

There's no mapping file, so I've just copied and pasted the table from the website into mapping.tsv
