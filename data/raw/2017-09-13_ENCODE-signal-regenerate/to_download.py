#!/usr/bin/env python
import argparse
import json
import sys

import requests

# CONSTANTS ###################################################################
# Various special words used to navigate through the input .json
ENCODE_TOP_LEVEL = '@graph'
ENCODE_DATASET = 'accession'
ENCODE_BIOSAMPLE = 'biosample_term_name'
ENCODE_BIOSAMPLE_SYNONYMS = 'biosample_synonyms'
BIOSAMPLE_SEP = '|'

# Where to download files from
ENCODE_URL = 'https://www.encodeproject.org/'


def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Parses file names from ENCODE json files. For a description of
    the fields in the json files (including options for the arguments below)
    see https://www.encodeproject.org/help/getting-started/status-terms/
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('json', help='Input ENCODE experiment json file')
    parser.add_argument('out', help='Output summary to write')
    parser.add_argument(
        '--assembly',
        default='hg19',
        help='Genome assembly to cut down to'
        )
    parser.add_argument(
        '--output_type',
        default='signal',
        help='Output file type to search for (signal, accession, etc.)'
        )
    parser.add_argument(
        '--allow-archived',
        action='store_true',
        help='Whether or not to allow archived files to be matched'
        )
    parser.add_argument(
        '--summary-only',
        action='store_true',
        help='Disables downloading files'
        )
    parser.add_argument(
        '--clobber',
        action='store_true',
        help='Where a file would be downloaded but already exists, redownload it'
        )
    return vars(parser.parse_args())


def main():
    """
    Script entry point
    """
    args = parse_args()
    experiments = None
    with open(args['json']) as in_file:
        experiments = json.load(in_file)

    with open(args['out'], 'w') as out_file:
        out_file.write("dataset\tcell.line\texperiment\n")
        for experiment in experiments[ENCODE_TOP_LEVEL]:
            dataset = experiment[ENCODE_DATASET]

            # Combine the biosample and its synonyms for maximum mappability
            biosample = experiment[ENCODE_BIOSAMPLE]
            biosample_synonyms = experiment[ENCODE_BIOSAMPLE_SYNONYMS]
            if biosample_synonyms:
                biosample = biosample + "|" + BIOSAMPLE_SEP.join(biosample_synonyms)

            for data_file in experiment['files']:
                # Ensure that this is the type of file the user is searching for
                if 'assembly' not in data_file or data_file['assembly'] != args['assembly']:
                    continue
                if ('output_type' not in data_file or
                        data_file['output_type'] != args['output_type']):
                    continue
                if (data_file['status'] != 'released' and
                        not (args['allow_archived'] and
                             data_file['status'] == 'archived')):
                    continue

                # Download if we can
                if not args['summary_only']:
                    filename = data_file['href'].split('/')[-1]

                    response = requests.get(
                        ENCODE_URL + data_file['href'],
                        stream=True
                        )
                    response.raise_for_status()
                    with open(filename, 'wb') as download:
                        for block in response.iter_content(1024):
                            download.write(block)

                out_file.write("\t".join([dataset, biosample, data_file['accession']]))
                out_file.write("\n")

if __name__ == "__main__":
    sys.exit(main())
