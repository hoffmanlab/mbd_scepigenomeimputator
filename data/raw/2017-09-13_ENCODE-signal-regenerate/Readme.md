Files here were obtained from parsing the .json file obtained via:
https://www.encodeproject.org/search/?type=Experiment&assay_title=DNase-seq&limit=all&replicates.library.biosample.donor.organism.scientific_name=Homo+sapiens&files.file_type=bigWig&format=json&frame=embedded

Files sorted afterwards with
~~~
sort -k2,2 -k1,1 -k3,3 experiment_mapping.tsv >experiment_mapping_sorted.tsv
~~~

Manually curated the sorted file to only be one experiment per cell type
