#!/usr/bin/env python
import argparse
import json
import requests
import sys

import pandas

# CONSTANTS ####################################################################
# Various special words used to navigate through the input .json
ENCODE_TOP_LEVEL = '@graph'
ENCODE_DATASET = 'accession'
ENCODE_BIOSAMPLE = 'biosample_term_name'
ENCODE_BIOSAMPLE_SYNONYMS = 'biosample_synonyms'
BIOSAMPLE_SEP = '|'

# Where to download files from
ENCODE_URL = 'https://www.encodeproject.org/'

# PARSE ARGS ###################################################################
def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Parses file names from ENCODE json files. For a description of
    the fields in the json files (including options for the arguments below)
    see https://www.encodeproject.org/help/getting-started/status-terms/
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('json', help='Input ENCODE experiment json file')
    parser.add_argument('out', help='Output summary to write')
    parser.add_argument(
            'accessions',
            help='accessions to search for',
            nargs='+'
            )
    parser.add_argument(
        '--assembly',
        default='hg19',
        help='Genome assembly to cut down to'
        )
    parser.add_argument(
        '--output_category',
        default='signal',
        help='Output file type to search for (signal, accession, etc.)'
        )
    parser.add_argument(
        '--allow-archived',
        action='store_true',
        help='Whether or not to allow archived files to be matched'
        )
    parser.add_argument(
        '--summary-only',
        action='store_true',
        help='Disables downloading matching files'
        )
    return vars(parser.parse_args())

# MAIN #########################################################################
def print_summary(experiment, out_file):
    """
    Print out a summary of this experiment
    """
    out_file.write("{}:\n".format(experiment[ENCODE_DATASET]))

    for data_file in experiment['files']:
        if 'assembly' in data_file:
            assembly = data_file['assembly']
        else:
            assembly = 'unknown'
        output_category = data_file['output_category']
        #status = data_file['status']
        accession = data_file['accession']
        href = data_file['href']
        out_file.write("\t{}\t{}\t{}\t{}\n".format(assembly, output_category, accession, href))
    out_file.write("\n")


def main():
    vars = parse_args()
    experiments = None
    with open(vars['json']) as in_file:
        experiments = json.load(in_file)

    with open(vars['out'], 'w') as out_file:
        out_file.write("dataset\tcell.line\texperiment\n")
        for experiment in experiments[ENCODE_TOP_LEVEL]:
            dataset = experiment[ENCODE_DATASET]

            # Combine the biosample and its synonyms for maximum mappability
            biosample = experiment[ENCODE_BIOSAMPLE]
            biosample_synonyms = experiment[ENCODE_BIOSAMPLE_SYNONYMS]
            if len(biosample_synonyms) > 0:
                biosample = biosample + "|" + BIOSAMPLE_SEP.join(biosample_synonyms)

            for data_file in experiment['files']:
                # Ensure that this is the type of file the user is searching for
                if 'assembly' not in data_file or data_file['assembly'] != vars['assembly']:
                    continue
                if ('output_category' not in data_file or
                        data_file['output_category'] != vars['output_category']):
                    continue
                if (data_file['status'] != 'released' and
                        not (vars['allow_archived'] and
                        data_file['status'] == 'archived')):
                    continue

                if data_file['accession'] in vars['accessions']:
                    # Print out a summary of all files in this location
                    print_summary(experiment, out_file)

if __name__ == "__main__":
    sys.exit(main())

