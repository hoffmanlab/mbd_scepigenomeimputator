#!/bin/bash
#$ -q download.q
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

while read p; do
    wget --wait=60 http://egg2.wustl.edu/roadmap/data/byFileType/signal/unconsolidated/foldChange/$p
done <to_download.txt
