#!/bin/bash
#$ -q download.q
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

while read p; do
    wget http://egg2.wustl.edu/roadmap/data/byFileType/peaks/unconsolidated/narrowPeak/$p
done <to_download.txt
