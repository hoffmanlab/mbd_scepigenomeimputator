#!/usr/bin/env python
import argparse
import os.path
import sys

import json

# PARSE ARGS ###################################################################
def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Grabs a listing of file name to
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "in_file",
        nargs = 1,
        help = """IHEC metadata file to parse"""
        )
    parser.add_argument(
        "out_file",
        nargs = 1,
        help = """Output file to write to"""
        )
    return vars(parser.parse_args())

# MAIN #########################################################################
def grab_data_basenames(browser):
    """Grabs a list of all big_data_url attributes from the 'browser' dict"""
    out = []
    for signal_type in browser.values():
        for item in signal_type:
            out.append(os.path.basename(item['big_data_url']))
    return out

def main():
    vars = parse_args()

    raw_metadata = {}
    with open(vars["in_file"][0], 'r') as in_file:
        raw_metadata = json.load(in_file)

    # Navigate the list of datasets
    with open(vars["out_file"][0], 'w') as out_file:
        for dataset in raw_metadata["datasets"].values():
            cell_type = dataset["ihec_data_portal"]["cell_type"]
            assay_type = dataset["ihec_data_portal"]["assay_type"]
            files = grab_data_basenames(dataset["browser"])
            # Write out each sample/filename pair
            for file in files:
                out_file.write("{}\t{}\n".format(cell_type, file))


if __name__ == "__main__":
    sys.exit(main())

