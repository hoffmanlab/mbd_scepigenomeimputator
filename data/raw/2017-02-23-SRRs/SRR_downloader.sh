#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

IN_NAME='SRR_list.txt';

while read filename; do
    wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByStudy/sra/SRP/SRP052/SRP052977/${filename}/${filename}.sra;
done < $IN_NAME
