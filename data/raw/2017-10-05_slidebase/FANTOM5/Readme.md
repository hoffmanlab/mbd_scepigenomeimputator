# Slidebase pre-defined tracks
http://slidebase.binf.ku.dk/human_enhancers/presets
http://slidebase.binf.ku.dk/human_enhancers/presets/serve/facet_expressed_enhancers.tgz expressed enhancers in all facets 
http://slidebase.binf.ku.dk/human_enhancers/presets/serve/facet_differentially_expressed_0.05.tgz Download enhancers that are positively differentially expressed in each facet against any other facet
http://slidebase.binf.ku.dk/human_enhancers/presets/serve/hg19_permissive_enhancer_usage.csv.gz Binary matrix of enhancer usage (significant enhancer expression compared to random regions, 1 means used) across all considered FANTOM libraries
http://slidebase.binf.ku.dk/human_enhancers/presets/serve/hg19_permissive_enhancers_expression_rle_tpm.csv.gz Expression (TPM and RLE normalized) matrix of enhancers across all considered FANTOM libraries 

# FANTOM5 Andersson et al. paper
The Cell line / organ identifiers are found in Table S10 and Table S11 from the Andersson et al. paper (doi:10.1038/nature12787). I've converted them to csv files here
