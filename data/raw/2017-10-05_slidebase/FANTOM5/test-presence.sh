#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

# Determine whether or not every coordinate in the input bed file appears in the target bed file
IN_FILE=${1:-facet_expressed_enhancers/UBERON:0000945_stomach_expressed_enhancers.bed}
TEST_FILE=${2:-permissive_enhancers}

cut -f 1-3 $IN_FILE | while read p; do
    grep -L "$p" $TEST_FILE
done
