# RNA-seq data
Downloaded from https://www.encodeproject.org/search/?type=Experiment&assay\_title=polyA+RNA-seq&replicates.library.biosample.donor.organism.scientific\_name=Homo+sapiens&limit=all&files.file\_type=bigWig&format=json&frame=embedded

File was processed with 

`python to_download.py --output_category signal --allow-archived --summary-only to_download.json experiment_mapping_raw.tsv` 

Raw file was trimmed with 

`sort -u -k2,2 experiment_mapping_raw.tsv >experiment_mapping.tsv` 

Then trimmed by hand to the cell types whose data we have. 

Then I grabbed the ones which did have it, and converted it to download\_urls.txt using 

`awk -F "\t" '(NF==3){print "https://www.encodeproject.org/files/" $3 "/@@download/" $3 }' experiment_mapping_trim.tsv`
