to\_download obtained from running find-similar.py on ../2017-09-13\_ENCODE-signal-regenerate/ for the accession numbers that we've been using in this analysis (i.e. everything in that directory)

I've deleted the files in this directory once it became clear I could use my old bedGraph files if I used [this script](https://github.com/daler/chromhmm-tools/blob/master/chromhmm_signalize.py)
