# FANTOM5 Enhancer Dataset
This dataset is FANTOM5 enhancers obtained from http://fantom.gsc.riken.jp/5/datafiles/latest/extra/Enhancers/ 

## Preprocessing
Find the column you want by
`gzip -cd human_permissive_enhancers_phase_1_and_2_expression_count_matrix.txt.gz | head >tmp.txt`
Then load up R:
    > a <- read.table("tmp.txt", header=TRUE)
    > which(rownames(a) == "CNhsxxxxx")
Then you can place that number in the cut command below:
`gzip -cd human_permissive_enhancers_phase_1_and_2_expression_count_matrix.txt.gz | cut -f 1,1507 | gzip - >CNhs11275_matrix.txt.gz`
Then run preprocess.R
