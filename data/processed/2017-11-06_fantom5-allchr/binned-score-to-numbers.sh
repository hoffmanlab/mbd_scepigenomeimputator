#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

#module load R/3.3.0

# Input args
IN_DIR=(${1:-binned-score-reparsed-evenhundred/}/*.gz)
OUT_DIR=${2:-binned-score-evenhundred-0index/}
IDX=$(($SGE_TASK_ID - 1))

IN_FILE=${IN_DIR[$IDX]}
OUT_FILE=`basename $IN_FILE`
OUT_FILE=$OUT_DIR/$OUT_FILE

# Substitute to zero-based index
Rscript binned-score-to-numbers.R --input $IN_FILE --output $OUT_FILE
