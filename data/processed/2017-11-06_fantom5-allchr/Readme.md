The binned-data run failed -- will need to rerun 

Removing the following intermediate directories for space: 

- binned-score-old/

- binned-score-raw/

- individual-runs-old/ 

- individual-runs/ 

- sorted-bed/ 

- windowed-data/

# all-facet-merged
all-facet-merged.bed is an attempt to figure out what possible enhancers there are within my input dataset
cat all facet files that have an entry within mappable_wide.tsv

raw-score is the result of parsing out the CAGE counts from hg19.cage_peak_phase1and2combined_counts_ann.osc.txt.gz

1) Rerun windower-fantom5.sh on raw-score/ to get binned-score-evenhundred/

2) Run reparse-score-raw.sh on binned-score-evenhundred/
