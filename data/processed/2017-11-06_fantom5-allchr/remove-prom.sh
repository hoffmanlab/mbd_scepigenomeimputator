#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

IN_DIR=${1:-binned-score-reparsed-evenhundred}
OUT_DIR=${2:-binned-score-evenhundred-noprom}

# Redo the binned-score-evenhundred-0index but remove all promoter regions to turn this into an easier problem to solve
for i in $IN_DIR/*; do
    echo $i
    J=$OUT_DIR/`basename $i`
    gzip -cd $i | sed 's/2_Transcribed/1_Quiescent/' | gzip - >"$J"

    #zgrep -P "[^1]$" $i | gzip -c - >$OUT_DIR/$J
done
