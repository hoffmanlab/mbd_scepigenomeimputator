#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

MAPPABLE_FILE=${1:-"mappable_wide.tsv"}
FACET_DIR=${2:-"facet_expressed_enhancers"}
OUT_FILE=${3:-"all-facet-merged.bed"}
TEST=(`cut -f5 $MAPPABLE_FILE`)

for i in ${TEST[@]}; do
    if [ -e $FACET_DIR/$i ]; then
        cat $FACET_DIR/$i >>$OUT_FILE
    fi
done

sort -k1,1 -k2,2n $OUT_FILE >${OUT_FILE}.tmp
bedtools merge -c 4,5,6,7,8,9,10,11 -o distinct -delim "|" -i ${OUT_FILE}.tmp >$OUT_FILE
rm ${OUT_FILE}.tmp
