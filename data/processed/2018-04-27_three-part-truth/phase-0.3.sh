#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l h_vmem=16G -l mem_requested=16G

# verbose failures
set -o nounset -o pipefail -o errexit

# This script creates the phase0.3 files from the phase0.3
# Phase0.3: Since there's a Gaussian-shaped peak in the DNase data
# We're going to look at whether or not we can define "flanking" regions
# that are indicative of enhancers/promoters

ID=$((SGE_TASK_ID - 1))

IN_DIR=(${1:-"phase0.2"}/*.gz)
OUT_DIR=${2:-"phase0.3"}
GENOME_FILE=${3:-"hg19.trim.chrom.sizes"}

IN_FILE=${IN_DIR[$ID]}
BASENAME=$(basename "$IN_FILE")

# Generate the flanking regions
gzip -cd $IN_FILE | grep -P "[^0]$" >tmp.${BASENAME}.bed
bedtools flank -i tmp.${BASENAME}.bed -g $GENOME_FILE -l 300 -r 300 >tmp.${BASENAME}.flanking.bed
bedtools subtract -a  tmp.${BASENAME}.flanking.bed -b tmp.${BASENAME}.bed >tmp.${BASENAME}.flank-nooverlap.bed
awk -F"\t" 'BEGIN { OFS = "\t" } {$4="4"; if ($3 % 100 == 0) {$3= $3-1}; if ($2 % 100 == 99) {$2=$2+1}; if ($2 < $3) {print}}' tmp.${BASENAME}.flank-nooverlap.bed >tmp.${BASENAME}.flank-fixed.bed

# Merge with original dataset, take lowest position for each overlap
gzip -cd $IN_FILE | cat tmp.${BASENAME}.flank-fixed.bed - | sort -k1,1 -k2,2n - >tmp.${BASENAME}.flank-mixed.bed
bedtools merge -i tmp.${BASENAME}.flank-mixed.bed -c 4 -o max | sed 's/0\.5/4/' | gzip - >$OUT_DIR/$BASENAME
rm tmp.${BASENAME}.flank-mixed.bed tmp.${BASENAME}.flank-fixed.bed tmp.${BASENAME}.flank-nooverlap.bed tmp.${BASENAME}.flanking.bed tmp.${BASENAME}.bed
