#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

# The files in this directory were done via windower.sh, not this script

for i in *.bed.gz; do
    base=${i%%.gz}
    #bedtools merge -i $i >${i}.tmp
    mv ${i} $base
    gzip $base
done

