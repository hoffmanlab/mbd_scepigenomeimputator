#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

module load bedtools;

#BULK_FILES=(GM12878-bulk.bed.gz H1hESC-bulk.bed.gz K562-bulk.bed.gz) 
SC_INDEXES=(scRuns/gm12878-samples-trunc.txt scRuns/H1Esc-samples-trunc.txt scRuns/K562-samples-trunc.txt)
OUT_DIRS=(GM12878 H1hESC K562)
BED_DIR="scRuns/bed/"

for i in ${!SC_INDEXES[@]}; do
    OUT_DIR=${OUT_DIRS[$i]}
    mkdir -p $OUT_DIR
    while IFS= read -r run; do
        bedtools intersect -a all-merged.bed -b $BED_DIR/$run.bed -u >$OUT_DIR/$run.bed
    done <${SC_INDEXES[$i]}
done
