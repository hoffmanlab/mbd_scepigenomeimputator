truncate.sh will grab the scRuns and compare them to all-sorted.bed

```
gzip -cd GM12878-bulk.bed.gz >>all-peaks.bed
gzip -cd H1hESC-bulk.bed.gz >>all-peaks.bed 
gzip -cd K562-bulk.bed.gz >>all-peaks.bed   
sort -k1,1 -k2,2n all-peaks.bed >all-sorted.bed
bedtools merge -i all-sorted.bed >all-merged.bed
```
