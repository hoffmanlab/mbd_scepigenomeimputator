#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

for i in ../binned-data/*.gz; do
    BASE=`basename $i`;
    gzip -cd $i | grep -P "chr17\t" | gzip - >$BASE
done
