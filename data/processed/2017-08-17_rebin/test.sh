#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

for i in *.bed; do
    mv $i ${i}.gz
done
