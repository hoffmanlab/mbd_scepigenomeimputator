# Converting the .bigWig signal files into bedGraphs so I can work with them
Redoing my analysis but with read-depth normalized data, which wasn't available when I started this analysis.

For ease of use, I've taken my old processing scripts and prefixed them with a letter so you know what order to run them in (a-z). I had to use letters instead of digits because SGE won't run scripts prefixed with numbers. 

I've copied and pasted the old mappable.tsv, and changed the chromatin accessibility column to my new data.
