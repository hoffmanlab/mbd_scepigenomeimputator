#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

# bin the mappable data, renaming it to something sane
MAPPABLE=${1:-mappable_new.tsv} 
IN_DIR=${2:-mappable-data} 
OUT_DIR=${3:-bedgraph-data} 

ID=$(( ${SGE_TASK_ID:-1} ))

IN_FILE=`sed "${ID}"'!d' $MAPPABLE | cut -f 3`
IN_FILE=${IN_FILE%%.bedGraph.gz}.bigWig
OUT_FILE=`sed "${ID}"'!d' $MAPPABLE | cut -f 1`.bedGraph

module load ucsctools/315

bigWigToBedGraph $IN_DIR/$IN_FILE $OUT_DIR/$OUT_FILE
gzip $OUT_DIR/$OUT_FILE
