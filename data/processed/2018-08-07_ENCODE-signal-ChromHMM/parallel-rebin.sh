#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

ID=$SGE_TASK_ID

python chromhmm-tools/chromhmm_signalize.py rebin --binsize 100 config-line-$ID.txt hg19
