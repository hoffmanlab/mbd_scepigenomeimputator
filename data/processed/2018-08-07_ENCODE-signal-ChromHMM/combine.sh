#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

python chromhmm-tools/chromhmm_signalize.py combine --binsize 100 config.txt
