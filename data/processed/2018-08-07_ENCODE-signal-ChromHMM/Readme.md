# ChromHMM Data processing
I'm using [this script](https://github.com/daler/chromhmm-tools) from Dr. Dale in order to convert my imputed RNA, real RNA, and real DNase-seq signal into one file.

## Commands
* `for i in `seq 88`; do sed "${i}q;d" config.txt \>config-line-${i}.txt; done`

* `qsub -t 1-88 -l h_vmem=8G -l mem_requested=8G parallel-rebin.sh` 

* `python chromhmm-tools/chromhmm_signalize.py combine --binsize 100 config.txt` 

* `java -jar -xm15G ChromHMM.jar BinarizeSignal signal binarized` 

* `java -jar -xm15G ChromHMM.jar LearnModel binarized output 9 hg19`
