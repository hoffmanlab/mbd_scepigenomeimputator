#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l h_vmem=16G
#$ -l mem_requested=16G

# verbose failures
set -o nounset -o pipefail -o errexit

IN_DIR=$1
OUT_DIR=${2:-binarized}
#CELL_MARK_FILE=${2:cellmarkfile.txt}
#OUT_DIR=${3:binarized-bed/}

# Assumes we have the ChromHMM jar in ${CHROMHMM}/ChromHMM.jar
CHROM_JAR=${CHROMHMM}/ChromHMM.jar
CHROM_SIZES=${CHROMHMM}/CHROMSIZES/hg19.txt
if [ ! -f ${CHROM_JAR} ] || [ ! -f ${CHROM_SIZES} ]; then
    echo "Error: environment variable \$CHROMHMM is not set to the ChromHMM directory. This script assumes the ChromHMM.jar is located in \${CHROMHMM}/ChromHMM.jar and that \${CHROMHMM}/CHROMSIZES/hg19.txt exists"
fi

# Attempt to binarize the input file
echo java -mx10G -jar $CHROM_JAR BinarizeSignal $IN_DIR $OUT_DIR
java -mx10G -jar $CHROM_JAR BinarizeSignal $IN_DIR $OUT_DIR
#java -Xmx15G BinarizeBam -b 100 $CHROM_SIZES IN_DIR $CELL_MARK_FILE $OUT_DIR
