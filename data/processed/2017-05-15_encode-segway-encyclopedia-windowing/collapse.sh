#!/bin/bash
#$ -N collapse.sh
#$ -cwd
#$ -l h_vmem=16G
#$ -l mem_requested=16G
#$ -q hoffmangroup

# verbose failures
set -o nounset -o pipefail -o errexit

module load python3
python3 collapse.py "$@"
