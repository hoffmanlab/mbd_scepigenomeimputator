#!/usr/bin/env python
# reduce.py ####################################################################
# Reduces the columns in the given input file into a less memory-intensive form
# Used for all_scores.tsv

import argparse
import operator
import re

# CONSTANTS ####################################################################
NON_VOTER_COL = 0
NUM_ANNOTS = 10

# MAIN #########################################################################
def kernel_trapezoidal(distance, scaling):
    # Handle cases where no ATAC-Seq peak exists in the entire chromosome
    if distance < 0:
        return 0

    # Penalize regions for being too close to a disqualification region
    vote_pct = 1-(distance / scaling)
    if vote_pct > 1:
        vote_pct = 1
    return vote_pct

def kernel_rectangular(distance):
    # Disqualify regions inside of a disqualification region
    # Or if it doesn't exist in the entire chromosome
    if distance <= 0:
        return 0

    # Otherwise: full vote
    return 1

def kernel(distance, use_trapezoidal, scaling):
    """Transforms given distance to nearest non-voting region into a
    percentage of a vote
    I.E. if the input region is discordant with the cell line of interest,
    the vote decreases from a full vote to zero vote dependant upon a function
    of the distance to the concordant region, as specified by this function."""
    if use_trapezoidal:
        return kernel_trapezoidal(distance, scaling)
    return kernel_rectangular(distance)

def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Reduce columns in the given input file into a less memory
    intensive form
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('infile')
    parser.add_argument('outfile')
    kernel_group = parser.add_mutually_exclusive_group(required = True)
    kernel_group.add_argument('--trapezoidalkernel',
            action = 'store_true',
            help = 'Whether to use the trapezoidal kernel')
    kernel_group.add_argument('--rectangularkernel',
            action = 'store_true',
            help = 'Whether to use the rectangular kernel')
    parser.add_argument('--distancescaling',
            default = 5000,
            type = int,
            help = 'Trapezoidal kernel scaling distance (has no effect if rectangular kernel used). Default: 5000'
            )
    parser.add_argument('--probability',
            default = 0.5,
            type = float,
            help = 'Probability at which to accept a label - the highest probability over this limit is used, otherwise no call is made'
            )
    return vars(parser.parse_args())

def main():
    args = parse_args()

    # Convert line by line
    with open(args['infile']) as infile, open(args['outfile'], 'w') as outfile:
        for line in infile:
            to_edit = line.strip().split()

            # Keep the first two (chr, start, end)
            outfile.write("\t".join(to_edit[0:3]))

            # Collapse the columns into percentages of each annotation
            annot_col = [0]*NUM_ANNOTS
            #for annot in to_edit[3:]:
            for col_id in range(3, len(to_edit), 2):
                prob = to_edit[col_id]
                prob = kernel(int(prob), args['trapezoidalkernel'], args['distancescaling'])
                annot = to_edit[col_id+1]
                annot_col[int(annot)] += prob
                annot_col[NON_VOTER_COL] += 1-prob

            # Rescale according to how many there is
            to_scale = sum(annot_col[1:])
            if to_scale > 0:
                annot_col[1:] = [num / to_scale for num in annot_col[1:]]

            winning_label = 0
            highest_prob = max(annot_col[1:])
            if highest_prob >= args['probability']:
                winning_label = annot_col.index(highest_prob)

            for score in annot_col:
                outfile.write("\t{0}".format(str(score)))
            outfile.write("\t{0}".format(str(winning_label)))
            outfile.write("\t{0}".format(str(highest_prob)))
            outfile.write("\n")

if __name__ == '__main__':
    main()

