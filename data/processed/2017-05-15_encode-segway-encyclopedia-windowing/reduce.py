#!/usr/bin/env python
# reduce.py ###################################################################
# Reduces the columns in the given input file into a less memory-intensive form
# Used for all_scores.tsv

import argparse
import re

import pandas as pd

MAPPING = {
    ".": "0",
    "Quiescent": "1",
    "Quies": "1",
    "ConstitutiveHet": "2",
    "FacultativeHet": "3",
    "Repr": "3",
    "Transcribed": "4",
    "Gen5'": "4",
    "Gen3'": "4",
    "Gen'": "4",
    "Tss": "4",
    "TssF": "4",
    "Promoter": "7",
    "PromF": "7",
    "PromP": "7",
    "Enhancer": "7",
    "Enh": "7",
    "EnhF": "7",
    "EnhW": "7",
    "EnhWf": "7",
    "RegPermissive": "7",
    "Bivalent": "8",
    "EnhPr": "8",
    "LowConfidence": "9",
    "Low": "9",
    "Ctcf": "9",
    "CtcfO": "9",
    "Elon": "9",
    "ElonW": "9"
    }

def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Reduce columns in the given input file into a less memory
    intensive form
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('infile')
    parser.add_argument('outfile')
    return vars(parser.parse_args())

def main():
    args = parse_args()
    reducer = re.compile("\d+_([a-zA-Z']+)")
    reducer2 = re.compile("([a-zA-Z']+)\d+")

    # Convert line by line
    with open(args['infile']) as infile, open(args['outfile'], 'w') as outfile:
        for line in infile:
            to_edit = line.strip().split()

            # Keep the first three (chr, start, end)
            outfile.write("\t".join(to_edit[0:3]))

            # Map the rest of the columns (assumed to be annotations)
            for col_id in range(3,len(to_edit),2):
                probability = to_edit[col_id]
                annot = to_edit[col_id+1]
                new_annot = reducer.sub("\\1", annot)
                new_annot = reducer2.sub("\\1", new_annot)
                if new_annot not in MAPPING:
                    print("{0}:{1}-{2} has strange entry:{3}".format(to_edit[0], to_edit[1], to_edit[2], new_annot))
                    outfile.write("\t0")
                    continue
                outfile.write("\t{0}\t{1}".format(probability, MAPPING[new_annot]))
            outfile.write("\n")

if __name__ == '__main__':
    main()

