"""Finds a matching DNase-seq experiment for the given fantom5 map
Meant to be used with python3.5+ for the pathlib library"""

import argparse
import gzip
import json
import os
import pathlib
import re
import sys


def parse_args():
    """Grab command line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument('fantom5-map', help='fantom5 map file')
    parser.add_argument('encode-map', help='Encode mappability file')
    parser.add_argument('roadmap-folder', help='Roadmap Chromatin Accessibility folder')
    parser.add_argument(
        '--enable-dups',
        action = 'store_true',
        help = 'Whether or not to allow duplicate entries per cell type'
        )
    return parser.parse_args()

class DirectoryCheck:
    """Holds all of the files in a folder for easy name checking"""
    def __init__(self, folder):
        self.folder = folder
        self.files = [x.name for x in folder.iterdir() if x.is_file()]

    def is_here(self, biosample_term_name):
        """Determines if this folder has a sample matching the given
        biosample_term_name"""
        biosample_re = re.compile(
            re.escape(biosample_term_name),
            flags=re.IGNORECASE
            )
        matches = []
        for to_match in self.files:
            match = biosample_re.search(to_match)
            if match:
                matches.append(to_match)
        return matches

class EncodeCheck:
    def __init__(self, mapping_file):
        self.cell_line2file = {}
        self.parse_mapping(mapping_file)

    def parse_mapping(self, mapping_file):
        """Parses a mapping file"""
        with open(mapping_file) as mapping:
            for line in mapping:
                info = line.strip().split("\t")
                cell_lines = info[1].split("|")
                for cell_line in cell_lines:
                    if cell_line not in self.cell_line2file:
                        self.cell_line2file[cell_line] = []
                    self.cell_line2file[cell_line].append(info[2])

    def is_here(self, biosample_term_name):
        biosample_re = re.compile(
            re.escape(biosample_term_name),
            flags=re.IGNORECASE
            )
        for cell_line in self.cell_line2file.keys():
            match = biosample_re.match(cell_line)
            if match:
                return self.cell_line2file[cell_line]
        return []

def parse_fantom5(filename):
    file_content = ''
    if filename.endswith('.gz'):
        with gzip.open(filename, 'rt') as f:
            file_content = f.read()
    else:
        with open(filename, 'r') as f:
            file_content = f.read()
    file_content = file_content.strip().split("\n")

    # Parse out the header
    header_line = file_content[0].split("\t")
    col_of_interest = header_line.index("Sample.Name")
    sample_names = []
    cell_line_re = re.compile(r'cell line:\s*(.+)')
    fetal_re = re.compile(r'(.+), (fetal|adult)')
    supplementary_re = re.compile(r'(.+?),.+')
    supplementary2_re = re.compile(r'(.+) ENCODE')
    treatment_re = re.compile(r'(treated|day|\dhr| after )')   # Visual inspection
    emdash_re = re.compile(r'- ')
    final_processing_re = re.compile(' ')
    for line in file_content[1:]:
        these_samples = line.split("\t")[col_of_interest].split("|")
        invalid_lines = []
        for (i, sample) in enumerate(these_samples):
            # Delete types that underwent treatment of any kind
            treatment_match = treatment_re.search(sample)
            if treatment_match is not None:
                invalid_lines.append(i)
                continue

            # Parse out cell lines
            cell_line_match = cell_line_re.search(sample)
            if cell_line_match is not None:
                sample = cell_line_match.group(1)
                print(sample, file=sys.stderr)

            # Move adult/fetal to the front and discard the rest
            fetal_match = fetal_re.search(sample)
            if fetal_match is not None:
                sample = "{}_{}".format(
                    fetal_match.group(2),
                    fetal_match.group(1))

            # Remove donor info and supplementary notes
            supplementary_match = supplementary_re.search(sample)
            if supplementary_match is not None:
                sample = supplementary_match.group(1)
            supplementary2_match = supplementary2_re.search(sample)
            if supplementary2_match is not None:
                sample = supplementary2_match.group(1)

            # Remove emdashes-that-are-actually-just-dashes
            sample = emdash_re.sub("", sample)

            # Perform any final processing steps
            sample = final_processing_re.sub("_", sample).upper()
            these_samples[i] = sample

        for line in invalid_lines:
            del these_samples[line]
        sample_names.append(these_samples)
    return (file_content, sample_names)

def main():
    args = vars(parse_args())
    roadmap_folder = pathlib.Path(args["roadmap-folder"]).expanduser()
    roadmap_check = DirectoryCheck(roadmap_folder)
    encode_check = EncodeCheck(args["encode-map"])
    (fantom5, types) = parse_fantom5(args["fantom5-map"])
    header = "{}\tNorm.Cell.Type\tDnase.Source\tDnase.File".format(fantom5[0])
    print(header)
    for (i, sample_names) in enumerate(types):
        this_row = fantom5[i+1]
        for sample_name in sample_names:
            enc_file = encode_check.is_here(sample_name)
            road_file = roadmap_check.is_here(sample_name)
            if args['enable_dups']:
                for file in enc_file:
                    print("{0}\t{1}\tENCODE\t{2}".format(this_row, sample_name, file))
                for file in road_file:
                    print("{0}\t{1}\tROADMAP\t{2}".format(this_row, sample_name, file))
            else:
                if enc_file:
                    print("{0}\t{1}\tENCODE\t{2}".format(this_row, sample_name, enc_file[0]))
                    continue
                if road_file:
                    print("{0}\t{1}\tROADMAP\t{2}".format(this_row, sample_name, road_file[0]))
                #print("{0} could not be found".format(this_row))

if __name__ == "__main__":
    main()
