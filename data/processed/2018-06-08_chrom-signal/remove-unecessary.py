#!/usr/bin/env python
import argparse
import os
import os.path
import sys

# PARSE ARGS ###################################################################
def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Remove all bigWigs that were not actually used in the future analysis
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "downloaddir",
        help = """Folder of all downloaded .bigWigs"""
        )
    parser.add_argument(
        "mappabledir",
        help = """Folder of all used .bigWigs"""
        )
    parser.add_argument(
        "--suffix",
        default = ".bigWig",
        help = "Data file suffix"
        )
    return parser.parse_args()

# MAIN #########################################################################
def main():
    args = parse_args()
    mappable_files = os.listdir(args.mappabledir)
    downloaded_files = os.listdir(args.downloaddir)
    for downloaded_file in downloaded_files:
        # Ignore downloaded files that are not bigWigs
        if not downloaded_file.endswith(args.suffix):
            continue

        # Ignore files that ARE used in downstream analyses
        if downloaded_file in mappable_files:
            continue

        # Unlink this file
        downloaded_file_path = os.path.join(args.downloaddir, downloaded_file)
        os.remove(downloaded_file_path)

if __name__ == "__main__":
    sys.exit(main())

