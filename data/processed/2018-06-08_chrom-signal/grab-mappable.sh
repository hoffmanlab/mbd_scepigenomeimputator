#!/bin/bash
# Aggregates the results of windower.sh

# verbose failures
set -o nounset -o pipefail # -o errexit

# Move all of the files that are mappable to its own folder
MAPPING_FILE=$1 # mappable.txt

DATA_FILES=`cut -f 3 $MAPPING_FILE`
mkdir -p mappable-data
cd mappable-data
for J in $DATA_FILES; do
    f=${J%%.bedGraph.gz}.bigWig
    roadmap_f=${J%%.bedGraph.gz}.bigwig
    echo ${f}
    if [ -e ../ENCODE-signal/${f} ]; then
        ln -sf ../ENCODE-signal/${f} ./
    elif [ -e ../Roadmap-signal/${roadmap_f} ]; then
        ln -sf ../Roadmap-signal/${roadmap_f} ./${f}
    else
        echo "${f} data not found!"
    fi
done
