# Converting the .bigWig signal files into bedGraphs so I can work with them
First thing of note is that the output files are 2gigs apiece, so I need to be careful about how much space I'm using. 
For now, let's only convert the files that I need (i.e. those corresponding to the cell types I use)

These cell types have been copied from results/2017-09_fantom5/2018-05-16_whendoesitmatter
