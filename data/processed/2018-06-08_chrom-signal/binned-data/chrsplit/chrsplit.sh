#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

CHR=(`seq 2 22`)
CHR+=(X)
for i in ${CHR[@]}; do
    for IN_FN in ../*.bedGraph.gz; do
        OUT_FN=`basename $IN_FN`
        if [ "$OUT_FN" = "tongue_test.bedGraph.gz" ]; then
            continue;
        fi
        OUT_FN=${OUT_FN%%.bedGraph.gz}
        zgrep -P "chr$i\t" $IN_FN | gzip -c - >${OUT_FN}-chr$i.bedGraph.gz
    done
done
