This folder does _not_ contained binned data. I'm not sure how this misnomer occurred, but since a lot of analysis was generated with it, I'd err on the side of caution. 

This folder instead contains *bedGraphs of chromatin accessibility signal*
