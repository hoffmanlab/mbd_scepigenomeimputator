#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

for file in *-trunc.txt; do
    cellline=`echo $file | tr '[:upper:]' '[:lower:]'`
    cellline=${cellline%%-*}
    mkdir -p ${cellline}-final
    while read FILE_NAME; do
        SRR_BASE=${FILE_NAME%%.*}
        TO_SORT=final/${SRR_BASE}.final.bed
        if [ -e $TO_SORT ]; then
            cp $TO_SORT ${cellline}-final/
        else
            echo $TO_SORT missing
        fi
    done <$file
done

#mkdir k562-final
#while read FILE_NAME; do
#    SRR_BASE=${FILE_NAME%%.*}
#    mv final/$SRR_BASE.final.bed k562-final/
#done <K562-samples.txt
#
## hl60 didn't complete successfully for some reason
#mkdir hl60-final
#while read FILE_NAME; do
#    SRR_BASE=${FILE_NAME%%.*}
#    mv final/$SRR_BASE.final.bed hl60-final/
#done <HL60-samples.txt

