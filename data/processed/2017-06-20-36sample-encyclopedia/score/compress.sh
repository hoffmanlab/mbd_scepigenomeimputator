#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -t 1-150
#$ -tc 50

# verbose failures
set -o nounset -o pipefail -o errexit

FILE_TO_COMPRESS=`sed "${SGE_TASK_ID}q;d" file_list.txt`
gzip $FILE_TO_COMPRESS
