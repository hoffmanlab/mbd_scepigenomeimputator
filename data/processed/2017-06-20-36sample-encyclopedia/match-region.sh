#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l h_vmem=16G
#$ -l mem_requested=16G
#$ -N match-region
#$ -t 1-1
#$ -tc 50

# t normally 1-1145

# verbose failures
set -o nounset -o pipefail -o errexit

module load python3

IN_DIR=(test_peaks/*.500bp.bed)
IN_FILE=${IN_DIR[$SGE_TASK_ID-1]}

python3 match-region.py $IN_FILE mappable.txt mappable-score/ mappable-data/
