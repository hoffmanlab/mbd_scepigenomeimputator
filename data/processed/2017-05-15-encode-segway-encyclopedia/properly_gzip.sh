#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# Fix files from the encyclopedia aren't actually gzipped (i.e. all of them)

# verbose failures
set -o nounset -o pipefail -o errexit

for SAMPLE_FILE in segway-annot/*.bed.gz; do
    # Check that the file actually needs to be re-gzipped
    set +o errexit
    Z=`file $SAMPLE_FILE | grep "ASCII"`;
    set -o errexit

    BASENAME=`basename $SAMPLE_FILE`
    echo $BASENAME
    if [ "$Z" == "" ]; then 
        cp $SAMPLE_FILE gzipped-annot/$BASENAME;
    else
        # Fix the file ending so we can gzip it properly
        NO_GZ_NAME=gzipped-annot/${BASENAME%%.gz};
        cp $SAMPLE_FILE $NO_GZ_NAME;
        gzip $NO_GZ_NAME;
    fi
done
