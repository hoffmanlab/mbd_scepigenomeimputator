# SEGWAY / ENCODE information 

experiment-mapping.tsv maps the ENCODE files to the cell line they came from  

segway-annot/ has all of the Segway .bed annotation files from http://noble.gs.washington.edu/proj/encyclopedia/interpreted/  

encode-dnase/ has all of the ENCODE bigBed files from https://www.encodeproject.org/search/?type=Experiment&replicates.library.biosample.biosample_type=immortalized+cell+line&y.limit=&assay_title=DNase-seq&limit=all  

## Experiment mapping

Use experiment-mapping.tsv to map encode-dnase to segway-annot 

Ok so we've got an issue with mapping - apparently the ENCODE tissue/cell line names got changed a while ago and so aren't mapping cleanly to the encyclopedia names 

This is an issue
