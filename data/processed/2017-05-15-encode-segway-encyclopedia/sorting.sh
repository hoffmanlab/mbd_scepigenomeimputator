#!/bin/bash
# Sort everything inside of segway-annot
# Meant to be called from a qsub SGE taskarray

# verbose failures
set -o nounset -o pipefail -o errexit

module load bedtools/2.23.0

FILES=(segway-annot/*.bed.gz)
TO_SORT=${FILES[$SGE_TASK_ID-1]}
sortBed -i $TO_SORT >${TO_SORT}.sort
mv ${TO_SORT}.sort $TO_SORT
