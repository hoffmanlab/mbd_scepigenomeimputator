#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

# Quickly fix the missing two columns

ID=$((SGE_TASK_ID - 1))

IN_FOLDER=(${1:-imputed-rna}/*.bedGraph)

IN_FILE=${IN_FOLDER[$ID]}
TMP_FILE=${IN_FILE}.tmp

sed 's/ /\t/g' $IN_FILE | head -n -1 - >$TMP_FILE
