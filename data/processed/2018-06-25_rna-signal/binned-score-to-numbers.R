# binned-score-to-numbers.R ####################################################
description <- "Replaces the input bed file with 1_Quiescent 2_Transcribed 3_x
etc. with 0 1 2"

library(optparse)

# Parse command line arguments #################################################
if (!interactive()) {
    option.list <- list(
        make_option(
            c("-i", "--input"),
            type="character",
            default=NA,
            help="input gzipped bed file output via reparse-score.py",
            metavar="character"
            ),
       make_option(
            c("-o", "--output"),
            type="character",
            default=NA,
            help="output gzipped bed file to output to",
            metavar="character"
            )
        )

    opt.parser = OptionParser(option_list=option.list, description=description)
    opt = parse_args(opt.parser)
    label.fn <<- opt$input
    out.fn <<- opt$output
}

# Convert input ################################################################
if (endsWith(label.fn, ".gz")) {
    label.fn <- gzfile(label.fn)
}
label.file <- read.table(label.fn, stringsAsFactors=TRUE)

labels <- levels(label.file$V4)
labels <- as.integer(gsub("([0-9]+)_.+", labels, repl="\\1")) - 1
levels(label.file$V4) <- labels

if (endsWith(out.fn, ".gz")) {
    out.fn <- gzfile(out.fn)
}
write.table(
    label.file,
    out.fn,
    quote=FALSE,
    row.names=FALSE,
    col.names=FALSE,
    sep="\t"
    )
