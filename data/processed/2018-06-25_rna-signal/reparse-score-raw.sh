#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l h_vmem=8G
#$ -l mem_requested=8G

# verbose failures
set -o nounset -o pipefail -o errexit

module load python3

IN_DIR=($1/*.gz)
MAPPABLE_FILE=$2
FACET_DIR=$3    # facet_expressed_enhancers/
OUT_DIR=$4

IDX=$(($SGE_TASK_ID - 1))
IN_SCORE=${IN_DIR[$IDX]}

# Determine whether or not there's a decent match for the input file
OUT_FN=`basename $IN_SCORE`
#CNHS_ID=`echo $OUT_FN | cut -d- -f2`
SCORE_ID=${OUT_FN%%.*}
MATCH=`grep -P "${SCORE_ID}\t" $MAPPABLE_FILE | cut -f 5`

mkdir -p $OUT_DIR
python3 reparse-score.py "$IN_SCORE" "$FACET_DIR/$MATCH" "$OUT_DIR/$OUT_FN" #--include_intersect
