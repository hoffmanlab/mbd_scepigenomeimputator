- Sorted the raw-rna files into sorted-bed
- Renamed each one into renamed-data for ease of use
- intersect -sorted -a genome.100.sorted.bed -c against each of the renamed data files to get only windows that varied between all of them
- grep -vP "\t0$" to get rid of all non-variant rows
- Then ran windower with that to get all of the rows needed

# Quantile Normalization
- Performed using quantile-normalize.py  
- Use split-and-combine.sh to split my data `qsub -t 1-191 split-and-combine.sh`
- Fix the final file, because it has too many lines (it overlaps with the previous data point)
- Then use `combine.sh` to make `combined.bedGraph.gz`
- Then use `quantile-normalize.py`

# RNA imputation
- Use impute-rna.py on one job (takes ~64GB to do so)
- `python impute-rna.py norm-evenhundred-data/ mappable_all.tsv imputed-evenhundred-rna/`
- Then use `qsub -t 1-22 impute-rna-fix.sh` to fix the missing positions
