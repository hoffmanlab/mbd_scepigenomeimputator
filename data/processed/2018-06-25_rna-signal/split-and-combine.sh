#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -e e.txt
#$ -o o.txt

# verbose failures
set -o nounset -o errexit

# Combines multiple bedGraphs together, in a parallelized manner
# by doing only a few lines at a time

# WARNING: DOES NOT PROPERLY HANDLE THE LAST JOB
# I HAD TO DO IT MANUALLY

ID=$((SGE_TASK_ID - 1))

FILE_DIR=${1:-./binned-data/}
PROCESS_AT_ONCE=${2:-100000} # How many lines to process at once

START=$((ID*$PROCESS_AT_ONCE))
END=$((($ID+1)*$PROCESS_AT_ONCE))

OUTFILE=split/${ID}_TMP

FILES=($FILE_DIR/*.bedGraph.gz)
FIRST_FILE=${FILES[0]}
zcat $FIRST_FILE | head -n $END - | tail -n $((PROCESS_AT_ONCE)) - | cut -f 1,2,3 - >$OUTFILE
for file in ${FILES[@]}; do
    zcat $file | head -n $END - | tail -n $((PROCESS_AT_ONCE)) - | cut -f 4 - | paste $OUTFILE - >${OUTFILE}_2
    mv ${OUTFILE}_2 $OUTFILE
done
gzip $OUTFILE
