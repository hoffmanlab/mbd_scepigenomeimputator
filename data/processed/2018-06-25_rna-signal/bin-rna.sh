#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

# bin the mappable data, renaming it to something sane
IN_DIR=${1:-raw-rna} 
OUT_DIR=${2:-bedgraph-data} 
MAPPABLE_FILE=${3:-mappable_all.tsv}

ID=$(( ${SGE_TASK_ID:-1} - 1 ))

IN_DIR_A=($IN_DIR/*.bigWig)
IN_FILE=${IN_DIR_A[$ID]}
IN_FILE=`basename $IN_FILE`

ENSCR_CODE=${IN_FILE%%.bigWig}
OUT_FILE=`grep "$ENSCR_CODE" $MAPPABLE_FILE | cut -f 1`.bedGraph

module load ucsctools/315

bigWigToBedGraph $IN_DIR/$IN_FILE $OUT_DIR/$OUT_FILE
gzip $OUT_DIR/$OUT_FILE
