#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

module load MACS

BAM_DIR=$1
SAMPLES_FILE=$2

# Read all of the files to process
FILE_LIST=""
while read line; do
    FILE_LIST="$FILE_LIST $BAM_DIR/${line}.bam"
done <"$SAMPLES_FILE"

# Peak calling and extension
macs2 callpeak --nomodel --nolambda --keep-dup all --call-summits -t $FILE_LIST -n $SAMPLES_FILE
bedtools slop -i ${SAMPLES_FILE}_summits.bed -g hg19.chrom.sizes.txt -b 250 >${SAMPLES_FILE}.500bp.bed

# Blacklist removal
bedtools intersect -v -a ${SAMPLES_FILE}.500bp.bed -b Blacklist.bed.gz >${SAMPLES_FILE}.final.bed
