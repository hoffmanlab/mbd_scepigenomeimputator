#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

module load bedtools/2.26.0

SAMPLES_FILE=$1

FILE_NAME=`sed "${SGE_TASK_ID}q;d" $SAMPLES_FILE`

# Count the signal at each identified peak
bedtools intersect -loj -wa -c -a ${SAMPLES_FILE}.final.bed -b bam/$FILE_NAME >${FILE_NAME}.peakSignal.bed
