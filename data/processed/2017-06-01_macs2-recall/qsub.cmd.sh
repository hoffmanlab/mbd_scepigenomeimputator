#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

for i in *-trunc.txt; do
    qsub -N $i -q hoffmangroup -l h_vmem=8G -l mem_requested=8G -cwd recall.sh bam/ $i
done
