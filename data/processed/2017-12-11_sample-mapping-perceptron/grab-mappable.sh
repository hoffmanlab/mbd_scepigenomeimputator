#!/bin/bash
# Aggregates the results of windower.sh

# verbose failures
set -o nounset -o pipefail # -o errexit

# Move all of the files that are mappable to its own folder
MAPPING_FILE=$1 # mappable.txt

DATA_FILES=`cut -f 3 $MAPPING_FILE`
mkdir mappable-data
cd mappable-data
for f in $DATA_FILES; do
    echo ${f%%.bed.gz}
    if [ -e ../encode-dnase-signal/${f}.bigWig ]; then
        ln -s ../encode-dnase-signal/${f}.bigWig ./
    else
        echo "${f} data not found!"
    fi
done
