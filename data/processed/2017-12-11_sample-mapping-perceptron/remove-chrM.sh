#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

for i in *.bed; do
    grep -vP "chrM|chrY" $i >${i}.tmp
    mv ${i}.tmp $i
done
