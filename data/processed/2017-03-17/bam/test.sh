#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

module load samtools;

for bamfile in final/*.bam; do
    samtools view $bamfile | wc -l
done
