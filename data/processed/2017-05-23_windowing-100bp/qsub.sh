#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

qsub -N K562-100bp -t 1-208 -tc 35 -q hoffmangroup -cwd -l h_vmem=8G -l mem_requested=8G windower.sh 100 ATAC_bed K562-samples-trunc.txt segway_k562.bed
qsub -N H1ESC-100bp -t 1-34 -tc 15 -q hoffmangroup -cwd -l h_vmem=8G -l mem_requested=8G windower.sh 100 ATAC_bed H1Esc-samples-trunc.txt segway_h1hesc.bed
#qsub -N TF1-100bp -t 1-72 -tc 10 -q hoffmangroup -cwd -l h_vmem=8G -l mem_requested=8G windower.sh 100 ATAC_bed TF1-samples-trunc.txt 
#qsub -N GM18278-100bp -t 1-200 -tc 15 -q hoffmangroup -cwd -l h_vmem=8G -l mem_requested=8G windower.sh 100 ATAC_bed gm12878-samples-trunc.txt segway_gm12878.bed
