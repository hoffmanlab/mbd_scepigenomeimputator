#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

LAST_LINE_START=-1
while read info; do
    THIS_LINE_START=`echo $info | sed "s/chr21 \+\([0-9]\+\).\+/\\1/g"`
    if [ "$THIS_LINE_START" -eq "$LAST_LINE_START" ]; then
        echo "Repeat line: $info"
    fi
    LAST_LINE_START=$THIS_LINE_START
done <$1
