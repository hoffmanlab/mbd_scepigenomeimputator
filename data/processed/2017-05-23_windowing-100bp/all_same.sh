#!/bin/bash
# Determines if all of the mappings are the same (they really should be, but this is a double-check)

# verbose failures
set -o nounset -o pipefail #-o errexit

TO_MATCH=(*.bed_mapping.txt)
FIRST_FILE=$TO_MATCH

for file in *.bed_mapping.txt; do
    #diff $file SRR1780385.final.bed_mapping.txt
    bash -c "diff <(sort $file) <(sort $FIRST_FILE)"
    if [ $? -gt 0 ]; then
        echo in $file
    fi
done
