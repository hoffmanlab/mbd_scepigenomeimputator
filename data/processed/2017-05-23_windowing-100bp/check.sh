#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

IN_FILE=$1
OUT_FILE=$2
# See which of the samples exist after all of the requirements
echo "">$OUT_FILE
while read base; do
    file=ATAC_bed/${base}.500bp.bed
    if [ ! -e $file ]; then
        tmp=1 #echo "$file does not exist"
    else
        echo "$base" >>$OUT_FILE
    fi
done <$IN_FILE
