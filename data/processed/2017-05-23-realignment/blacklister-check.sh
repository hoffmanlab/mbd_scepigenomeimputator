#!/bin/bash

# verbose failures
set -o nounset -o pipefail

UNFILTERED=$1
FILTERED=$2
for file in $UNFILTERED/*.500bp.bed; do
    IN_BASENAME=`basename $file`
    IN_BASENAME=${IN_BASENAME%.500bp.bed*}

    diff $file filtered.peaks/${IN_BASENAME}.final.bed
    if [ "$?" -ne 0 ]; then
        echo "in $file"
    fi
done
