# scATAC-seq realignment
## Changes from 2017-04-04

1. MACS2 peak calling (use final.peaks!)
2. ENCODE blacklist (based off of (ENCFF419RSJ.bed.gz)[https://www.encodeproject.org/annotations/ENCSR636HFF/])

## MACS2 peak calling
* MACS2 called with the same parameters as the original scATAC-seq paper
* Output gets slopped to make the characteristic 500bp peaks

## ENCODE blacklist
* (Also included a summary of the things that got excluded)[./blacklist_results.txt]
