#!/bin/bash
# A scrit because I'm dumb and didn't do single peak calling like an idiot

# verbose failures
set -o nounset -o pipefail -o errexit

module load MACS
module load bedtools/2.23.0

# Input parameters
IN_DIR=($1/*.bam)
#OUT_DIR=$(cd $2; pwd)

IN_BAM="${IN_DIR[$SGE_TASK_ID-1]}"
IN_BASENAME=`basename $IN_BAM`
IN_BASENAME=${IN_BASENAME%.*}

# Single peak calling?
macs2 callpeak --nomodel --nolambda --keep-dup all --call-summits -t $IN_BAM -n $IN_BASENAME
bedtools slop -i ${IN_BASENAME}_summits.bed -g hg19.trim.chrom.sizes -b 250 >${IN_BASENAME}.500bp.bed
