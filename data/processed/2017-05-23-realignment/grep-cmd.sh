module load python3

while read i; do
    #a=`python3 grep-cmd.py tmp/${i}.aligned.sam`
    a=`grep -c "chrY" tmp/${i}.aligned.sam`
    b=`grep -c "chr19" tmp/${i}.aligned.sam`
    perl -e "\$c=$a/$b*100; printf(\"$a vs $b (%.3f %)\\n\", \$c)"
done<../2017-05-23-everything-100bp/K562-samples.txt
