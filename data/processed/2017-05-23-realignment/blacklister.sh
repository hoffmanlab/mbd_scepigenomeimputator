#!/bin/bash
# Another script because I'm dumb and forgot the blacklisting step

# verbose failures
set -o nounset -o pipefail -o errexit

IN_DIR=($1/*.500bp.bed)
BLACKLIST_FILE=$2
OUT_DIR=$3

# The intersection is quick enough that parallelizing this is detrimental
for file in $1/*.500bp.bed; do
    #IN_BED="${IN_DIR[$SGE_TASK_ID-1]}"
    IN_BED=$file
    IN_BASENAME=`basename $IN_BED`
    IN_BASENAME=${IN_BASENAME%.500bp.bed*}

    bedtools intersect -v -a ${IN_BED} -b $BLACKLIST_FILE >${OUT_DIR}/${IN_BASENAME}.final.bed
done
