#!/bin/bash
#$ -N prefilter
#$ -cwd
#$ -l h_vmem=8G
#$ -l mem_requested=8G
#$ -q hoffmangroup
#$ -t 1-920
#$ -tc 50

# Handles bowtie alignment and filtering using the same method as the Buenrostro et al. 2015 paper, "Single-cell chromatin accessibility reveals principles of regulatory variation"
# This is meant to be used on the SRA files from that paper, GSE65360

# verbose failures
set -o nounset -o pipefail -o errexit

# Load requisite modules
module load igenome-human/hg19
module load sratoolkit/2.8.0
module load bowtie2
module load picard
module load samtools
module load bamtools

# Input parameters
IN_DIR=($1/*.sra)
OUT_DIR=$(cd $2; pwd)
BLACKLIST_FILE=$3

# Figure out which SRR we're supposed to do
IN_SRR="${IN_DIR[$SGE_TASK_ID-1]}"
IN_BASENAME=`basename $IN_SRR`
IN_BASENAME=${IN_BASENAME%.*}
PICARD_JAR="$picard_dir/picard.jar"
SAM_ALIGNED="$OUT_DIR/$IN_BASENAME.aligned.sam"
SAM_FILTERED="$OUT_DIR/$IN_BASENAME.filtered.sam"
BAM_QUALITY="$OUT_DIR/$IN_BASENAME.wc.bam"
BAM_SORTED="$OUT_DIR/$IN_BASENAME.sorted.bam"
OUT_BAM=$OUT_DIR/$IN_BASENAME.bam
OUT_BED=$OUT_DIR/$IN_BASENAME.bed
OUT_METRICS=$OUT_DIR/$IN_BASENAME.metrics.txt

# Determine which 
#fastq-dump --split-files --readids --dumpbase --skip-technical -W --read-filter pass --gzip $IN_SRR
bowtie2 -x $BOWTIE2INDEX -1 fastq/${IN_BASENAME}_pass_1.fastq.gz -2 fastq/${IN_BASENAME}_pass_2.fastq.gz -S $SAM_ALIGNED -X 2000

## Remove reads mapping to chrM, chrY, chrUn
#sed '/chrM/d;/random/d;/chrUn/d;/chrY/d' <$SAM_ALIGNED >$SAM_FILTERED
#
## Filter on quality
#samtools view -bSq 31 $SAM_FILTERED >$BAM_QUALITY 
#
## Picard to remove duplicates, estimate library size
#java -jar $PICARD_JAR SortSam INPUT=$BAM_QUALITY OUTPUT=$BAM_SORTED SORT_ORDER=coordinate QUIET=TRUE
#java -jar $PICARD_JAR MarkDuplicates INPUT=$BAM_SORTED OUTPUT=$OUT_BAM METRICS_FILE=$OUT_METRICS REMOVE_DUPLICATES=true QUIET=TRUE
#
## Output the file in a more readable format
#bedtools bamtobed -i $OUT_BAM >$OUT_BED
#samtools index $OUT_BAM
#
## Peak calling and extension
#macs2 callpeak --nomodel --nolambda --keep-dup all --call-summits -t $OUT_BAM -n $IN_BASENAME
#bedtools slop -i ${IN_BASENAME}_summits.bed -g hg19.chrom.sizes -b 250 >${IN_BASENAME}.500bp.bed
#
## Blacklist removal
#bedtools intersect -v -a ${IN_BASENAME}.500bp.bed -b $BLACKLIST_FILE >${IN_BASENAME}.final.bed
