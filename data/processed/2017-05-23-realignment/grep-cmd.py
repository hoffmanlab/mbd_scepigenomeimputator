import argparse

def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(
        description = """Count Y-chromosome aligned reads that aren't in PARs
        (assumes hg19)"""
        )
    parser.add_argument(
        "in_sam",
        nargs = 1,
        help = """SAM file containing reads"""
        )
    return parser.parse_args()

def run():
    config = vars(parse_args())
    with open(config["in_sam"][0], "r") as infile:
        match_count = 0
        for line in infile:
            info = line.strip().split("\t")
            if info[2] == "chrY" and int(info[3]) > 2781479 and int(info[3]) < 56887903:
                match_count += 1
    print(match_count)

if __name__ == '__main__':
    run()
