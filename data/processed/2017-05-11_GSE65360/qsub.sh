#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

module load bedtools

#bedtools intersect -wa -wb -loj -a GSE65360_single-GM12878.peaks.bed -b segway_gm12878.bed >GSE65360_single-GM12878.realoverlap.bed
bedtools intersect -wa -wb -loj -a segway_gm12878.bed -b GSE65360_single-GM12878.peaks.bed >GSE65360_single-GM12878.revoverlap.bed
