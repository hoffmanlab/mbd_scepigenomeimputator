#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

IDX=$(( SGE_TASK_ID - 1 ))
IN_FILES=(*.score.bed.gz)
IN_FILE=${IN_FILES[$IDX]}
OUT_FILE=`basename $IN_FILE`
OUT_FILE=${OUT_FILE%%.gz}

TMP_FILE=${IN_FILE}.tmp.bed
gzip -cd $IN_FILE | grep -P '(?<!\t0)$' >$TMP_FILE
bedtools genomecov -i $TMP_FILE -g ../hg19.chrom.sizes.sorted.txt >genomecov/$OUT_FILE
