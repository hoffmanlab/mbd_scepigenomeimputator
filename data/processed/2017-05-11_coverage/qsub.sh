#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

module load bedtools

SAMPLES_FILE=$1
FILE_NAME=`sed "${SGE_TASK_ID}q;d" $SAMPLES_FILE`
BASE_NAME=${FILE_NAME%%.*}
CELL_LINE=${SAMPLES_FILE%%.*}

bedtools genomecov -i all-bed/$FILE_NAME -g hg19.chrom.sizes.txt >$CELL_LINE/${BASE_NAME}.cov.txt
