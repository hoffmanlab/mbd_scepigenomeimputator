#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

qsub -q hoffmangroup -l h_vmem=8G -l mem_requested=8G -t 1-384 -tc 10 -cwd qsub.sh gm12878-samples.txt
qsub -q hoffmangroup -l h_vmem=8G -l mem_requested=8G -t 1-96 -tc 5 -cwd qsub.sh h1esc-samples.txt
qsub -q hoffmangroup -l h_vmem=8G -l mem_requested=8G -t 1-288 -tc 10 -cwd qsub.sh K562-samples.txt
qsub -q hoffmangroup -l h_vmem=8G -l mem_requested=8G -t 1-96 -tc 5 -cwd qsub.sh TF1-samples.txt
