#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
# Quick bugfix

# verbose failures
set -o nounset -o pipefail -o errexit

for i in *_fantom5.bed; do
    sed -e '2d' $i | gzip - >combined-fantom5/$i.gz
done
