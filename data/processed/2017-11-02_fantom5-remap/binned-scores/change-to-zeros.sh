#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

for i in *.bed.gz; do
    gzip -cd $i | sed --expression=s'/\.$/0/g' | gzip - >${i}.tmp
    mv ${i}.tmp $i
done
