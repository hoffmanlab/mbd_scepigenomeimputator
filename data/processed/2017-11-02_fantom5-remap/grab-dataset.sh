#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

# Obtains the dataset associated with the files in the given mappable_wide.tsv

cut -f3 mappable_wide.tsv >chosen-dnase.tsv
grep -f chosen-dnase.tsv ENCODE-signal/experiment_mapping.tsv | cut -f 1 >chosen-dnase-datasets.tsv
grep -f chosen-dnase-datasets.tsv ENCODE-signal/bam/experiment_mapping.tsv | sort -u -k1,1 | cut -f 3 >ENCODE-signal/bam/select-experiments.tsv
cd ENCODE-signal/bam/
grep -f select-experiments.tsv download_url.txt >select-download-url.tsv
wget -f select-download-url.tsv -q
