# Remap
Given the new mapping files that I developed in 2017-10-30_sample-mapping, I need to regenerate the binned scores and data files. 

This should result in 45 mappable cell types 

Thing to note is that up until now I've only taken one DNase-seq file as my gold standard for each cell type -- at some point in the future I should expand this to average out all of the available DNase-seq files

## Leave-4-out
This time, I've elected to remove these four cell lines:
skin_fibroblast	ENCODE	ENCFF001WFF.bed.gz	skin_fibroblast_fantom5	CL:0002620_skin_fibroblast_expressed_enhancers.bed
thymus	ENCODE	ENCFF278XGA.bed.gz	thymus_fantom5	UBERON:0002370_thymus_expressed_enhancers.bed
t-cell	ENCODE	ENCFF345YDG.bed.gz	t-cell_fantom5	CL:0000084_T_cell_expressed_enhancers.bed
pancreas	ENCODE	ENCFF231QBI.bed.gz	pancreas_fantom5	UBERON:0001264_pancreas_expressed_enhancers.bed
