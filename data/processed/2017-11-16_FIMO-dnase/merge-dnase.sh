#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l mem_requested=16G
#$ -N merge-dnase

# verbose failures
set -o nounset -o pipefail -o errexit

# Merges the input DNase files, doing repeated merging to avoid the file getting too large
# Usage: merge-dnase.sh <input_files> <out_file_name> 
module load bedtools

OUT_FILE_NAME=${@: -1}
if [ -e $OUT_FILE_NAME ]; then
    echo "Refusing to overwrite existing file $OUT_FILE_NAME"
    exit
fi

# Generate the output file
touch $OUT_FILE_NAME
for file in "$@"; do
    # Ignore the final argument
    echo $file
    if [[ "$file" == *.gz ]]; then
        gzip -cd $file | cat - $OUT_FILE_NAME | cut -d$'\t' -f1-3 | sort -k1,1 -k2,2n - >${OUT_FILE_NAME}.tmp
    else
        cat $file $OUT_FILE_NAME | sort -k1,1 -k2,2n - >${OUT_FILE_NAME}.tmp
    fi
    bedtools merge -i ${OUT_FILE_NAME}.tmp >$OUT_FILE_NAME
done
