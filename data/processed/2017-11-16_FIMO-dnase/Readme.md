```
module load igenome-human/hg19 

./merge-dnase.sh ENCODE-DNase/*.gz encode-merged.bed

bedtools getfasta -fi $REF -bed encode-merged.bed -fo encode-accessible.fa
```

Also grabbed a blacklist from https://www.encodeproject.org/files/ENCFF000KJP/@@download/ENCFF000KJP.bigBed 
