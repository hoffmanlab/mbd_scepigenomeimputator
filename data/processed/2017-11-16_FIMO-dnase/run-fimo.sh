#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

# Adapted from Mehram Karimzadeh's fimo-motor.sh script
# Usage: qsub <qsub args> -t 1-<NumberOfTfs> -cwd -b y sh fimo_motor.sh <FIMODIR> <FASTADIR> <TFMOTIFPATH>

FIMODIR=$1
FASTADIR=$2
TFMOTIFPATH=$3

module load meme/4.11.2

JOBID=$(($SGE_TASK_ID - 1))
MOTIFS=($(cat $TFMOTIFPATH | awk -v OFS='\t' '{if($3 == "TRUE") print $1}'))
TFS=($(cat $TFMOTIFPATH | awk -v OFS='\t' '{if($3 == "TRUE") print $2}'))

JASPAR=/mnt/work1/users/hoffmangroup/mkarimzadeh/2015/VirtualChipSeq/Databases/Jaspar/motif_databases_1210/motif_databases/JASPAR/JASPAR_CORE_2016_vertebrates_FriendlyNames.meme

SAMPLE="test_sample"
TF=${TFS[$JOBID]}
MOTIF=${MOTIFS[$JOBID]}
OUTDIR=$FIMODIR/$TF\_$MOTIF
FASTAFILE=$FASTADIR
JOBNAME=$SAMPLE\_$TF
fimo --bgfile motif-file --max-strand --max-stored-scores 100000000 --motif $MOTIF --oc $OUTDIR --thresh 1e-4 $JASPAR $FASTAFILE
