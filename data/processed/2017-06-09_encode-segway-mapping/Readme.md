# Matching encyclopedia annotations with ENCODE/Roadmap samples

I'm aiming to map encyclopedia annotations with ENCODE DNase-seq runs and Roadmap Chromatin Accessibility runs for later analysis 

## Encyclopedia matching experiment

~~~
usage: find-matching-experiment.py [-h] [--enable-dups]
                                   segway-folder encode-map roadmap-folder

positional arguments:
  segway-folder   Location of Segway folder
  encode-map      Encode mappability file
  roadmap-folder  Roadmap Chromatin Accessibility folder

optional arguments:
  -h, --help      show this help message and exit
  --enable-dups   Whether or not to allow duplicate entries per cell type
~~~
### Input
**segway-folder**: [Cell-specific Segway encyclopedia files](https://noble.gs.washington.edu/proj/encyclopedia/interpreted/) 

**encode-map**: Experiment mapping from the ../2017-05-15-encode-segway-encyclopedia/experiment.mapping.tsv 

(This file was generated from the download script I was using) 

**roadmap-folder**: Roadmap Chromatin Accessibility experiments. Uses a regex to find the proper mapping, so just make sure the file names contain the cell line name


## Analysis performed thus far

### mappable_old.txt
Mapping file generated from ENCODE DNase-seq narrowPeak, but also a bad Roadmap Chromatin Accessibility dataset (it wasn't narrowPeak)

### 2017-08-18-mappable.txt
Mapping file generated from ENCODE DNase-seq narrowPeak, and the proper Roadmap Chromatin Accessibility narrowPeak files

### 2017-08-30-multiple-exp-mappable.txt
Mapping file generated from ENCODE DNase-seq narrowPeak, and Roadmap Chromatin Accessibility signal files. 

Each cell type was allowed to map to multiple files, unlike previous analyses

### 2017-09-14-signal.txt
Mapping file generated from ENCODE DNase-seq signal, and Roadmap Chromatin Accessibility signal files. 

Each cell type was allowed to map to multiple files.
