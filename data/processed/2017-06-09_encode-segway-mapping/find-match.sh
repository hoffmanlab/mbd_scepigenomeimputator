#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

module load python

TO_FIND=$1
FILE_NAME=`basename $TO_FIND`
CELL_NAME=${FILE_NAME%%.*}

# Does this exist in ENCODE?
echo $CELL_NAME
python find-encode-match.py $CELL_NAME
