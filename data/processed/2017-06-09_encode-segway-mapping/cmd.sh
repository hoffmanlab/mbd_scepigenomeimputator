#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

python3 2017-06-find-matching-experiment.py segway-annot/ ../2017-05-15-encode-segway-encyclopedia/encode-dnase/ roadmap/ >unmappable.txt
