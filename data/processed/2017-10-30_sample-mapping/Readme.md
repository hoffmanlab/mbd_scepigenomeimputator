# Consolidated Manual Mapping
After much consternation due to not having done this manually, I'm finally doing it

Code used to generate the roadmap stuff
~~~
/bin/ls -1 -a roadmap-dnase-narrowPeak/*.gz >mapping_roadmap.tsv

(Then in R): 

a <- read.table("mapping_roadmap.tsv", sep="\t")
a <- as.data.frame(apply(a, MARGIN=2, basename))
b <- apply(a, MARGIN=2, gsub, pattern="UW.(.+).ChromatinAccessibility.+", repl="\\1")
a$V2 <- b
colnames(a) <- c("Filename", "Cell.Type")
write.table(a, "mapping_roadmap.tsv", sep="\t", col.names=TRUE, row.names=FALSE, quote=FALSE)
~~~
