#!/bin/bash
# Extract only chr1-22+X

# verbose failures
set -o nounset -o pipefail -o errexit

for bedfile in ./raw/*.bed
do
    basefn=`basename $bedfile`
    grep -P 'chr[0-9X]+\t' raw/$basefn >$basefn
done
