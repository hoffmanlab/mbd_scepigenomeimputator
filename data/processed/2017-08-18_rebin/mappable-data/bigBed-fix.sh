#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

module load ucsctools/315

for i in *.bigBed; do
    NEW_FN=${i%%.bigBed}.bed
    echo ${NEW_FN}
    bigBedToBed $i $NEW_FN
    gzip $NEW_FN
done

for i in *.narrowPeak.gz; do
    NEW_FN=${i%%.gz}.bed.gz
    echo $NEW_FN
    mv $i $NEW_FN
done
