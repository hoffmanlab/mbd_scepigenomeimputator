#!/bin/bash
# Aggregates the results of windower.sh

# verbose failures
set -o nounset -o pipefail # -o errexit

# Move all of the files that are mappable to its own folder
MAPPING_FILE=../mappable.txt
DATA_FILES=`cut -f 3 $MAPPING_FILE`
#for f in $DATA_FILES; do
#    #echo ${f}.score.bed
#    #cut -f 4 ${f}.score.bed | paste reduced.scores.tsv - >reduced.scores.tmp.tsv
#    #mv reduced.scores.tmp.tsv reduced.scores.tsv
#    if [ -e ../encode-dnase/${f} ]; then
#        ln -s ../encode-dnase/${f} ./
#    elif [ -e ../roadmap-dnase/${f} ]; then
#        ln -s ../roadmap-dnase/${f} ./
#    else
#        echo "${f} data not found!"
#    fi
#done

SCORE_FILES=`cut -f 1 $MAPPING_FILE`
#cd ../mappable-score
for f in $SCORE_FILES; do
    if [ -e ../score/$f.score.bed ]; then
        ln -s ../score/$f.score.bed ./
    else
        echo "${f} score not found!"
    fi
done
