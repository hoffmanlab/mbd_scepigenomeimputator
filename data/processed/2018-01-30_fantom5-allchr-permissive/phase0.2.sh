#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

# Grab input arguments
PHASE0=${1:-"binned-score-tpm-0index"}
PHASE1=${2:-"binned-score-tpm-0.1-0index"}
OUTDIR=${3:-"binned-score-tpm-0.2-0index-test"}

IDX=$(( $SGE_TASK_ID - 1 ))

# Find this file in both directories
PHASE0DIR=($PHASE0/*.gz)
FILENAME=`basename ${PHASE0DIR[$IDX]}`

PHASE0FN=$PHASE0/$FILENAME
PHASE1FN=$PHASE1/$FILENAME

if [ ! -e "$PHASE1FN" ]; then
    echo "$FILENAME does not exist in $PHASE1, exiting"
    exit 1
fi

# merge & take max
# rationale: a transcribed call in either dataset should supercede a quiescent call
# an enhancer call should supercede transcribed&quiescent
TMP_FN="tmp_$FILENAME"
SORT_FN="$TMP_FN.sorted.bed"
gzip -cd $PHASE0FN | sed 's/2$/1/' - >$TMP_FN   # The promoter dataset should only be used to define promoter regions
gzip -cd $PHASE1FN | sed 's/1$/2/' - >>$TMP_FN  # The enhancer dataset should only be used to define enhancers

sort -k1,1 -k2,2n $TMP_FN >$SORT_FN
bedtools merge -i $SORT_FN -c 4 -o sum | gzip -c - >$OUTDIR/$FILENAME
rm $TMP_FN
rm $SORT_FN
