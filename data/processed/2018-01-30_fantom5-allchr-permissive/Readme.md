# Permissive Enhancers
After discovering a low amount of enhancers in each position, I'm going to try being more permissive with the Slidebase enhancers 

The following changes are applied to the scripts in 2017-11-06_fantom5-allchr/ 

1) Changing the detection limit for an enhancer for 3 to 1 

2) If there is any transcription inside of a permissive Slidebase-defined enhancer regions, call the entire region 

To clean up space on mordor, I've removed the following directories: 

~~~
binned-score-tpm-out/ 

binned-score-tpm-raw/ 

binned-score-out/
~~~

All of which were generated via scripts in this folder. Scripts were, in order: 

1. reduce_mappable.R 

2. qsub -t 1-44 windower-fantom5.sh 100 combined-fantom5/ 1,2,3,7 binned-score-tpm-0.1-out/ -loj

2. reparse-score.py (to convert score into calls with the above rules)

2. binned-score-to-numers.sh (to convert text scores into numbers)

3. mergeSimilar.sh (For space compression) 

# Other notes

In order to get 99% of all enhancers, I need to limit it so tpm > 0.0637547266081033 (cutting out 109043 hits across the entire tpm matrix) 

In order to get 90% of all enhancers, I need to limit to tpm > 0.116455791805935 (cutting out 1122860 hits across the entire tpm matrix) 

0.1: work with human_permissive_enhancers_phase_1_and_2_expression_tpm_matrix.txt.gz 

No number: work with hg19.cage_peak_phase1and2combined_tpm_ann.osc.txt 

0.2: merging of 0.1 and 0.2
