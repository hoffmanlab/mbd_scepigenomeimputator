#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

IN_DIR=(${1:-binned-score-raw}/*.gz)
OUT_DIR=${2:-binned-score-out}

IDX=$(( $SGE_TASK_ID - 1 ))
IN_FILE=${IN_DIR[$IDX]}
OUT_FN=`basename $IN_FILE`

echo $IN_FILE
python reparse-score.py "$IN_FILE" "permissive_enhancers.bed" "$OUT_DIR/$OUT_FN" --minimum_score 0.0637547266081033
