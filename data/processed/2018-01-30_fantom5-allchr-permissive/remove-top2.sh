#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
# Quick bugfix

# verbose failures
set -o nounset -o pipefail -o errexit

for i in *_fantom5.bed; do
    tail -n +2 $i | gzip - >combined-fantom5/`basename $i`.gz
done
