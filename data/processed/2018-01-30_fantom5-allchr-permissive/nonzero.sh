#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

IN_DIR=($1/*)
OUT_DIR=${2:-${1%/}-nonzero}

for i in "${IN_DIR[@]}"; do
    cmd='grep -vP "\t0$"'
    if [[ $i == *.gz ]]; then
        cmd="gzip -cd $i | $cmd | gzip -"
    else
        cmd="$cmd $i"
    fi
    cmd="$cmd >$OUT_DIR/`basename $i`"
    echo $cmd
    $cmd
    #gzip -cd $i | grep -vP "\t0$" | gzip - >binned-score-0index-nonzero/`basename $i`
done
