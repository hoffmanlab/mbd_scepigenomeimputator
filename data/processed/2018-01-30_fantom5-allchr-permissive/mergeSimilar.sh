#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

IN_DIR=($1/*.gz)
OUT_DIR=$2

IDX=$(( $SGE_TASK_ID - 1 ))
IN_FILE=${IN_DIR[$IDX]}

python ./mergeSimilar.py "$IN_FILE" | gzip - >"$OUT_DIR/`basename $IN_FILE`"
