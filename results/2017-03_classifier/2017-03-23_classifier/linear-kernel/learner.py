# learner.py ##################################################################
# This module attempts to learn how to go from input to output
#
# Contruct the learning dataset by collecting:
# 1) Chromosome number
# 2) Distance from nearest ATAC-Seq peak
# 3) RNA-Seq signal strength

import argparse
import os
import pickle
import re

import numpy as np
from sklearn import svm
from sklearn import grid_search

# CONSTANTS ###################################################################

# INITIALIZATION ##############################################################
def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(
        description = "Learn, from a set of input files, how to map to output"
        )
    parser.add_argument(
        "training_set",
        nargs = 1,
        help = "Location of training set"
        )
    return parser.parse_args()

def load_datasets(config):
    """
    Load the training dataset
    """
    datasets = {}
    datasets["train"] = np.loadtxt(config["training_set"][0])
    return datasets

def reorder_datasets(config, datasets):
    """
    Reorder the input datasets for input into sklearn
    config: dict of settings, loaded via safe_load_config()
    datasets: dict of the BedTools datasets, loaded via load_datasets()
    returns (X, Y), which are for svm to classify
    """

    # At some point in my life I'm going to have to admit I have no idea
    # what the heck I'm doing

    # Allocate enough space for X and Y
    X = datasets["train"][:,(0,3,4)]
    Y = datasets["train"][:,5]
    # ...and then populate it. Note the wild assumption that the lines match up
    return (X, Y)

def learn(config, X, Y):
    """
    Learn the input using an SVC, optimizing across possible parameters
    using grid_search
    X: Input predictors to give to svm.SVC().fit()
    Y: Output classifications to give to svm.SVC().fit()
    """
    #param_grid = {
    #    "kernel": ['linear', 'rbf']
    #    }

    # In parallel, determine the best kernel type to use
    #gs = grid_search.GridSearchCV(
    #    svm.SVC(), param_grid, n_jobs=4, cv=3)
    #gs.fit(X, Y)
    #print("Kernel used: {0}".format(gs.best_params_["kernel"]))

    # Use the best kernel type to create our classifier
    #classifier = svm.SVC(gs.best_params_["kernel"])
    classifier = svm.SVC(kernel="linear", probability=True, verbose=True)
    classifier.fit(X, Y)
    return classifier

def validate(config, model):
    pass

def run():
    config = vars(parse_args())
    #config = safe_load_config(args.config)
    print(config)
    datasets = load_datasets(config)
    print("Datasets loaded!")
    (X, Y) = reorder_datasets(config, datasets)
    print("Datasets prepped!")
    classifier = learn(config, X, Y)
    # uhh... pickle it or something
    with open('classifier.pickle', 'wb') as f:
        pickle.dump(classifier, f, pickle.HIGHEST_PROTOCOL)
    print("Done!")


if __name__ == '__main__':
    run()

