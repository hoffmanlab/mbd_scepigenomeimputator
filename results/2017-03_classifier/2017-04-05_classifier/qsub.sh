#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

module load python
python learner.py SRR1779587.final.bed
