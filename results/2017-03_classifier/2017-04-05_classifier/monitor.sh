#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

starting_num=`qstat | wc -l`
a=$starting_num

echo "Waiting for job count to go below ${a-4}"
while [ $a -ge $starting_num ]; do
    sleep 5
    a=`qstat | wc -l`
done
