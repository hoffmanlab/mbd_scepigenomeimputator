# learner.py ##################################################################
# This module attempts to learn how to go from input to output
#
# Contruct the learning dataset by collecting:
# 1) Chromosome number
# 2) Distance from nearest ATAC-Seq peak
# 3) RNA-Seq signal strength

import argparse
import os
import pickle
import re

import numpy as np
from sklearn import svm
from sklearn import grid_search
from sklearn import metrics

# CONSTANTS ###################################################################

# INITIALIZATION ##############################################################
def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(
        description = "Learn, from a set of input files, how to map to output"
        )
    parser.add_argument(
        "classifier.pickle",
        nargs = 1,
        help = "Classifier pickle file"
        )
    parser.add_argument(
        "verification.tsv",
        nargs = 1,
        help = "Verification dataset"
        )
    return parser.parse_args()

def load_classifer(config):
    """
    Load the classifier given in the config file
    """
    classifier = None
    with open(config["classifier.pickle"][0], 'rb') as f:
        classifier = pickle.load(f)
    return classifier

def load_verificiation(config):
    """
    Load the verification file given in the config file
    """
    verification = np.loadtxt(config["verification.tsv"][0])
    X = verification[:,(0,3,4)]
    Y = verification[:,5]
    return (X, Y)

def verify(config, X, Y):
    """
    Grab the ROC and AUC from the classifier
    """
    metrics.roc_curve()


def run():
    config = vars(parse_args())
    classifier = load_classifier(config)
    (X, Y) = load_verification(config)

if __name__ == '__main__':
    run()
