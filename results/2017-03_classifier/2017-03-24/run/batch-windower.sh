#!/bin/bash
# Performs a batch run of window.sh for each scATAC-seq file given

# verbose failures
set -o nounset -o pipefail -o errexit

SAMPLES_FILE=`realpath $1`
ATAC_PATH=/mnt/work1/users/home2/fnguyen/scProject/data/2017-02-23/bed

while read ATAC_NAME; do
    qsub -q hoffmangroup -N $ATAC_NAME -cwd ./windower.sh 100000 $ATAC_PATH/$ATAC_NAME ENCFF573QZL.bedGraph
done <$SAMPLES_FILE

