#!/bin/bash
# Makes the input dataset for the learner.

# verbose failures
set -o nounset -o pipefail -o errexit

module load bedtools
module load R

# Read input arguments
WINDOW_SIZE=$1
ATAC_FILE=$2
RNA_FILE=$3
ATAC_BASE=`basename $ATAC_FILE`
ATAC_BASE=${ATAC_BASE%%.*}
RNA_BASE=`basename $RNA_FILE`
RNA_BASE=${RNA_BASE%%.*}

# Create a "window" bed file with positions on a single base pair every $WINDOW_SIZE apart
if [ ! -e genome.${WINDOW_SIZE}.validation.sorted.bed ]; then
    bedtools makewindows -g hg19.trim.chrom.sizes -w $WINDOW_SIZE >genome.${WINDOW_SIZE}.ranges.bed
    awk '/(.+)\t(.+)\t.+/ { print $1"\t"$2"\t"$2+1 }' genome.${WINDOW_SIZE}.ranges.bed >genome.${WINDOW_SIZE}.bed

    # Split the windows into an analysis and validation set (every 400th position will be training)
    awk 'NR % 800 == 0' genome.${WINDOW_SIZE}.bed >genome.${WINDOW_SIZE}.training.bed
    awk 'NR % 800 != 0' genome.${WINDOW_SIZE}.bed >genome.${WINDOW_SIZE}.validation.bed
    sortBed -i genome.${WINDOW_SIZE}.training.bed >genome.${WINDOW_SIZE}.training.sorted.bed
    sortBed -i genome.${WINDOW_SIZE}.validation.bed >genome.${WINDOW_SIZE}.validation.sorted.bed
fi

# Ensure all bed files are sorted
if [ ! -e ${ATAC_BASE}.sorted.bed ]; then
    sortBed -i $ATAC_FILE >${ATAC_BASE}.sorted.bed
fi
if [ ! -e ${RNA_BASE}.sorted.bed ]; then
    sortBed -i $RNA_FILE >${RNA_BASE}.sorted.bed
fi

# Grab the distance to the nearest scATAC-Seq peak
bedtools closest -t first -D b -a genome.${WINDOW_SIZE}.training.sorted.bed -b ${ATAC_BASE}.sorted.bed >${ATAC_BASE}.training.score.bed
bedtools closest -t first -D b -a genome.${WINDOW_SIZE}.validation.sorted.bed -b ${ATAC_BASE}.sorted.bed >${ATAC_BASE}.validation.score.bed

# Append the RNA signal from this position
bedtools intersect -loj -wa -wb -sorted -a ${ATAC_BASE}.training.score.bed -b ${RNA_BASE}.sorted.bed >${ATAC_BASE}.training.rna.bed
bedtools intersect -loj -wa -wb -sorted -a ${ATAC_BASE}.validation.score.bed -b ${RNA_BASE}.sorted.bed >${ATAC_BASE}.validation.rna.bed

# Append the Segway annotation for this position
bedtools intersect -loj -wb -sorted -a ${ATAC_BASE}.training.rna.bed -b segway_gm12878.bed >${ATAC_BASE}.training.seg.bed
bedtools intersect -loj -wb -sorted -a ${ATAC_BASE}.validation.rna.bed -b segway_gm12878.bed >${ATAC_BASE}.validation.seg.bed

# Parse out the necessary columns for training
Rscript windower.R ${ATAC_BASE}.training.seg.bed ${ATAC_BASE}.training.tsv
Rscript windower.R ${ATAC_BASE}.validation.seg.bed ${ATAC_BASE}.validation.tsv
