#!/bin/bash
# Moves intermediate output by batch-windower.sh into different folders

# verbose failures
set -o nounset -o pipefail -o errexit

mkdir sorted_bed
mkdir training
mkdir validation
mkdir log
mkdir tmp
mv *.sorted.bed sorted_bed/
mv *.training.tsv training/
mv *.validation.tsv validation/
mv *.e* *.o* log/
mv *.training.* *.validation.* tmp/
