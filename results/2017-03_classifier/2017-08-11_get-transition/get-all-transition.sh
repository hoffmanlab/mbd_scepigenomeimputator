#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -t 1-34

# verbose failures
set -o nounset -o pipefail -o errexit

# Grab all of the transition matrices
module load R/3.3.0

IN_DIR=($1/*.bed.gz)

IDX=$((SGE_TASK_ID - 1))
IN_FILE=${IN_DIR[$IDX]}
BASENAME=`basename $IN_FILE`
BASENAME=${BASENAME%%.gz}

Rscript get-transition.R -s $IN_FILE -o ${BASENAME}.tsv
