#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -t 1-34

# verbose failures
set -o nounset -o pipefail -o errexit

module load python3

IN_DIR=($1/*.bed.gz)
OUT_DIR=$2

IDX=$(( $SGE_TASK_ID - 1 ))
IN_FILE=${IN_DIR[$IDX]}
BASENAME=`basename $IN_FILE`
BASENAME=${BASENAME%%.gz}

python3 remap.py $IN_FILE $OUT_DIR/$BASENAME 4
