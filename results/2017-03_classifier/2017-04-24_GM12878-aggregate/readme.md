# Aggregate scATAC-seq analysis
This was my attempt to train on aggregated scATAC-seq of GM12878 mapped to SEGWAY annotations of GM12878

## Files
classifier.pickle: aggregate-trained HMM classifier  
mapping.txt: result of applying classifier.pickle to one of the GM12878 scATAC-seq runs  

## Results
This ended up being futile - it labelled every point either quiescent or silent
