#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

mkdir -p logs
mkdir -p logs/o
mkdir -p logs/e
mv *.o* logs/o
mv *.e* logs/e

mkdir -p sorted
mv SRR*.sorted.bed sorted/
mkdir -p score
mv *.score.bed score/
mkdir -p cut
mv *.cut.bed cut/
mkdir -p seg
mv *.seg.bed seg/
mkdir -p final
mv *.final.bed final/
mkdir -p mapping
mv *_mapping.txt mapping/
