#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

module load bedtools

cd gm12878-bed
while read sample; do
    sortBed -i ../../2017-04-04/bed/$sample >$sample
done <../gm12878-samples.txt
cd --
