# Single cell ATAC-seq quantification 

## Question  
How many runs are required to recapitulate a bulk signal?*

## Steps
./commands.R to get TSSes from gm12878-tss-intersects-count.bed from gm12878-samples.txt  
./gm12878-generator.sh to grab and sort out gm12878 bed files from my processed files  
./intersection.sh to grab the number of overlaps with ATAC-Seq signal  
