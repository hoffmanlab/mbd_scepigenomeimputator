#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

# Slop the segway annotations so that we can grab proximal ATAC-seq peaks
bedtools slop -i tss-segway-K562.bed -g ../../data/2017-01-23/hg19.chrom.sizes.txt -b 500 >tss-segway-K562-slop.bed
bedtools slop -i tss-segway-gm12878.bed -g ../../data/2017-01-23/hg19.chrom.sizes.txt -b 500 >tss-segway-gm12878-slop.bed

# Count up the number of scATAC-seq peaks at Segway-identified TSSes
bedtools intersect -loj -wa -c -sorted -a tss-segway-K562-slop.bed -b gm12878-bed/* >K562-tss-intersects-count.bed
bedtools intersect -loj -wa -c -sorted -a tss-segway-gm12878-slop.bed -b gm12878-bed/* >gm12878-tss-intersects-count.bed
bedtools intersect -loj -wa -c -sorted -a tss-segway-gm12878-slop.bed -b tss-segway-K562-slop.bed >intersample-tss-intersects-count.bed

#grep -wvE "0$" K562-tss-intersects-count.bed
