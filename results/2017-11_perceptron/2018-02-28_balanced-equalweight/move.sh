#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

SUFFIX=$1

TO_MOVE=(annotate_results cohens_data gmtk_parameters train_results length_distribution)
for i in ${TO_MOVE[@]}; do
    if [ -e ${i} ]; then
        mv ${i} ${i}_$SUFFIX
    fi
done
