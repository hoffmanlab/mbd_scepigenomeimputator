#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

# Train segway on a given cell type
# (This is for training one cell type at a time)

module load R

# Input
SCORE_GENOMEDATA_DIR=${1:-genomedata-score}
CHROM_GENOMEDATA_DIR=${2:-genomedata-dnase}
SCORE_BED_DIR=${3:-binned-score}
TRAIN_LINE=${4:-stomach}
EXCLUDE_FILE=${5:-test-set-exclude.bed} #-failures.bed}
#VALIDATE_FILE=${6:-validation-set.bed}
TRUTH=${7:-stomach_fantom5_balanced-testset.bed}
SEG_TABLE=${8:-seg_table.tsv}
STRUCTURE_FILE=${9:-custom-structure.str}

SCORE_TRACKS=($SCORE_GENOMEDATA_DIR/*)
TRAINING_TRACKS=(${SCORE_TRACKS[@]/$SCORE_GENOMEDATA_DIR\/${TRAIN_LINE}.genomedata/})
CHROM_ACCESS_TRACK=$CHROM_GENOMEDATA_DIR/${TRAIN_LINE}.genomedata
#TRUTH=$SCORE_BED_DIR/${TRAIN_LINE}_fantom5.score.bed

# Workaround for Segway not submitting jobs from non-head-nodes cluster
export DRMAA_LIBRARY_PATH=/sge/lib/linux-x64/libdrmaa.so
export SEGWAY_RAND_SEED=22426492
#export SEGWAY_NUM_LOCAL_JOBS=31
#export NUM_THREADS=31

# Train the model into the directory train_results/
segway --resolution=100 \
    --semisupervised=$TRUTH \
    --exclude-coords=$EXCLUDE_FILE \
    --split-sequences=4000000 \
    --cluster-opt="-q hoffmangroup -l hostname='!n22vm1'" \
    --tracks-from="tracks.txt" \
    --num-instances=10 \
    --num-labels=10 \
    --seg-table=$SEG_TABLE \
    --mem-usage=8,10,12,16,32,64 \
    --structure=$STRUCTURE_FILE \
    train \
    ${TRAINING_TRACKS[@]} $CHROM_ACCESS_TRACK \
    train_results
    #--minibatch-fraction=0.001 \
    #--validation-coords=${VALIDATE_FILE} \
    #--max-train-rounds=5 \

# Identify the entirety of the genome
segway \
    --exclude-coords=hg19-blacklist.bed \
    --cluster-opt="-q hoffmangroup" \
    identify \
    ${TRAINING_TRACKS[@]} $CHROM_ACCESS_TRACK \
    train_results \
    annotate_results
#    --exclude-coords=$EXCLUDE_FILE \
#    --bigBed=annotate_results/segway.layered.bigBed \

# Visualize various model metrics
segtools-gmtk-parameters train_results/params/params.params
segtools-length-distribution annotate_results/segway.bed.gz
