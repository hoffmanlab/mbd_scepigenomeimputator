#!/bin/bash
#$ -cwd
#$ -q hoffmangroup
#$ -l h_vmem=16G
#$ -l mem_requested=16G
#$ -N aggregate
# Aggregates the results of windower.sh

# verbose failures
set -o nounset -o pipefail -o errexit

DIR=($1/*.gz)

FIRST_FILE=${DIR[1]}
echo $FIRST_FILE
gunzip -cd $FIRST_FILE | cut -f 1,2,3 - >reduced_scores.tsv
for f in ${DIR[@]}; do
    echo ${f}
    gunzip -cd $f | cut -f 4,5 - | paste reduced_scores.tsv - >reduced_scores_tmp.tsv
    mv reduced_scores_tmp.tsv reduced_scores.tsv
done
