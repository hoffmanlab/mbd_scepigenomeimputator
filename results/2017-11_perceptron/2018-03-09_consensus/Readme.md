# Consensus trial 

My Segway runs have failed so spectacularly that I wanted to see how well basic consensus alignment would do. I've adapted aggregate.sh from my previous analysis to help with this.

Commands: 

~~~ 

aggregate-general.sh binned-score-tpm-0.2-0index/ 

cut -f 1-3,14 collapsed_scores.tsv >consensus-prediction.bed 

~~~
