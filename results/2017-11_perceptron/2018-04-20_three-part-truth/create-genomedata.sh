#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l h_vmem=16G
#$ -l mem_requested=16G
# Creates a genomefile from the given data directory

# verbose failures
set -o nounset -o pipefail -o errexit

module load bedtools/2.23.0

IN_DIR=(${1:-"binned-score"}/*.gz)
MAPPING_FILE=${2:-"mappable_wide.tsv"}
OUT_DIR=${3:-"genomedata-score/"}
SUFFIX=${4:-"score"}
AGP=${5:-"AGP/*.gz"}
#BLACKLIST=${4:-""}

#TMPFILE=$(mktemp)

IDX=$(($SGE_TASK_ID - 1))
IN_FILE=${IN_DIR[$IDX]}

IN_ROOT=`basename $IN_FILE`
IN_ROOT=${IN_ROOT%%.*}

echo $IN_ROOT
SAMPLE_NAME=`grep $IN_ROOT $MAPPING_FILE | cut -f 1`
echo $SAMPLE_NAME

## Prevent clobbering old files
#if [[ -e $OUT_DIR/${SAMPLE_NAME}.genomedata ]]; then
#    exit
#fi

ANNOT_FILE=$IN_FILE
#if [[ -n $BLACKLIST ]]; then
#    bedtools intersect -v -wa -a $IN_FILE -b $BLACKLIST >$TMPFILE
#    ANNOT_FILE=$TMPFILE
#fi
genomedata-load --assembly -s "$AGP" -t ${SAMPLE_NAME}_${SUFFIX}=$ANNOT_FILE $OUT_DIR/${SAMPLE_NAME}.genomedata
#genomedata-load --sizes -s hg19.trim.chrom.sizes -t ${SAMPLE_NAME}_${SUFFIX}=$ANNOT_FILE $OUT_DIR/${SAMPLE_NAME}.genomedata
