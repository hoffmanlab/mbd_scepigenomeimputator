#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

# Check to make sure every file is properly gzipped
for i in */*.gz; do
    if [[ `file $i` == *"ASCII"* ]]; then
        basefile=${i%%.gz}
        mv $i $basefile
        gzip $basefile
    fi
done
