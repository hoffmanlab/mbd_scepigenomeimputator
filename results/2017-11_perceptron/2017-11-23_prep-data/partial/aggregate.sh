#!/bin/bash
#$ -cwd
#$ -q hoffmangroup
#$ -l h_vmem=16G
#$ -l mem_requested=16G
#$ -N aggregate
# Aggregates the results of windower.sh

# verbose failures
set -o nounset -o pipefail -o errexit

TO_PARSE=(`ls *_chr*.bed.gz | sort -n`)
echo "${TO_PARSE:0}"
gunzip -cd ${TO_PARSE} >reduced_scores.tsv
for f in "${TO_PARSE[@]:1}"; do
    echo ${f}
    gunzip -cd ${f} | cut -f 4- - | sed -e 's/\t\t/\t0.0\t/g' | paste reduced_scores.tsv - >reduced_scores_tmp.tsv
    mv reduced_scores_tmp.tsv reduced_scores.tsv
done
