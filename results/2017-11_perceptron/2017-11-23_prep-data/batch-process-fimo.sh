#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

for i in $(seq 1 1); do
    echo $i
    qsub -N chr${i}-fimo -t 1-41 ./process-fimo.sh fimo-out/ hg19-blacklist-no-test.bed.gz hg19.trim.chrom.sizes chr${i} chr${i}_fimo.bed.gz
    cd partial
    qsub -hold_jid chr${i}-fimo -N chr${i}-agg ./aggregate-chr.sh chr${i}
    #qsub -N chr${i}-agg ./aggregate-chr.sh chr${i}
    cd ../
done
