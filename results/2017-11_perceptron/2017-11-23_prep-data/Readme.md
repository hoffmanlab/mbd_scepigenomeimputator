# Data Prep
This directory is used to collate all of my input data in order for input into a machine learning method such as a multi-layer perceptron

First, create your minibatch to train upon:
```
./prepare-minibatch.sh hg19.chrom.sizes.bed hg19-blacklist.bed minibatch.bed
```

Next, run the preprocessing script on your FIMO motifs
```
./batch-process-fimo.sh
```

Then, train it via learn.py
```python learn.py```

# Segway Data Prep
This directory also contains files for later use by Segway. 

The ones of note are the .genomedata files, which were created with create-genomedata.sh 


# Other

I also have a few other files here, created mostly via running ```mergeSimilar.py``` on various binned-score/binned-data objects. ```mappable_wide.tsv``` has my file mapping
