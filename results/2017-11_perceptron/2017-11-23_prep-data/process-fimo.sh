#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l mem_requested=16G
##$ -t 1-41

# process-fimo.sh: execute on SGE with -t 1-N where N is the number of 
# TF families in your fimo directory divided by 5

# The number of TF families is determined by the unique number of 
# 3-letter starting characters

# verbose failures
set -o nounset -o pipefail -o errexit

idx=$((SGE_TASK_ID -  1))

FIMO_DIR=$1     # ./fimo-out/
BLACKLIST_NAME=$2 # hg19-blacklist.bed
CHROM_SIZES=$3  # hg19.trim.chrom.sizes
CHROM=$4        # chr1
OUT_NAME=$5

module load python3

echo python3 process-fimo.py $FIMO_DIR $CHROM_SIZES $BLACKLIST_NAME $idx $CHROM $OUT_NAME
python3 process-fimo.py $FIMO_DIR $CHROM_SIZES $BLACKLIST_NAME $idx $CHROM $OUT_NAME
