#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

# Grabs some random locations in the genome to prepare a minibatch with
# usage: ./prepare-minibatch.sh hg19.chrom.sizes.bed hg19-blacklist.bed minibatch.bed
CHROM_SIZES=$1
BLACKLIST=$2
OUT_NAME=$3

module load bedtools

# Prep non-blacklisted regions
bedtools makewindows -g $CHROM_SIZES -w 100000 >test.bed
bedtools intersect -a test.bed -b $2 -v >test-blacklisted.bed

# Grab random regions
shuf -n 300 test-blacklisted.bed >random_regions.tmp
sort -k1,1 -k2,2n random_regions.tmp >$OUT_NAME
bedtools intersect -wa -a test.bed -b $OUT_NAME -v >${OUT_NAME}.reversed.bed.tmp
bedtools merge ${OUT_NAME}.reversed.bed.tmp >${OUT_NAME}-reversed.bed
rm random_regions.tmp test.bed test-blacklisted.bed ${OUT_NAME}.reversed.bed.tmp
