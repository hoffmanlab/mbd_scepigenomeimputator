# learner.py ##################################################################
# This script trains a perceptron using seqlearn to obtain epigenomic state

# PREAMBLE ####################################################################
import argparse
import gzip
import warnings

import numpy as np
import pandas as pd
#import seqlearn.hmm
from hmmlearn import hmm
from sklearn.externals import joblib

# INPUT PARSING ###############################################################
def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(
        description = "Learn, from a set of input files, how to map to output"
        )
    parser.add_argument(
        "training_set",
        help = "Location of training set"
        )
    parser.add_argument(
        "test_set",
        help = "Location of test set"
        )
    parser.add_argument(
        "out_train_fn",
        help = "Output file name for training predictions"
        )
    parser.add_argument(
        "out_test_fn",
        help = "Output file name for testing predictions"
        )
    parser.add_argument(
        "output",
        help = "name of output file to write the classifier to"
        )
    return parser.parse_args()

def load_datasets(config):
    """
    Load the training dataset
    """
    #with gzip.open(config["training_set"], 'rt') as f:
    #    lines = (line for (i, line) in enumerate(f) if i > 1)
    #    ncols = len(f.readline().split('\t')) - 1
    #    print(ncols)
    #    datasets = {"train": np.loadtxt(lines, skiprows=1, usecols=range(3,ncols))}
    datasets = {"train": np.genfromtxt(config["training_set"]),
                "test": np.genfromtxt(config["test_set"])}
    return datasets

def reorder_datasets(config, datasets):
    """
    Reorder the input datasets for input into sklearn
    config: dict of settings, loaded via safe_load_config()
    datasets: dict of the BedTools datasets, loaded via load_datasets()
    returns (X, Y), which are for svm to classify
    """
    # Transform X to become one-hot encoded for seqlearn
    X = datasets["train"][:,3:].astype(int)
    Y = datasets["test"][:,3:].astype(int)
    return (X, Y)

def learn(config, X, Y):
    """
    Learn the input using a HMM, optimizing across possible parameters
    using grid_search
    X: Input predictors to give to svm.SVC().fit()
    """
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    clf = hmm.GaussianHMM(n_components=10, n_iter=100)
    clf.fit(X)
    return clf

def validate(config, clf, X, Y):
    print(X.shape)
    train_out = clf.predict(X)   # TODO
    print(train_out.shape)
    test_out  = clf.predict(Y)   # TODO
    train_out.tofile(config["out_train_fn"], sep="\n")
    test_out.tofile(config["out_test_fn"], sep="\n")

def run():
    config = vars(parse_args())
    #config = safe_load_config(args.config)
    print(config)
    datasets = load_datasets(config)
    print("Datasets loaded.")
    (X, Y) = reorder_datasets(config, datasets)
    print("Datasets prepped.")
    classifier = learn(config, X, Y)
    print("Classifier trained.")
    validate(config, classifier, X, Y)
    print("Validation set written.")
    # Output the final classifier as a pickle
    joblib.dump(classifier, config["output"])
    print("Done!")

if __name__ == '__main__':
    run()
