#!/usr/bin/env python
import argparse
import sys

from functools import partial
import gzip
import numpy as np
import os
import pandas as pd
import re

TFS_TO_PROCESS = 5
# FIMO Processing ##############################################################
# (Functions taken from Mehran)
def get_chrsize_dict(chromsize_path):
    # Grabs the chromosome sizes from the hg19.chromsize file
    chrom_dict = {}
    with open(chromsize_path, "r") as chrom_link:
        for chrom_line in chrom_link:
            chrom, size = chrom_line.rstrip().split("\t")
            chrom_dict[chrom] = int(size)
    return chrom_dict

def make_regions(chrom, chromsize, window, slide_window, blacklist_path):
    # Generates an empty, windowed dataframe
    st_regs = np.empty(0)
    end_regs = np.empty(0)
    for i in np.arange(0, window, step=slide_window):
        st_regs_temp = np.arange(i, chromsize - window, step=window)
        st_regs = np.concatenate((st_regs_temp, st_regs))
        st_regs.sort()
        st_regs = np.array(st_regs, dtype=int)
        end_regs_temp = np.arange(window + i, chromsize, step=window)
        end_regs = np.concatenate((end_regs_temp, end_regs))
        end_regs.sort()
        end_regs = np.array(end_regs, dtype=int)
    black_df = pd.read_csv(blacklist_path, sep="\t", compression="gzip")
    black_df = black_df[black_df.iloc[:, 0] == chrom]
    ind_keep = np.where(
        np.logical_not(
            pd.DataFrame(st_regs).iloc[:, 0].isin(black_df.iloc[:, 1])))[0]
    dict_data = {"#Chrom": chrom, "Start": st_regs,#[ind_keep],
                 "End": end_regs}#[ind_keep]}
    bed_df = pd.DataFrame(dict_data)
    bed_df = bed_df[["#Chrom", "Start", "End"]]
    return bed_df

def get_ar_val(start, np_ar, window_size):
    # Grabs the mean value of the input np_ar for the given start/size
    if np_ar.size == 0:
        return 0
    end_pos = start + window_size + 1
    if end_pos > len(np_ar):
        end_pos = len(np_ar)
    range_vals = np.array(range(start, end_pos), dtype=int)
    out_val = np.mean(np_ar[range_vals])
    return out_val

def get_chrom_fimo(chrom_st_end):
    # Helper function to grab the chromosome number from a position string
    return chrom_st_end.split(":")[0]

def get_start_fimo(chrom_st_end):
    # Helper function to grab the start position from a position string
    return int(chrom_st_end.split(":")[1].split("-")[0])

def make_fimo_ar(fimo_path, chrom, chromsize):
    # Turns the input fimo file into a bed file
    fimo_ar = np.array([])
    fimo_df = pd.read_csv(fimo_path, sep="\t")
    if fimo_df.shape[0] > 0:
        fimo_df["#Chrom"] = fimo_df.iloc[:, 1].map(get_chrom_fimo)
        fimo_df = fimo_df[fimo_df["#Chrom"] == chrom]
        fimo_df["Start"] = fimo_df.iloc[:, 1].map(get_start_fimo)
        fimo_df["End"] = fimo_df["Start"] + fimo_df["stop"]
        fimo_df["Start"] = fimo_df["Start"] + fimo_df["start"]
        fimo_df = fimo_df.iloc[
            :, np.logical_not(fimo_df.columns.isin(["start", "stop"]))]
        fimo_ar = np.zeros(chromsize, dtype=float)
        len_motif = fimo_df.iloc[0, -1] - fimo_df.iloc[0, -2]
        for i in range(fimo_df.shape[0]):
            st = int(fimo_df["Start"].iloc[i])
            score = float(fimo_df["score"].iloc[i])
            fimo_ar[st:(st + len_motif + 1)] = score
    return fimo_ar

def add_fimo(bed_df, chrom, tf, fimo_dir, chromsize, window_size) -> np.array:
    # Adds the fimo motifs associated with the given TF
    # Note: it assumes that all motifs associated with any TF
    # share the first three characters with its name
    tfmotifs = [tfmotif for tfmotif in next(os.walk(fimo_dir))[1]
                if re.search(
                    "^{}".format(tf.lower()[:3]), tfmotif.lower())]
    for tfmotif in tfmotifs:
        fimo_path = "{}/{}/fimo.txt".format(fimo_dir, tfmotif)
        print("Using {}".format(fimo_path))
        fimo_ar = make_fimo_ar(fimo_path, chrom, chromsize)
        get_fimo = partial(get_ar_val,
                           np_ar=fimo_ar,
                           window_size=window_size)
        fimo_score = bed_df.iloc[:, 1].map(get_fimo)
        bed_df[tfmotif] = fimo_score
        print("Added {}".format(tfmotif))
    return bed_df

# PARSE ARGS ###################################################################
def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Process the output of fimo-motor.py into a normalized bed file
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "fimo-dir",
        help = """Directory of fimo to process"""
        )
    parser.add_argument(
        "chrom-sizes",
        help = """Chromosome sizes file"""
        )
    parser.add_argument(
        "blacklist",
        help = """Blacklist file"""
        )
    parser.add_argument(
        "idx",
        help = """Process index"""
        )
    parser.add_argument(
        "chr-num",
        help = "Chromosome number to work on (e.g. 'chr1')",
        )
    parser.add_argument(
        "outname",
        help = """Output file name"""
        )
    parser.add_argument(
        "--window-size",
        default = 100,
        help = """Size of the window to scan across the genome"""
        )
    parser.add_argument(
        "--slide-size",
        default = 100,
        help = """Distance between each slice of the genome"""
        )
    return vars(parser.parse_args())
    return vars(parser.parse_args())

# MAIN #########################################################################
def main():
    args = parse_args()
    idx = int(args["idx"])
    chromsizes = get_chrsize_dict(args["chrom-sizes"])
    chrom = args["chr-num"]
    print(args)
    all_df = make_regions(
        chrom,
        chromsizes[chrom],
        args["window_size"],
        args["slide_size"],
        args["blacklist"]
        )

    # Grab the unique fimo motifs in the given directory
    tfs = set([tf.lower()[:3] for tf in next(os.walk(args["fimo-dir"]))[1]])
    tfs = sorted(tfs)
    start = TFS_TO_PROCESS*idx
    print(len(tfs))
    for tf in tfs[start:(start+TFS_TO_PROCESS)]:
        all_df = add_fimo(
            all_df,
            chrom,
            tf,
            args["fimo-dir"],
            chromsizes[chrom],
            args["window_size"]
            )

    # Write output
    out_fn = "{}_{}".format(str(idx), args["outname"])
    with gzip.open(out_fn, "wt") as out_link:
        all_df.to_csv(out_link, sep="\t", index=False)

if __name__ == "__main__":
    sys.exit(main())

