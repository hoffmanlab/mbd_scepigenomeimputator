# aggregate.R ##################################################################
description <- "Grabs the percentage of each file from the input bed files"

library(optparse)
library(stats)

DATA_COL <- 4
# parse command line arguments #################################################
if (!interactive()) {
    option.list <- list(
        make_option(
            c("-o", "--output"),
            type="character",
            default=NA,
            help = "output prefix to write the final files to",
            metavar="character"
            )
        )
    opt.parser <- OptionParser(option_list = option.list)
    opt <- parse_args(opt.parser, positional_arguments = TRUE)
    out.fn <<- opt$output
    in.fns <<- opt$args
}

# Aggregate input ##############################################################
one_hot_encode <- function(data) {
    # Turn the input data column into a one-hot encoded matrix
    # data is the input data column
    out <- model.matrix( ~.-1, as.data.frame(as.factor(data)))
    colnames(out) <- levels(data)
    return(out)
}

safe_read_table <- function(fn) {
    # Read table, handling gzipped files
    if (endsWith(fn, ".gz")) {
        in.fn <- gzfile(fn)
    }
    return(
        read.table(
            in.fn, 
            stringsAsFactors=TRUE,
            colClasses = c(character(), integer(), integer(), character())
            )
        )
}

safe_add_table <- function(a, b) {
    all.names <- unique(c(colnames(a), colnames(b)))
    a[, all.names[!(all.names %in% colnames(a))]] <- 0
    b[, all.names[!(all.names %in% colnames(b))]] <- 0
    a <- a[, all.names]
    b <- b[, all.names]
    return(a+b)
}

# Read the first file and keep all of it
in.file <- in.fns[[1]]
in.fns <- in.fns[2:length(in.fns)]
all.data <- safe_read_table(in.file)
positions <- all.data[,1:3]
all.data <- one_hot_encode(all.data[,DATA_COL])

# Read files sequentially to avoid running into memory issues
for (in.fn in in.fns) {
    print(in.fn)
    next.bed <- one_hot_encode(safe_read_table(in.fn)[, DATA_COL])
    if (nrow(next.bed) != nrow(all.data)) {
        stop(sprintf("%s does not have the same number of rows as every other input file (%d)", in.fn, nrow(all.data)))
    }
    all.data <- safe_add_table(all.data, next.bed)

    rm(next.bed)
    gc()
    }
combined.data <- cbind(positions, all.data)
write.table(
    combined.data,
    out.fn,
    col.names=TRUE,
    row.names=FALSE,
    sep="\t",
    quote=FALSE
    )
