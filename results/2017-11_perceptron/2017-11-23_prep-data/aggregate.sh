#!/bin/bash
#$ -cwd
#$ -q hoffmangroup
#$ -l h_vmem=16G
#$ -l mem_requested=16G
#$ -N aggregate
# Aggregates the results of windower.sh

# verbose failures
set -o nounset -o pipefail -o errexit

IN_DIR=($1/*.gz)

FIRST_FILE=${IN_DIR[1]}
gunzip -cd ${FIRST_FILE} | cut -f 1,2,3 - >reduced_scores.tsv
SCORE_FILES=`cut -f 1 $MAPPING_FILE`
for f in $SCORE_FILES; do
    echo ${f}.score.bed
    gunzip -cd ${f} | cut -f 4 - | paste reduced_scores.tsv - >reduced_scores_tmp.tsv
    mv reduced_scores_tmp.tsv reduced_scores.tsv
done
