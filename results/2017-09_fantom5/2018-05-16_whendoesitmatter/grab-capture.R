# grab-capture.R ###############################################################
# Graph the average position of peaks around each point

library(Cairo)
library(ggplot2)

# Constants
BED.HEADER <- c("chr", "start", "end", "label")
RESOLUTION <- 100
PLOT.WIDTH <- 200
PLOT.HALFWIDTH <- PLOT.WIDTH %/% 2

# Input files (to move to argparser)
score.fn <- "peaks-of-interest.bed"
dnase.fn <- "103-diffs.bed"
TO.FIND <- c(0)
out.fn <- "RegPermissive.png"
plot.titles <- "Enhancers found by DNase peak presence"

# Read input files #############################################################
safe.read.bed <- function(filename) {
    # Read a bed file, handling gzipped files safely and applying a header
    fh <- filename
    if (endsWith(fh, ".gz")) {
        fh <- gzfile(fh)
    }

    dt <- read.table(fh)
    colnames(dt) <- BED.HEADER[1:ncol(dt)]
    return(dt)
}
dnase <- safe.read.bed(dnase.fn)
score <- safe.read.bed(score.fn)

dnase$label <- 1
for (i in 1:length(TO.FIND)) {
    print(out.fn[i])

    # Grab center locations of every enhancer
    #enh.locs <- score[score$label == TO.FIND[i],]
    enh.locs <- score
    enh.locs$center <- enh.locs$start + (enh.locs$end - enh.locs$start) %/% 2

    # Plot the local DNase signal around these points (going chr by chr)
    plot.offsets <- seq(-PLOT.HALFWIDTH, PLOT.HALFWIDTH, RESOLUTION)
    plot.index.width <- length(plot.offsets)
    dnase.scores <- matrix(list(), ncol=plot.index.width, nrow=1)
    for (chr in unique(enh.locs$chr)) {
        print(chr)
        these.enh <- enh.locs[enh.locs$chr == chr, ]

        these.dnase <- dnase[dnase$chr == chr, ]

        # At every point on the plot...
        for (j in 1:plot.index.width) {
            to.find <- these.enh$center - plot.offsets[j]
            # Turn to.find into its binned location
            to.find <- (to.find %/% RESOLUTION) * RESOLUTION
            to.grab <- these.dnase$start %in% to.find
            local.scores <- c(sum(to.grab), rep(0, length(to.find)-sum(to.grab)))
            dnase.scores[[j]] <- c(dnase.scores[[j]], local.scores)
        }
    }

    # Average out the scores we just got
    score.means <- c()
    score.var <- c()
    for (j in 1:plot.index.width) {
        score.means <- c(score.means, mean(dnase.scores[[j]]))
        score.var <- c(score.var, var(dnase.scores[[j]]))
    }

    # Plot it with ggplot2
    plot.data <- data.frame(dist = plot.offsets, dnase = score.means)
    p <- ggplot(data = plot.data, aes( x = dist, y = dnase ))
    p <- p + geom_line()
    p <- p + scale_y_continuous(limits = c(0, 1))
    p <- p + labs(
        title=plot.titles[i],
        x="Distance from random midpoint (bp)",
        y = "Percentage of enhancers with DNase peak"
        )
    p <- p + theme_classic(base_size = 32)

    CairoPNG(out.fn[i], width=900, height=900)
    print(p)
    dev.off()
}
