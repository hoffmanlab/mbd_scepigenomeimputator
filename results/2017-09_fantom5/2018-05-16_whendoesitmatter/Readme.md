# Aim 

The purpose of this analysis is to figure out when the presence of an ATAC-seq peak matters to the output of the analysis. 

Let's think through how to do this: 
- We've got a run that we know _depends_ on the presence of ATAC-seq peaks 
- Let's take a list of what the ATAC-seq peaks are relative to the annotation centre for each 
- Determine what combinations of peaks would result in one vs the other 

Assuming one peak, we could scan across the region? No that analysis would be both not indicative of what's going on _and_ too expensive. 

# Question 1:
How about we take all of the true hits from before and see where the proximal peaks are? And then we can try ablating them to see whether or not it's indicative or not 


http://journals.plos.org/plosone/article/file?id=10.1371/journal.pone.0041882&type=printable 

It seems like we might need to perform micro-averaging in order to calculate the MCC/F1 score 

But first I need to copy over all of the code necessary to run the analysis in the first place 

Ok so I've found the runs I last did (102/ and 103/) 
Inside I can tell the name of the file (from the .e file) and I've got the disqualification amounts inside 
So it's just a matter of finding the original DNase-seq file 

102 -> ENCFF345YDG.bed -> T-cells 
103 -> ENCFF231QBI.bed -> Pancreas 

Let's focus on 102 for now. 
In 102, we start by looking for every called RegPermissive (regardless of correctness) 
My kernel for this function was about 5000bp so... we only have to consider things that are +/- 5000bp of each region 
We'll start by grepping out RegPermissive, then slopping it 5000bp, then intersect with the original. Let's do rectangular because trapezoidal hurts my head 


~~~
grep -P "\tRegPermissive" $indir/all-rectangular.bedGraph >${inprefix}-rect.bed 
bedtools slop -b 5000 -i ${inprefix}-rect.bed -g ../hg19.chrom.sizes.sorted.txt ${inprefix}-slop.bed 
bedtools intersect -a ${inbed} -b ${inprefix}-slop.bed >${inprefix}-capture.bed 
~~~

Uhhhh ok so that gets every peak that's around everything, but it's also not terribly useful... what was I going for again?
How about instead of that I do another look at the aggregate total vs centers? 
That'd mean going through the endings again?

For future me: I'm now just going to try to answer Dr. He's question of "when does the dnase peak matter" by looking at which locations require a peak _at that position_ to change between any of the three states. 
To do this, I looked at all places where it could vary (via looking at the Slidebase files), and merging them. 
Then doing a bedtools intersect across each of them into voteratio/all.bed. 
(This is all in `find-vote-ratios.sh`)
Whenever this finishes, we'll just need to do a good ol' sort -k1,1 -k2,2n, or otherwise just stick it in R and see the ratios 

Ok! Found all of the spots that are different. 
Now I'm going to figure out what the misclassification across my four left out spots to figure out what's going on. 

# Question 2
How far around each peak do I have to look in order to get a unique ATAC-seq profile for all of the peaks that agree? 

Ok so for this question I'm going to need to load every chrom access profile into R? 
I'm recording my steps in find-smallest-unique-region.R 

Made a .sh file that parallelizes the thing, and then all of the results (including further analysis and stuff) are in smallest-unique-region/ 

![plot](2018-05-30-Smallest-unique-region.png) 

* Median size: 20,300bp
* Max: 1,955,100bp
* Min: 0bp


