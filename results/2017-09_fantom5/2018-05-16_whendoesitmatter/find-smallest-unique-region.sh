#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

IDX=$((SGE_TASK_ID - 1))
START_IDX=$((IDX * 10 + 1))
END_IDX=$(((IDX+1) * 10))

module load R;

Rscript find-smallest-unique-region.R --start $START_IDX --end $END_IDX
