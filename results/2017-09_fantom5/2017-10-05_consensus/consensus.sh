#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

# Create the first one
FIRST_FILE=`ls binned-scores/*.bed.gz | head -n 1`
gunzip -cd $FIRST_FILE | cut -f 1,2,3 >consensus.bed
for i in binned-scores/*.bed.gz; do
    gunzip -cd $i | cut -f 4 | paste consensus.bed - >consensus.bed.tmp
    mv consensus.bed.tmp consensus.bed
done
