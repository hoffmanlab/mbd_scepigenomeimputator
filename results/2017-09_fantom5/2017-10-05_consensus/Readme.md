# Consensus test 

## Analysis
Given the extremely high auPR of my last attempt, I'm trying to see what the Cohen's Kappa and PRC would be from a simple consensus vote. 

Used consensus.R/consensus.sh, all of the params are hard-coded. Using the same binned-scores/binned-data from before 


## Results
So, bad news: auPR went _down_ relative to my other analysis (with the local chromatin-accessibility matching). 

### New data

~~~
> cohen.matrix 

        1    4 

1 1616133  106 

4    4125 3540 

> kappa[["kappa"]] 

[1] 0.6248 
~~~

![pr1](2017-10-consensus_CAGESignal_ROC-PRC.png) 
![pr2](2017-10-consensus_NoCAGESignal_ROC-PRC.png)

### For reference: old PR curves
![oldpr1](../2017-09-18_fantom5-TSS/2017-10-05_CAGESignal_ROC-PRC.png) 
![oldpr2](../2017-09-18_fantom5-TSS/2017-10-05_NoCAGESignal_ROC-PRC.png)
