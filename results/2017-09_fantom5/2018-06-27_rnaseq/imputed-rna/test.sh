#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

ID=$((SGE_TASK_ID - 1))

IN_DIR=(./*.gz)
IN_FILE=${IN_DIR[$ID]}
TMP_FILE=${IN_FILE}.tmp
gzip -cd $IN_FILE | sed -r "s/ +/\t/g" >$TMP_FILE
gzip -c $TMP_FILE >$IN_FILE
