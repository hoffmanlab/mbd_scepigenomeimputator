#!/usr/bin/env python
import argparse
import itertools
import os
import os.path
import sys
import tempfile

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pybedtools
import rpy2.robjects as ro
import sklearn
from rpy2.robjects.packages import importr
from rpy2.robjects import pandas2ri
from sklearn import svm, ensemble, neighbors, naive_bayes, semi_supervised
from sklearn import discriminant_analysis, linear_model, neural_network
from sklearn.externals import joblib
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import precision_recall_curve, average_precision_score
from sklearn.preprocessing import Imputer

SCORE_MAPPING = {
    "1_Quiescent": 1,
    "2_Transcribed": 2,
    "3_RegPermissive": 3
    }
CLASS_NAMES = [
    'Quiescent',
    'TSS',
    'Enhancer'
    ]
MODEL_SVC = 0
MODEL_EXTRA_TREES = 1
MODEL_RANDOM_FOREST = 2
MODEL_KNN = 3
MODEL_BERNOULLI_NB = 4
MODEL_GAUSSIAN_NB = 5
MODEL_LABEL_PROP = 6
MODEL_LABEL_SPREADING = 7
MODEL_LINEAR_DISCRIM = 8
MODEL_LOG_REGRESSION = 9
MODEL_MLP = 10

def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Analysis with sklearn
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "score_folder",
        help="""Annotation folder"""
        )
    parser.add_argument(
        "varying_locations",
        help="""bed file of locations to learn from"""
        )
    parser.add_argument(
        "data_folder",
        help="""Folder of ATAC/DNase-seq runs"""
        )
    parser.add_argument(
        "mapping",
        help="""Mapping file"""
        )
    parser.add_argument(
        "feature_width",
        type=int,
        default=20000,
        help="""Area (in bp) around features to consider"""
        )
    parser.add_argument(
        "--start-idx",
        type=int,
        default=0,
        help="""Which line of the mapping file to start operating on"""
        )
    parser.add_argument(
        "--end-idx",
        type=int,
        default=-1,
        help="""Which line of the mapping file to end operating on (-1 for all)"""
        )
    parser.add_argument(
        "--by",
        type=int,
        default=1,
        help="""How many lines of the mapping file to do at once"""
        )
    parser.add_argument(
        "--processing-dir",
        default='processed/',
        help="""Directory to place intermediate files in (enables caching)"""
        )
    parser.add_argument(
        "--output-model-fn",
        default="model.pkl",
        help="Output joblibbed model filename"
        )
    parser.add_argument(
        "--train-only",
        help="""Only generate training files""",
        action='store_true'
        )
    parser.add_argument(
        "--validation",
        help="""Validation mapping file"""
        )
    parser.add_argument(
        "--kernel",
        type=int,
        default=0,
        help="""kernel selection
        0 = SVC (default)
        1 = ExtraTrees
        2 = RandomForest
        3 = KNN
        4 = BernoulliNaiveBayes
        5 = GaussianNaiveBayes
        6 = LabelPropagation
        7 = LabelSpreading
        8 = LinearDiscriminantAnalysis
        9 = LogisticRegression
        10 = MultilayerPerceptron
        """
        )
    parser.add_argument(
        "--output-fn",
        default="PRC.png",
        help="""Output plot filename"""
        )
    parser.add_argument(
        "--num-jobs",
        default=4,
        help="""Number of cores to use during training"""
        )
    parser.add_argument(
        "--rna-dir",
        help="""Directory with RNA-seq files"""
        )
    args = parser.parse_args()

    # Validity checking: make sure either both start & end idx specified, or
    # neither
    if ((args.start_idx is not None or args.end_idx is not None)
            and not (args.start_idx  is not None and args.end_idx is not None)):
        stop("--start-idx requires --end-idx to be specified and vice versa")
    if not os.path.exists(args.processing_dir):
        os.makedirs(args.processing_dir)
    if not args.kernel <= 10 and args.kernel >= 0:
        stop("--kernel must be between 0 and 10")

    return args

def read_mapping(fn_mapping):
    """
    Reads the mapping file
    """
    return pd.read_csv(
            fn_mapping,
            sep = "\t",
            header = None
            )

def _truncate_score(feature):
    """
    Truncates an annotation BedTool that has been intersected.
    Meant to be used with pybedtools.BedTool.each()
    """
    annot = feature[-1].split(",")
    annot.sort(reverse=True)
    feature[3] = SCORE_MAPPING[annot[0]]
    return feature[0:4]

def read_data(mapping, data_folder, score_folder, features_fn, rna_folder=None):
    """
    Reads both the chromatin accessibility, rna-seq, and score file paths.

    Returns (cell_types, chrom_access, rna_seq, score_per_pos),
    where cell_types is a list of the cell types,
    chrom_access is a list of chromatin accessibility BedTool objects
    rna_seq is a list of RNA-Seq BedTool objects (or NA if none were found)
    and score_per_pos is a list of FANTOM5 annotations.
    """
    features = pybedtools.BedTool(features_fn)

    # For each line in the mapping file...
    cell_types = []
    chrom_access = []
    rna_seq = []
    score_per_pos = []
    for index, sample in mapping.iterrows():
        cell_types.append(sample[0])

        # Read in the dnase track
        data_path = os.path.join(data_folder, sample[0]+".bedGraph.gz")
        chrom_access.append(pybedtools.BedTool(data_path))

        # Read in the rna-seq track
        if rna_folder:
            data_path = os.path.join(rna_folder, "{}.bedGraph.gz".format(sample[0]))
            if os.path.isfile(data_path):
                rna_seq.append(pybedtools.BedTool(data_path))
            else:
                rna_seq.append(None)

        # Read in the score for each file & cut to points of interest
        processed_score_fn = os.path.join("cut-score", "{}.tmp.bed".format(sample[0]))
        try:
            # Load if already generated
            score_per_pos.append(pybedtools.BedTool(processed_score_fn))
        except:
            # Regenerate if not
            score_path = os.path.join(score_folder, sample[3]+".score.bed.gz")
            score = pybedtools.BedTool(score_path)
            score = features.intersect(score, wa=True, wb=True, sorted=True).sort()
            score = score.merge(d=1, c=7, o="distinct", delim=",")
            score = score.each(_truncate_score).saveas(processed_score_fn)
            score_per_pos.append(score)

        # Read in the rna-seq track
    return (cell_types, chrom_access, rna_seq, score_per_pos)

def construct_windows(feature, window_length, feature_length):
    """
    Constructs windows upstream, overlapping, and downstream of the feature given
    """
    chrom = feature[0]
    start = int(feature[1])
    end = int(feature[2])
    num_windows = feature_length // window_length
    # Generate the downstream windows
    windows_str = ""
    start_pos = max(start%window_length, start-feature_length) # Prevent negative windows
    for i in range(start_pos, start, window_length):
        windows_str += "{}\t{}\t{}\n".format(chrom, i, i+window_length-1)
    windows_str += "{}\t{}\t{}\n".format(chrom, start, end)
    for i in range(end+1, end+feature_length+1, window_length):
        windows_str += "{}\t{}\t{}\n".format(chrom, i, i+window_length-1)
    return(pybedtools.BedTool(windows_str, from_string=True))

def _normalize_score(feature):
    """
    Normalizes the score counts from the given window intersections.
    Meant to be used with pybedtools.BedTool.each()
    """
    if feature[-1] == ".":
        feature[-1] = "0"
    else:
        feature[-1] = str(float(feature[-1]) / (int(feature[-2]) - int(feature[-3])))
    return(feature)

def _construct_track(X, X_row, X_col_start, windows_bedtool, track_bedtool,
        expected_length):
    """
    Windows the given track_bedtool and normalizes the signal.
    """
    windowed_track = windows_bedtool.intersect(track_bedtool,
            wb=True,
            wa=True,
            loj=True,
            sorted=True
            )
    windowed_track = windowed_track.sort().each(_normalize_score)
    windowed_track = windowed_track.merge(d=0, c=7, o="sum")

    this_x = [float(f[-1]) for f in windowed_track]

    # If windows is too small, e.g. at the start of chromosomes,
    # prepend with zeros
    to_fill_start = expected_length - len(this_x) + X_col_start
    to_fill_end = to_fill_start + len(this_x)
    X[X_row, to_fill_start:to_fill_end] = this_x

def construct_training_cell_type(chrom_access, scores, window_length,
        feature_width, cell_type, start_idx=0, end_idx=-1, rna_seq=None):
    """
    Generate the training dataset for a given input cell type.
    Negative end_idx implies to do the entire training set

    Returns (X,y), where X is a numpy array of shape (num_features, num_obs)
    and y a numpy array of shape (1, num_obs) containing the signal and the
    output classes respectively
    """

    # Run on all rows if end_idx negative
    if end_idx < 0:
        end_idx = len(scores[cell_type])

    # Determine how large our output matrix will be
    features_per_track = feature_width//window_length*2+1
    total_features = features_per_track
    if rna_seq:
        total_features += features_per_track

    X = np.zeros((end_idx-start_idx, total_features))
    y = np.zeros((end_idx-start_idx, 1))

    for position in range(start_idx, end_idx):
        # Obtain the score at this position
        observation = scores[cell_type][position]
        y[position-start_idx] = int(observation.name)

        # Obtain the normalized chrom accessibility around this position
        windows = construct_windows(observation, window_length, feature_width)
        _construct_track(
                X,
                position-start_idx,
                0,
                windows,
                chrom_access[cell_type],
                features_per_track
                )

        # If RNA-seq data is available, include it as further data
        if rna_seq:
            # Avoid using if rna_seq[cell_type] due to the large evaluation
            # time of rna_seq[cell_type]
            if rna_seq[cell_type] is not None:
                _construct_track(
                        X,
                        position-start_idx,
                        features_per_track,
                        windows,
                        rna_seq[cell_type],
                        features_per_track
                        )
            else:
                # RNA-seq data missing
                this_rna = np.tile(np.nan, features_per_track)
                X[position-start_idx, features_per_track:total_features] = \
                    np.tile(np.nan, features_per_track)

        pybedtools.helpers.cleanup()

    # Save the training set to the given filename
    return (X, y)

def construct_training(chrom_access, scores, window_length, feature_width,
        cell_types, cache_directory, rna_seq=None, start_idx=0, end_idx=-1, by=1):
    """
    Generate the training dataset from given feature BedTool and chromatin
    accessibility BedTools files.
    If a negative end_idx is given, it will run on all rows of the input file.

    Returns (X,y), where X is a numpy array of shape (num_features, num_obs)
    and y a numpy array of shape (1, num_obs) containing the signal and the
    output classes respectively
    """

    # Run on all rows if end_idx negative
    if end_idx < 0:
        end_idx = len(scores[0])

    # Determine how large our output matrix will be
    num_entries = (end_idx-start_idx)*len(cell_types)
    features_per_obs = feature_width//window_length*2+1
    total_features = features_per_obs
    if rna_seq:
        total_features += features_per_obs

    X = np.empty(shape=(num_entries, total_features))
    y = np.empty(shape=(num_entries, 1))

    for i in range(start_idx, end_idx, by):
        next_idx = i+by
        thispos = (i - start_idx)*len(cell_types)
        nextpos = (next_idx - start_idx)*len(cell_types)
        X_filename = "{}-{}_X_formatted.npy".format(i, next_idx)
        X_filename = os.path.join(cache_directory, X_filename)
        y_filename = "{}-{}_y_formatted.npy".format(i, next_idx)
        y_filename = os.path.join(cache_directory, y_filename)

        # Try to load the training set from cache
        try:
            with open(X_filename) as X_file:
                X[thispos:nextpos,:] = np.load(X_file)
            with open(y_filename) as y_file:
                y[thispos:nextpos,:] = np.load(y_file)
        except IOError: # File not found
            # Generate training set for each cell type
            for (j, cell_type) in enumerate(cell_types):
                (X_np, y_np) = construct_training_cell_type(chrom_access, scores,
                        window_length, feature_width, j, i, next_idx, rna_seq=rna_seq)
                X[thispos+j,:] = X_np
                y[thispos+j,:] = y_np

            # Cache the training set that was just generated
            np.save(X_filename, X[thispos:nextpos, :])
            np.save(y_filename, y[thispos:nextpos, :])
    return(X, y)

def train(X, y, model_type):
    """
    Train the scikit-learn model using GridSearch, for the given model type.
    Returns a trained scikit-learn GridSearchCV object.
    """
    y = np.ravel(y)
    model = None
    parameters = {}
    n_jobs=4
    if (model_type == MODEL_SVC):
        model = svm.LinearSVC()
        parameters = [
            {'penalty': ['l1'],
                'loss': ['squared_hinge'],
                'multi_class': ['ovr', 'crammer_singer'],
                'dual': [False]
            },
            {'penalty': ['l2'],
                'loss': ['hinge', 'squared_hinge'],
                'multi_class': ['ovr', 'crammer_singer']
            }]
    elif (model_type == MODEL_EXTRA_TREES):
        model = ensemble.ExtraTreesClassifier()
        parameters['n_estimators'] = [5, 10, 100]
        parameters['criterion'] = ['gini', 'entropy']
        parameters['max_depth'] = [3, None]
        parameters['max_features'] = ['sqrt', 'log2', None]
    elif (model_type == MODEL_RANDOM_FOREST):
        model = ensemble.RandomForestClassifier()
        parameters['n_estimators'] = [5, 10, 100]
        parameters['criterion'] = ['gini', 'entropy']
        parameters['max_depth'] = [3, None]
        parameters['max_features'] = ['sqrt', 'log2', None]
    elif (model_type == MODEL_KNN):
        model = neighbors.KNeighborsClassifier()
        parameters['n_neighbors'] = [5, 10, 20]
        parameters['weights'] = ['uniform', 'distance']
        parameters['p'] = [1, 2]
    elif (model_type == MODEL_BERNOULLI_NB):
        model = naive_bayes.BernoulliNB()
        parameters['alpha'] = [1, 0.5, 0.1]
        parameters['binarize'] = [0.75, 0.5, 0.25]
    elif (model_type == MODEL_GAUSSIAN_NB):
        model = naive_bayes.GaussianNB()
    elif (model_type == MODEL_LABEL_PROP):
        model = semi_supervised.LabelPropagation()
        parameters = [
            {'kernel': ['knn'],
                'gamma': [10, 20, 100]
            },
            {'kernel': ['rbf'],
                'n_neighbors': [5, 7, 9]
            }]
    elif (model_type == MODEL_LABEL_SPREADING):
        model = semi_supervised.LabelSpreading()
        parameters = [
            {'kernel': ['knn'],
                'gamma': [10, 20, 100]
            },
            {'kernel': ['rbf'],
                'n_neighbors': [5, 7, 9]
            }]
    elif (model_type == MODEL_LINEAR_DISCRIM):
        model = discriminant_analysis.LinearDiscriminantAnalysis()
        parameters = [
                {'solver': ['svd'],
                    'shrinkage': [None]
                },
                {'solver': ['lsqr', 'eigen'],
                    'shrinkage': [None, 'auto']
                }]
    elif (model_type == MODEL_LOG_REGRESSION):
        model = linear_model.LogisticRegressionCV()
        parameters = [
            {'Cs': [1, 2, 3],
                'penalty': ['l1'],
                'solver': ['saga']
            },
            {'Cs':  [1, 2, 3],
                'penalty': ['l2'],
                'solver': ['lbfgs']
            }]
    elif (model_type == MODEL_MLP):
        model = neural_network.MLPClassifier()
        parameters['hidden_layer_sizes'] = [
            #tuple(50 for i in range(2)),
            #tuple(100 for i in range(2)),
            #tuple(50 for i in range(5)),
            #tuple(100 for i in range(5)),
            tuple(50 for i in range(10)),
            tuple(100 for i in range(10))
            #tuple(50 for i in range(50)),
            #tuple(100 for i in range(50))
            ]
        parameters['activation'] = ['logistic', 'tanh', 'relu']
        parameters['alpha'] = [0.0001, 0.001, 0.01]
        n_jobs=32
    clf = GridSearchCV(model, parameters, n_jobs=4)
    clf.fit(X, y)
    print("Optimal parameters:")
    print(clf.best_params_)
    return(clf)

def one_hot_encode(y, num_classes=-1):
    """
    Converts a 1d array into a one-hot encoded array
    """
    if num_classes < 0:
        num_classes = int(max(y))
    zero_encoded_y = y.astype(int).ravel()-1
    one_hot = np.zeros((len(y), num_classes))
    one_hot[np.arange(len(y)), zero_encoded_y] = 1
    return(one_hot)

def _calculate_roc_prc(labels, score):
    # Use precrec due to the sklearn library having a bug in auPRC calc
    # First convert true labels & score into string vectors
    labels_str = np.char.mod("%d", labels)
    labels_str = ",".join(labels_str)
    score_str = np.char.mod("%f", score)
    score_str = ",".join(score_str)

    # Run precrec on these vectors
    importr("precrec")
    ro.r("sscurves = evalmod(scores=c({}), labels=c({}))".format(
        score_str,
        labels_str
        ))

    # Retrieve the auc
    auc_df = ro.r("auc_df = auc(sscurves)")
    auc_df = pandas2ri.ri2py_dataframe(auc_df)

    # Convert the ROC&PRC into vectors
    prc_df = ro.r("sscurves$prcs[[1]]")
    roc_df = ro.r("sscurves$rocs[[1]]")
    return(
        np.asarray(roc_df[0]),  # False Positives
        np.asarray(roc_df[1]),  # True Positives
        np.asarray(prc_df[0]),  # Recall
        np.asarray(prc_df[1]),  # Precision
        auc_df.iloc[0,-1], # auROC
        auc_df.iloc[1,-1], # auPRC
        )

def _plot_curve(ax, x, y, label_text, micro_avg_text, xlabel, ylabel,
        bbox_to_anchor):
    # Plot the given curve
    lines = []
    labels = []

    # Plot each class seperately
    for i, color in zip(range(3), colors):
	l, = ax.plot(roc_fp[i], roc_tp[i], color=color, lw=2)
	lines.append(l)
	labels.append(label_text.format(CLASS_NAMES[i], au_roc[i]))

    l, = ax.plot(roc_fp["micro"], roc_tp["micro"], color='gold', lw=2)
    lines.append(l)
    labels.append(micro_avg_text.format(au_roc["micro"]))
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_ylim([0.0, 1.0])
    ax.set_xlim([0.0, 1.0])

    # Shrink current axis's height by 30% on the bottom
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.3,
                         box.width, box.height * 0.7])

    # Put a legend below current axis
    ax.legend(lines, labels, loc='upper center', bbox_to_anchor=bbox_to_anchor,
                      fancybox=True)

def plot_all(output_fn, roc_fp, roc_tp, recall, precision, au_roc, au_pr,
        class_pct, dpi=100):
    """
    Plot the roc/auc, saving it as output_fn
    """

    # Set up pyplot
    plt.switch_backend('agg')
    colors = itertools.cycle(['navy', 'turquoise', 'darkorange'])
    plt.figure(figsize=(16, 8))
    font = {'size'   : 20}
    plt.rc('font', **font)

    # Plot the ROC curve and random-case line
    ax_roc = plt.subplot(1, 2, 1)
    _plot_curve(
        ax_roc,
        roc_fp,
        roc_tp,
        'ROC Curve for class {0} (area = {1:0.2f})',
        'micro-average ROC curve (area = {0:0.2f})',
        'False Positive',
        'True Positive',
        (0.45, -0.15)
        )
    ax_roc.plot([0,1], [0,1], 'k:', alpha=0.75, zorder=0)

    # Plot the PR curve and random-case lines per class
    ax_pr = plt.subplot(1, 2, 2)
    _plot_curve(
        ax_pr,
        recall,
        precision,
        'PR Curve for class {0} (area = {1:0.2f})',
        'micro-average PR curve (area = {0:0.2f})',
        'Recall',
        'Precision',
        (0.55, -0.15)
        )
    for i, color in zip(class_pct, colors):
        ax_pr.axhline(i, 0.0, 1.0, linestyle=':', alpha=0.75, zorder=0, color=color)

    plt.suptitle('3-class ROC/PR curves: micro-average auPR={0:0.2f}\n{1}'.format(
                  au_pr['micro'], str(model.best_params_)))

    fig1 = plt.gcf()
    fig1.savefig(output_fn, dpi=dpi)

def validate(model, X, y, kernel):
    """
    Evaluate the input scikit-learn model's performance.
    Returns (roc_fp, roc_tp, recall, precision, au_roc, au_pr, class_pct)
    which are numpy arrays with the ROC curve, PR curve, auROC, auPR, and the
    class balance percentages respectively
    """
    # Run the validation
    score = None
    if (kernel == MODEL_SVC):
        score = model.decision_function(X)
    else:
        score = model.predict_proba(X)

    # Convert y to one-hot encoding
    y = one_hot_encode(y, 3)

    # Calculate PR per class
    precision = dict()
    recall = dict()
    roc_fp = dict()
    roc_tp = dict()
    au_roc = dict()
    au_pr = dict()
    for i in range(0,3):
        (roc_fp[i], roc_tp[i], recall[i], precision[i],
                au_roc[i], au_pr[i]) = _calculate_roc_prc(y[:, i], score[:, i])

    # Calculate the class balance
    class_pct = np.sum(y, axis=0) / np.sum(y, axis=None)

    # Calculate the micro-average, which quantifies score on all classes jointly
    (roc_fp['micro'], roc_tp['micro'], recall['micro'], precision['micro'],
        au_roc['micro'], au_pr['micro']) = _calculate_roc_prc(y.ravel(), score.ravel())

    return (roc_fp, roc_tp, recall, precision, au_roc, au_pr, class_pct)


# MAIN #########################################################################
def main():
    args = parse_args()
    mapping = read_mapping(args.mapping)

    # Create a temporary directory
    tmpdir = tempfile.mkdtemp()
    pybedtools.set_tempdir(tmpdir)

    # Extract features
    (cell_types, chrom_access, rna_seq, scores) = read_data(
            mapping,
            args.data_folder,
            args.score_folder,
            args.varying_locations,
            rna_folder=args.rna_dir
            )

    # Generate the training set
    (X, y) = construct_training(
            chrom_access,
            scores,
            100,
            args.feature_width,
            cell_types,
            args.processing_dir,
            start_idx=args.start_idx,
            end_idx=args.end_idx,
            by=args.by,
            rna_seq=rna_seq
            )

    # If this is a batch job, stop here
    if args.train_only:
        return

    model = train(X, y, args.kernel)
    joblib.dump(model, output_model_fn)

    # Validate & plot if we have the validation set
    if args.validation:
        v_mapping = read_mapping(args.validation)
        (v_cell_types, v_chrom_access, v_rna_seq, v_scores) = read_data(
                v_mapping,
                args.data_folder,
                args.score_folder,
                args.varying_locations,
                rna_folder=args.rna_dir
                )
        (val_X, val_y) = construct_training(
                v_chrom_access,
                v_scores,
                100,
                args.feature_width,
                v_cell_types,
                "validation/",
                start_idx=args.start_idx,
                end_idx=args.end_idx,
                by=args.by,
                rna_seq=v_rna_seq
                )
        (roc_fp, roc_tp, recall, precision, au_roc, au_pr, class_pct) = \
                validate(model, val_X, val_y, args.kernel)
        plot_all(args.output_fn, roc_fp, roc_tp, recall, precision, au_roc,
                au_pr, class_pct)

if __name__ == "__main__":
    sys.exit(main())
