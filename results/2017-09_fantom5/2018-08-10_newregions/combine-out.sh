#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

IN_DIR=${1:-out/}
START_IDX=${2:-1}
END_IDX=${3:-249}

# Combine all of the files together chronologically
for i in `seq $START_IDX $END_IDX`; do
    cat $IN_DIR/$((i*10000))-$(((i+1)*10000))-best.bed >>best-combined.bed
done
#FILES=`echo $IN_DIR/*-raw.bed | tr " " "\n" | sort -n | tr "\n" " "`
#2490000-2500000-best.bed
#cat $FILES >raw-combined.bed

# Convert the new file into bed12 format, formatted based on the total 
