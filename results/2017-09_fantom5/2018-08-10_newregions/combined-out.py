#!/usr/bin/env python
import argparse
import os
import sys

OUT_HEADER = 'track autoscale="off" description="RF Classifier best guess" name=classifier.out viewLimits=0:1 visibility=dense itemRgb=on'

# PARSE ARGS ###################################################################
def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Reduce columns in the given input file into a less memory
    intensive form
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "in_dir",
        help = """Directory of classify-new-out.sh output files to combine"""
        )
    parser.add_argument(
        "out_file",
        help = """output .bed file name"""
        )
    return parser.parse_args()

def convert_line(line):
    """Convert this line into a new bed12 format"""

    line = line_txt.split("\t")
    this_data = [float(i) for i in line[3:len(line)]]

    # Determine the best class
    highest = max(this_data)
    best_class = this_data.index(highest)

    # Determine the correct RGB value to use
    (r, g, b) = (0, 0, 0)
    if best_class == 0:
        # Queiscent: blue
        b = (highest - 0.33)/0.67 * 255
        g = (1 - highest)/0.67 * 128
        r = g
    elif best_class == 1:
        # TSS: green
        g = (highest - 0.33)/0.67 * 255
        b = (1 - highest)/0.67 * 128
        r = b
    else:
        # Enhancer: red
        r = (highest - 0.33)/0.67 * 255
        g = (1 - highest)/0.67 * 128
        b = g
    rgb = "{},{},{}".format(r, g, b)

    return("{}\t{}\t{}\t{}\t1000\t.\t{}\t{}\t{}\n".format(
        line[0],    # Chr
        line[1],    # Start
        line[2],    # End
        best_class,
        line[1],    # Thick Start
        line[2],    # Thick End
        rgb
        ))

# MAIN #########################################################################
def main():
    args = parse_args()

    # Grab in_dir/*-raw.bed
    files = [x for x in os.listdir(args.in_dir) if x.endswith("-raw.bed")]

    # Sort based on the number before the '-'
    files = files.sort(key = lambda x: int(x[0:x.index('-')]))

    # Read each file, append to output
    with open(args.out_file, 'w') as out_file:
        # Insert the header line
        out_file.write(OUT_HEADER)
        for in_file in files:
            with open(in_file) as in_fh:
                for line_txt in in_fh:
                    out_file.write(convert_line(line_txt))

if __name__ == "__main__":
    sys.exit(main())

