#!/usr/bin/env python
import argparse
import sys

# PARSE ARGS ###################################################################
def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Combines the output of classify-new.py into a single, UCSC-ready file.
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "in_folder",
        help = """Folder of classify-new.py output bed files"""
        )
    parser.add_argument(
        "--start-idx",
        help = """Line number of locations file to start at (default 0)"""
        )
    parser.add_argument(
        "--end-idx",
        help = """Line number of locations file to end at"""
        )
    return parser.parse_args()

# MAIN #########################################################################
def main():
    args = parse_args()

    # Load all the bed files together (might take forever???)
    for i in range(

if __name__ == "__main__":
    sys.exit(main())

