#!/usr/bin/env python
import argparse
import os.path
import sys
import tempfile

import numpy as np
import pybedtools
from sklearn.externals import joblib

import train

SCORE_MAPPING = {
    "1_Quiescent": 1,
    "2_Transcribed": 2,
    "3_RegPermissive": 3
    }


def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Classify new regions given a model, and measures its performance
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "model",
        help="""Joblibbed sklearn model file"""
        )
    parser.add_argument(
        "feature_width",
        help="""Width of features.""",
        type=int
        )
    parser.add_argument(
        "locations",
        help="""bed file of locations to classify, fourth column indicates
        their true state. This is generally a score file."""
        )
    parser.add_argument(
        'chrom_access',
        help="Chromatin accessibility input file to classify"
        )
    parser.add_argument(
        'rna',
        help="RNA-seq input file to classify"
        )
    parser.add_argument(
        "output_folder",
        help="Folder to output performance plots and classified locations to."
        )
    parser.add_argument(
        "--start-idx",
        help="Line number of locations file to start at (default 0)",
        default=0,
        type=int
        )
    parser.add_argument(
        "--end-idx",
        help="Line number of locations file to end at (default -1 = all)",
        default=-1,
        type=int
        )
    return parser.parse_args()


def _truncate_score(feature):
    """
    Grabs the last column (score) from a score BedTool
    Meant to be used with pybedtools.BedTool.each()
    """
    annot = feature[-1].split(",")
    if len(annot) > 1:
        annot.sort(reverse=True)
    feature[3] = SCORE_MAPPING[annot[0]]
    return feature[0:4]


def _streamed_bedtool_to_bedtool(bedtool):
    """
    Converts a streamed bedtool (like the one output from .each()) to a normal
    one, so that it doesn't get consumed on use.
    """
    # Create a temporary bedfile
    # Note that we cannot directly use the file handle from tempfile.mkstemp,
    # due to pybedtools saveas requiring a filename instead of a file handle
    _, temp_path = tempfile.mkstemp(suffix=".bed")
    bedtool.saveas(temp_path)
    return pybedtools.BedTool(temp_path)


def load_score(score_fn):
    X = pybedtools.BedTool(score_fn)
    y = _streamed_bedtool_to_bedtool(X.each(_truncate_score))
    return (X, y)


def construct_and_predict(model, chrom_access, scores, window_length, feature_width,
        cell_types, cache_directory, rna_seq=None, start_idx=0, end_idx=-1):
    """
    Predict over the given bin of the genome
    """

    # Run on all rows if end_idx negative
    if end_idx < 0:
        end_idx = len(scores[0])

    # Determine how large our output matrix will be
    num_entries = (end_idx-start_idx)*len(cell_types)
    features_per_obs = feature_width//window_length*2+1
    total_features = features_per_obs
    if rna_seq:
        total_features += features_per_obs

    probs = np.empty(shape=(num_entries, 3))

    for i in range(start_idx, end_idx):
        # Generate training set for this position
        (X_np, y_np) = train.construct_training_cell_type(
            chrom_access,
            scores,
            window_length,
            feature_width,
            0,
            i,
            i+1,
            rna_seq=rna_seq
            )
        probs[i-start_idx, :] = model.predict_proba(X_np)

    return(probs)


def grabPos(feature):
    """
    Grabs the position of the input feature and puts it into a tuple
    """
    return (feature.chrom, feature.start, feature.stop)


def save_probs(folder, prefix, probs, features_fn, start_idx):
    """
    Save the probabilities to two files:
    [folder]/[prefix]-raw.bed contains the raw probabilities for each position
    [folder]/[prefix]-best.bed has the best call i nthe fourth column
    """

    num_obs = probs.shape[0]
    features = pybedtools.BedTool(features_fn)
    positions = map(grabPos, features)
    positions = np.array(
            positions
            )
    positions = positions[start_idx:(start_idx+num_obs), :]

    # Save the raw probabilities
    raw_fn = os.path.join(folder, "{}-raw.bed".format(prefix))
    probs_bed = np.concatenate(
            (positions, probs),
            axis=1
            )
    np.savetxt(raw_fn, probs_bed, fmt="%s", delimiter="\t")

    # Save the best call
    best_fn = os.path.join(folder, "{}-best.bed".format(prefix))
    best_call = np.expand_dims(np.argmax(probs, axis=1), 1)
    best_bed = np.concatenate(
            (positions, best_call),
            axis=1
            )
    np.savetxt(best_fn, best_bed, fmt="%s", delimiter="\t")


def main():
    args = parse_args()

    # Setup a temporary directory for pybedtools
    tmpdir = tempfile.mkdtemp()
    pybedtools.set_tempdir(tmpdir)

    model = joblib.load(args.model)
    (positions, real_y) = load_score(args.locations)
    chrom_access = pybedtools.BedTool(args.chrom_access)
    rna = pybedtools.BedTool(args.rna)

    (probs) = construct_and_predict(
        model,
        [chrom_access],
        [real_y],
        100,
        args.feature_width,
        ['validation'],
        'validation/',
        start_idx=args.start_idx,
        end_idx=args.end_idx,
        rna_seq=[rna]
        )

    out_prefix = "{}-{}".format(args.start_idx, args.end_idx)
    save_probs(
        args.output_folder,
        out_prefix,
        probs,
        args.locations,
        args.start_idx
        )

if __name__ == "__main__":
    sys.exit(main())
