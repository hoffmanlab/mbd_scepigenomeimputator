#!/usr/bin/env python
import argparse
import os.path
import sys
import tempfile

import numpy as np
import pybedtools
from sklearn.externals import joblib


SCORE_MAPPING = {
    "1_Quiescent": 1,
    "2_Transcribed": 2,
    "3_RegPermissive": 3
    }


def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Classify new regions given a model, and measures its performance.
    Assumes that the data spans the entire genome.
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "model",
        help="""Joblibbed sklearn model file"""
        )
    parser.add_argument(
        "feature_width",
        help="""Width of features.""",
        type=int
        )
    parser.add_argument(
        "chrom_sizes",
        help="""Either the tab-delimited chromsizes file, where one column is
        the chromosome name and the second column is the chromosome size, or
        the name of the assmebly to use (e.g. hg19)"""
        )
    parser.add_argument(
        "locations",
        help="""Score file"""
        )
    parser.add_argument(
        'chrom_access',
        help="Chromatin accessibility input file to classify"
        )
    parser.add_argument(
        'rna',
        help="RNA-seq input file to classify"
        )
    parser.add_argument(
        "output_folder",
        help="""Folder to output performance plots and classified locations to."""
        )
    parser.add_argument(
        "processing_dir",
        help="Directory to place intermediate files"
        )
    parser.add_argument(
        "--start-idx",
        help="Line number of locations file to start at (default 0)",
        default=0,
        type=int
        )
    parser.add_argument(
        "--end-idx",
        help="Line number of locations file to end at (default -1, meaning all)",
        default=-1,
        type=int
        )
    return parser.parse_args()


def _truncate_score(feature):
    """
    Grabs the last column (score) from a score BedTool
    Meant to be used with pybedtools.BedTool.each()
    """
    annot = feature[-1].split(",")
    if len(annot) > 1:
        annot.sort(reverse=True)
    feature[3] = SCORE_MAPPING[annot[0]]
    return feature[0:4]


def _streamed_bedtool_to_bedtool(bedtool):
    """
    Converts a streamed bedtool (like the one output from .each()) to a normal
    one, so that it doesn't get consumed on use.
    """
    # Create a temporary bedfile
    # Note that we cannot directly use the file handle from tempfile.mkstemp,
    # due to pybedtools saveas requiring a filename instead of a file handle
    _, temp_path = tempfile.mkstemp(suffix=".bed")
    bedtool.saveas(temp_path)
    return pybedtools.BedTool(temp_path)


def _grab_score(feature):
    """
    Returns a feature's score, meant to be used with pybedtools.BedTool.each()
    """
    return float(feature[-1])


def bedtool_to_score(bedtool):
    """
    Converts a pybedtools.BedTool object into a numpy array for ease of use
    """
    return map(_grab_score, bedtool)


def load_score(score_fn, cache_directory):
    X = pybedtools.BedTool(score_fn)
    truncated_fn = "{}.truncated_score.bed".format(os.path.basename(score_fn))
    truncated_fn = os.path.join(cache_directory, truncated_fn)
    try:
        y = pybedtools.BedTool(truncated_fn)
    except:
        y = _streamed_bedtool_to_bedtool(X.each(_truncate_score))
        y.saveas(truncated_fn)
    return (X, y)


def _normalize_score(feature):
    """
    Normalizes the score counts from the given window intersections.
    Meant to be used with pybedtools.BedTool.each()
    """
    if feature[-1] == ".":
        feature[-1] = "0"
    else:
        feature[-1] = str(float(feature[-1]) / (int(feature[-2]) - int(feature[-3])))
    return(feature)


def generate_windowed(windows, bed, chromosomes):
    """
    Generates the windowed version of the input track file
    """
    intersection = windows.intersect(
        bed,
        wa=True,
        wb=True,
        loj=True,
        sorted=True
        ).each(_normalize_score).sort()
    windowed = intersection.merge(d=0, c=7, o="sum", delim=",")
    windowed = _streamed_bedtool_to_bedtool(windowed)

    # Split the windowed bedfile into a list of BedTools, one per chromosome
    chr_windows = {}
    for chromosome in chromosomes:
        chr_windows[chromosome] = windowed.filter(lambda x: x[0] == chromosome)
        chr_windows[chromosome] = _streamed_bedtool_to_bedtool(chr_windows[chromosome])

        # For this script, we want to convert this to a numpy array for fast
        # reading
        chr_windows[chromosome] = np.asarray(bedtool_to_score(chr_windows[chromosome]))
    return(chr_windows)


def fix_end(feature):
    """
    Changes the end point to be something zero-based
    """
    feature.end = feature.end - 1
    return(feature)


def try_load_cached(cache_directory, chrom_sizes, window_length, chrom_access, rna_seq=None):
    """
    Tries to load cached, pre-binned versions of the input files
    """
    chrs = get_chromosomes()
    chrom_access_chr = {}
    rna_seq_chr = {}
    try:
        for chromosome in chrs:
            chrom_access_fn = "{}.{}.chrom.npy".format(os.path.basename(chrom_access), chromosome)
            chrom_access_fn = os.path.join(cache_directory, chrom_access_fn)
            chrom_access_chr[chromosome] = np.load(chrom_access_fn)

            if rna_seq:
                rna_fn = "{}.{}.rna.npy".format(os.path.basename(rna_seq), chromosome)
                rna_fn = os.path.join(cache_directory, rna_fn)
                rna_seq_chr[chromosome] = np.load(rna_fn)
        # Use a temp bedtool object to create genomic windows
        # windows = pybedtools.example_bedtool('a.bed').window_maker(
        #         genome = chrom_sizes,
        #         w = window_length
        #         )
        # windows = _streamed_bedtool_to_bedtool(windows.each(fix_end))
        windows = pybedtools.BedTool(chrom_sizes)
    except IOError as e:
        print("Regenerating cache...")
        # Generate windowed versions of our data bedfiles
        chrom_access_bed = pybedtools.BedTool(chrom_access)
        # windows = chrom_access_bed.window_maker(
        #         genome = chrom_sizes,
        #         w = window_length
        #         )
        # windows = _streamed_bedtool_to_bedtool(windows.each(fix_end))
        windows = pybedtools.BedTool(chrom_sizes)
        chrom_access_chr = generate_windowed(windows, chrom_access_bed, chrs)
        if rna_seq:
            rna_seq_chr = generate_windowed(windows, pybedtools.BedTool(rna_seq), chrs)

        # Cache for future use
        for chromosome in chrs:
            chrom_access_fn = "{}.{}.chrom.npy".format(os.path.basename(chrom_access), chromosome)
            chrom_access_fn = os.path.join(cache_directory, chrom_access_fn)
            np.save(chrom_access_fn, chrom_access_chr[chromosome])

            if rna_seq:
                rna_fn = "{}.{}.rna.npy".format(os.path.basename(rna_seq), chromosome)
                rna_fn = os.path.join(cache_directory, rna_fn)
                np.save(rna_fn, rna_seq_chr[chromosome])
    return(chrom_access_chr, rna_seq_chr, windows)


def grab_feature(data_track, chr_track, feature, feature_width, window_length):
    """
    Grab a feature around i, prepadding the feature if necessary
    """
    # Pre-pad, if necessary, the input track
    chrom = feature.chrom
    start = feature.start
    end = feature.end
    features_per_obs = feature_width//window_length*2+1
    if start - feature_width < 0:
        # We know that this is going to be around the edge of the chromosome
        # So, we should prepad this feature with 0s
        # Determine where to start, i.e. how far along this particular feature is
        start_idx = (feature_width - start) // window_length
        out = np.zeros(shape=(features_per_obs, 1))
        # Note: the following will fail if feature_width > the size of chromosome
        out[start_idx:features_per_obs] = np.expand_dims(
                data_track[chrom][0:(features_per_obs-start_idx)],
                1)
        print(start_idx, features_per_obs)
    else:
        start_idx = (start - feature_width) // window_length
        end_idx = (end + feature_width) // window_length + 1
        # Truncate end_idx so we handle the end position properly
        end_idx = min(end_idx, len(data_track[chrom]))
        length = end_idx-start_idx
        out = np.zeros(shape=(features_per_obs, 1))
        out[0:length] = np.expand_dims(data_track[chrom][start_idx:end_idx], 1)
    return(out)


def get_chromosomes():
    """
    Obtain a list of all of the chromosome names
    """
    chrs = ["chr{}".format(i) for i in range(1, 23)]
    chrs.append('chrX')
    chrs.append('chrY')
    return(chrs)


def construct_and_predict(model, windows, chrom_access, window_length,
        feature_width, cell_types, rna_seq=None, start_idx=0, end_idx=-1):
    """
    Predict over the given bin of the genome
    """

    # Run on all rows if end_idx negative
    if end_idx < 0:
        end_idx = len(windows)

    # Determine how large our output matrix will be
    num_entries = (end_idx-start_idx)*len(cell_types)
    features_per_obs = feature_width//window_length*2+1
    total_features = features_per_obs
    if rna_seq:
        total_features += features_per_obs

    probs = np.empty(shape=(num_entries, 3))
    X_np = np.empty(shape=(total_features, 1))
    chrs = get_chromosomes()

    # Generate the list of features beforehand, as this is expensive to perform
    features = windows[start_idx:end_idx]
    for i in range(start_idx, end_idx):
        # Generate training set for this position
        # Determine which window we are at (i)
        feature = features.next()
        X_np[0:features_per_obs] = grab_feature(
            chrom_access,
            chrs,
            feature,
            feature_width,
            window_length
            )
        if rna_seq:
            X_np[features_per_obs:total_features] = grab_feature(
                rna_seq,
                chrs,
                feature,
                feature_width,
                window_length
                )
        probs[i-start_idx, :] = model.predict_proba(X_np.T)

    return(probs)


def grab_pos(interval):
    """
    Grabs the position of the input interval and puts it into a tuple
    """
    return (interval.chrom, interval.start, interval.stop)


def save_probs(folder, prefix, probs, features_fn, start_idx, windows):
    """
    Save the probabilities to two files:
    [folder]/[prefix]-raw.bed contains the raw probabilities for each position
    [folder]/[prefix]-best.bed has the best call i nthe fourth column
    """

    num_obs = probs.shape[0]
    positions = map(grab_pos, windows)
    positions = np.array(
        positions
        )
    positions = positions[start_idx:(start_idx+num_obs), :]

    # Save the raw probabilities
    raw_fn = os.path.join(folder, "{}-raw.bed".format(prefix))
    probs_bed = np.concatenate(
        (positions, probs),
        axis=1
        )
    np.savetxt(raw_fn, probs_bed, fmt="%s", delimiter="\t")

    # Save the best call
    best_fn = os.path.join(folder, "{}-best.bed".format(prefix))
    best_call = np.argmax(probs, axis=1)
    best_bed = np.concatenate(
        (positions, best_call[:, None]),
        axis=1
        )
    np.savetxt(best_fn, best_bed, fmt="%s", delimiter="\t")


def main():
    args = parse_args()

    # Setup a temporary directory for pybedtools
    tmpdir = tempfile.mkdtemp()
    pybedtools.set_tempdir(tmpdir)

    model = joblib.load(args.model)

    # (positions, real_y) = load_score(args.locations, args.processing_dir)

    (chrom_access, rna, windows) = try_load_cached(
      args.processing_dir,
      args.chrom_sizes,
      100,
      args.chrom_access,
      args.rna
      )

    # chrom_access = pybedtools.BedTool(args.chrom_access)
    # rna = pybedtools.BedTool(args.rna)
    # chrom_access_list = bedtool_to_score(chrom_access)
    # rna_list = bedtool_to_score(rna)

    (probs) = construct_and_predict(
        model,
        windows,
        chrom_access,
        100,
        args.feature_width,
        ['validation'],
        start_idx=args.start_idx,
        end_idx=args.end_idx,
        rna_seq=rna
        )

    out_prefix = "{}-{}".format(args.start_idx, args.end_idx)
    save_probs(
        args.output_folder,
        out_prefix,
        probs,
        args.chrom_sizes,
        args.start_idx,
        windows
        )

if __name__ == "__main__":
    sys.exit(main())
