#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l h_vmem=16G
#$ -l mem_requested=16G
#$ -e classify-new-out.e
#$ -o classify-new-out.o

# verbose failures
set -o nounset -o pipefail -o errexit

MODEL=${1:-rf-model.pkl}
GENOME=${2:-../genome.100.sorted.bed}
IN_SCORE=${3:-binned-score/heart_fantom5.score.bed.gz}
IN_CHROM=${4:-binned-data/heart.bed}
IN_RNA=${5:-combined-rna/heart.bedGraph}
OUT_DIR=${6:-out/}
PROCESSING_DIR=${7:-processing/}

# We want to cover 2252374752, which means hitting task 2252

ID=$((SGE_TASK_ID - 1))

START_IDX=$((ID * 10000))
END_IDX=$(((ID+1) * 10000))
python -m cProfile -o prof_output_$ID classify_new_numpy.py --start-idx $START_IDX --end-idx $END_IDX $MODEL 20300 $GENOME $IN_SCORE  $IN_CHROM $IN_RNA $OUT_DIR $PROCESSING_DIR
