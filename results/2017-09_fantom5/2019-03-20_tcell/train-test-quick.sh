#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -e e.txt
#$ -o o.txt

# verbose failures
set -o nounset -o pipefail -o errexit

MAPPABLE=${1:-mappable_test.tsv}

ID=$(( SGE_TASK_ID - 1 ))

START_ID=$(( $ID*15 ))
END_ID=$(( $ID*15+15 ))
# 41-6536
python train.py binned-score/ varying-locations-merged.bed binned-data/ $MAPPABLE 20300 --start-idx $START_ID --end-idx $END_ID --rna-dir combined-rna/ --processing-dir validation-test/ --train-only
