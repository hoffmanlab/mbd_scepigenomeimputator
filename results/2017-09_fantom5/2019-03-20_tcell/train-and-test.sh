#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

KERNEL=${1:-2}
OUT_FN=${2:-2018-10-25-RF-final.png}

OUT_ROC=${3:-${OUT_FN%.*}_roc.txt}
OUT_PR=${4:-${OUT_FN%.*}_pr.txt}
MODEL_FN=${5:-${OUT_FN#*-}.pkl}

conda activate py27-2

python train.py binned-score/ varying-locations-merged.bed binned-data/ mappable_impute_training.tsv 20300 --start-idx 0 --end-idx 6546 --rna-dir combined-rna/ --validation mappable_test.tsv --validation-dir validation-test/ --kernel $KERNEL --processing-dir processed-impute/ --output-fn $OUT_FN --binary-case --save-roc $OUT_ROC --save-pr $OUT_PR --use-cached-model --output-model-fn $MODEL_FN
