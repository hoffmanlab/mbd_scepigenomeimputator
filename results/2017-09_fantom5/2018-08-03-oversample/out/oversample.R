description <- "Oversamples the input cut-score/ file, so that class balance is
achieved"
library(optparse)

if (!interactive()) {
    option.list <- list(
        make_option(
            "--input",
            type = "character",
            default = NA,
            help = "classifier guesses for each site (predict.py output)"
            ),
        make_option(
            "--output",
            type = "character",
            default = NA,
            help = "Output file name (will overwrite input if none given)"
            )
        )

    opt.parser <- OptionParser(
        option_list = option.list,
        desc = description
        )

    opt <- parse_args(opt.parser)
    truth.fn <<- opt$input
    if (is.na(opt$output)) {
        out.fn <<- truth.fn
    } else {
        out.fn <<- opt$output
    }
}

open.file.handle <- function(filename) {
    if (endsWith(filename, ".gz")) {
        return(gzfile(filename))
    } else {
        return(file(filename))
    }
}

# Read input
truth.fh <- open.file.handle(truth.fn)
truth.bed <- read.table(truth.fh, sep="\t")
truth.bed$class <- apply(truth.bed[, 4:ncol(truth.bed)], MARGIN=1, FUN=which.max)
class.balance <- table(truth.bed$class)
largest.class <- which.max(class.balance)
target.amount <- class.balance[largest.class]

# Enforce class balance per class
for (i in 1:length(class.balance)) {
    these.lines <- truth.bed[truth.bed$class == i, ]
    # Append random lines from these.lines to truth.bed
    # Should try expanding the table only once, but not currently 
    # worth the effort
    idx.to.add <- sample.int(
                             nrow(these.lines),
                             size = target.amount - class.balance[i],
                             replace = TRUE
                             )
    truth.bed <- rbind(truth.bed, truth.bed[idx.to.add, ])
}

# Sort truth.bed based on chr&start position
truth.bed <- truth.bed[order(truth.bed$V1, truth.bed$V2), ]

write.table(
    truth.bed[, -ncol(truth.bed)],  # Do not include the "class" column
    out.fn,
    sep="\t",
    quote=FALSE,
    row.names=FALSE,
    col.names=FALSE
    )
