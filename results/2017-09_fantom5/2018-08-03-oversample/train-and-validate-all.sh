#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

#qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N OverSampLinearSVC ./train-and-validate.sh 0 2018-08-03-oversample-SVC.png SVC.pkl
#qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N OverSampExtraTrees ./train-and-validate.sh 1 2018-08-03-oversample-ExtraTrees.png ExtraTrees.pkl
qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N OverSampRandomForest ./train-and-validate.sh 2 2018-08-03-oversample-RandomForest.png RF.pkl
#qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N OverSampKNN ./train-and-validate.sh 3 2018-08-03-oversample-KNN.png KNN.pkl
#qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N OverSampBernoulliNB ./train-and-validate.sh 4 2018-08-03-oversample-BernoulliNB.png BernoulliNB.pkl
#qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N OverSampGaussianNB ./train-and-validate.sh 5 2018-08-03-oversample-GaussianNB.png GaussianNB.pkl
#qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N OverSampLabelProp ./train-and-validate.sh 6 2018-08-03-oversample-LabelProp.png LabelProp.pkl
#qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N OverSampLabelSpread ./train-and-validate.sh 7 2018-08-03-oversample-LabelSpread.png LabelSpread.pkl
#qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N OverSampLinearDiscrim ./train-and-validate.sh 8 2018-08-03-oversample-LinearDiscriminant.png LinearDiscriminant.pkl
#qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N OverSampLogRegression ./train-and-validate.sh 9 2018-08-03-oversample-LogRegression.png LogRegression.pkl
#qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N OverSampAdaBoost ./train-and-validate.sh 11 2018-08-03-oversample-AdaBoost.png AdaBoost.pkl
#qsub -hold_jid 2321926 -pe smp 32 -N QuiesMultiLayerPerceptron ./train-and-validate.sh 10 2018-08-03-oversample-MultiLayerPerceptron.png
