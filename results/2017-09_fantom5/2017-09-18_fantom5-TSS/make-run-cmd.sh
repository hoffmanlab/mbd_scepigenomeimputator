#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

./make-run.sh ENCFF001SOV.bed 100 mappable_no_dups.txt
./make-run.sh ENCFF001SOV.bed 500 mappable_no_dups.txt
./make-run.sh ENCFF001SOV.bed 1000 mappable_no_dups.txt
./make-run.sh ENCFF001SOV.bed 10000 mappable_no_dups.txt
./make-run.sh ENCFF001SOV.bed 30000 mappable_no_dups.txt
./make-run.sh ENCFF001SOV.bed 60000 mappable_no_dups.txt
./make-run.sh ENCFF001SOV.bed 100000 mappable_no_dups.txt
./make-run.sh ENCFF001SOV.bed 200000 mappable_no_dups.txt
./make-run.sh ENCFF001SOV.bed 1000000 mappable_no_dups.txt
./make-run.sh UW.Penis_Foreskin_Melanocyte_Primary_Cells.ChromatinAccessibility.skin01.DS18590.narrowPeak.bed 101 mappable_no_dups.txt
./make-run.sh UW.Penis_Foreskin_Melanocyte_Primary_Cells.ChromatinAccessibility.skin01.DS18590.narrowPeak.bed 501 mappable_no_dups.txt
./make-run.sh UW.Penis_Foreskin_Melanocyte_Primary_Cells.ChromatinAccessibility.skin01.DS18590.narrowPeak.bed 1001 mappable_no_dups.txt
./make-run.sh UW.Penis_Foreskin_Melanocyte_Primary_Cells.ChromatinAccessibility.skin01.DS18590.narrowPeak.bed 10001 mappable_no_dups.txt
./make-run.sh UW.Penis_Foreskin_Melanocyte_Primary_Cells.ChromatinAccessibility.skin01.DS18590.narrowPeak.bed 30001 mappable_no_dups.txt
./make-run.sh UW.Penis_Foreskin_Melanocyte_Primary_Cells.ChromatinAccessibility.skin01.DS18590.narrowPeak.bed 60001 mappable_no_dups.txt
./make-run.sh UW.Penis_Foreskin_Melanocyte_Primary_Cells.ChromatinAccessibility.skin01.DS18590.narrowPeak.bed 100001 mappable_no_dups.txt
./make-run.sh UW.Penis_Foreskin_Melanocyte_Primary_Cells.ChromatinAccessibility.skin01.DS18590.narrowPeak.bed 200001 mappable_no_dups.txt
./make-run.sh UW.Penis_Foreskin_Melanocyte_Primary_Cells.ChromatinAccessibility.skin01.DS18590.narrowPeak.bed 1000001 mappable_no_dups.txt
