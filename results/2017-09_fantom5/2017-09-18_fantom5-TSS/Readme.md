# FANTOM5 TSS Prediction

## Rationale
Using the same cell-type encyclopedia tactic, I aim to predict the location of TSS using only open chromatin.

I'm using the fact that CAGE captures only the 5' end of RNA in order to label regions as TSS. 

In a way, this is sorta predicting gene transcription from chromatin accessibility, which might not be a thing. 

## Input Processing Steps

1) Map the files together using an input mappability file. 

2) Restrict ourselves to only looking at peaks from chr17. 

3) Restrict the samples to only those that are mappable. Other assumptions: it is not a control type. 

~~~
map.fn <- "fantom5-encyclopedia/mappable.txt"
counts.fn <- "CAGE-data/link/hg19.cage_peak_phase1and2combined_counts_ann.osc.txt.gz"
source("match.R")                                                                    
~~~ 

At this point, I have two analyses to follow:

## Global (ElasticNetCV) Analysis

Let's see how well the data can be transferred.

## Local (Consensus Voting) Analysis 

We need to make sure that my input file formats are the same, so first generate the analogues to the score bin file. 

(get_unique_accessibility.R, match.R, windower.sh) 

Leaving out:
~~~
Testing set:
CNhs11275	ENCODE	ENCSR000EIE.bed.gz	CNhs11275	A549	10499-107C4	lung adenocarcinoma cell line:A549	pneumocyte, type ii, great alveolar cell (septal cell)	counts.lung%20adenocarcinoma%20cell%20line%3aA549.CNhs11275.10499-107C4
CNhs11275	ENCODE	ENCSR000ELW.bed.gz	CNhs11275	A549	10499-107C4	lung adenocarcinoma cell line:A549	pneumocyte, type ii, great alveolar cell (septal cell)	counts.lung%20adenocarcinoma%20cell%20line%3aA549.CNhs11275.10499-107C4
CNhs11275	ENCODE	ENCSR000ELW.bed.gz	CNhs11275	A549	10499-107C4	lung adenocarcinoma cell line:A549	pneumocyte, type ii, great alveolar cell (septal cell)	counts.lung%20adenocarcinoma%20cell%20line%3aA549.CNhs11275.10499-107C4
CNhs11275	ENCODE	ENCSR000ELW.bed.gz	CNhs11275	A549	10499-107C4	lung adenocarcinoma cell line:A549	pneumocyte, type ii, great alveolar cell (septal cell)	counts.lung%20adenocarcinoma%20cell%20line%3aA549.CNhs11275.10499-107C4

Training set:
CNhs11385	ENCODE	ENCSR000ELJ.bed.gz	CNhs11385	OSTEOBLAST	11354-117H4	Osteoblast, donor2	osteoblast	counts.Osteoblast%2c%20donor2.CNhs11385.11354-117H4
CNhs11385	ENCODE	ENCSR000ELJ.bed.gz	CNhs11385	OSTEOBLAST	11354-117H4	Osteoblast, donor2	osteoblast	counts.Osteoblast%2c%20donor2.CNhs11385.11354-117H4
CNhs12036	ENCODE	ENCSR000ELJ.bed.gz	CNhs12036	OSTEOBLAST	11426-118G4	Osteoblast, donor3	osteoblast	counts.Osteoblast%2c%20donor3.CNhs12036.11426-118G4
CNhs12036	ENCODE	ENCSR000ELJ.bed.gz	CNhs12036	OSTEOBLAST	11426-118G4	Osteoblast, donor3	osteoblast	counts.Osteoblast%2c%20donor3.CNhs12036.11426-118G4
~~~

Second attempt at single analysis leaving out
~~~
Validation Set:
CNhs11275	ENCODE	ENCFF001UUD.bed.gz	CNhs11275	A549	10499-107C4	lung adenocarcinoma cell line:A549	pneumocyte, type ii, great alveolar cell (septal cell)	counts.lung%20adenocarcinoma%20cell%20line%3aA549.CNhs11275.10499-107C4
CNhs10650	ROADMAP	UW.Fetal_Thymus.ChromatinAccessibility.H-23964.DS17876.narrowPeak.bed.gz	CNhs10650	FETAL_THYMUS	10043-101F7	thymus, fetal, pool1	thymus	counts.thymus%2c%20fetal%2c%20pool1.CNhs10650.10043-101F7 
Test Set:
CNhs12626	ENCODE	ENCFF001SOV.bed.gz	CNhs12626	HEPATOCYTE	11684-122I1	Hepatocyte, donor3	hepatocyte	counts.Hepatocyte%2c%20donor3.CNhs12626.11684-122I1
CNhs13406	ROADMAP	UW.Penis_Foreskin_Melanocyte_Primary_Cells.ChromatinAccessibility.skin01.DS18590.narrowPeak.bed.gz	CNhs13406	MELANOCYTE	12837-137B2	Melanocyte, donor3 (MC+3)	melanocyte	counts.Melanocyte%2c%20donor3%20%28MC%2b3%29.CNhs13406.12837-137B2
~~~

Third attempt at single analysis leaving out 

~~~
Validation Set:
CNhs12340	ENCODE	ENCFF001SOV.bed.gz	CNhs12340	HEPATOCYTE	11523-119I2	Hepatocyte, donor1	hepatocyte	counts.Hepatocyte%2c%20donor1.CNhs12340.11523-119I2
CNhs12816	ROADMAP	UW.Penis_Foreskin_Melanocyte_Primary_Cells.ChromatinAccessibility.skin01.DS18590.narrowPeak.bed.gz	CNhs12816	MELANOCYTE	12641-134G4	Melanocyte, donor1 (MC+1)	melanocyte	counts.Melanocyte%2c%20donor1%20%28MC%2b1%29.CNhs12816.12641-134G4

Test Set:
CNhs11275	ENCODE	ENCFF001UUD.bed.gz	CNhs11275	A549	10499-107C4	lung adenocarcinoma cell line:A549	pneumocyte, type ii, great alveolar cell (septal cell)	counts.lung%20adenocarcinoma%20cell%20line%3aA549.CNhs11275.10499-107C4
CNhs10650	ROADMAP	UW.Fetal_Thymus.ChromatinAccessibility.H-23964.DS17876.narrowPeak.bed.gz	CNhs10650	FETAL_THYMUS	10043-101F7	thymus, fetal, pool1	thymus	counts.thymus%2c%20fetal%2c%20pool1.CNhs10650.10043-101F7
~~~

## Results 

Initial validation runs had a tend to increase in Cohen's Kappa -- turns out to have been due to the large number of NonVoter regions called (Note that the original dataset has no NonVoter regions). 

I am currently testing the effects of reducing the probability-to-accept-a-call to 0.01. 

### Oct 3rd

Segmentation faults appear to occur when the DNase files are the exact same, which needs to be fixed. However, the fact that I am using biological replicates of left-out samples is a problem of its own, so I've removed them. 

### Oct 6th

Calculated Cohen's Kappa + Generated Pr curves... see below:

| Tissue       | Cohen's Kappa |
| ------------ | -------------:|
| A549         | 0.32          |
| Fetal Thymus | 0.62          |
| Total        | 0.44          |

![pr1](2017-10-05_CAGESignal_ROC-PRC.png)
![pr2](2017-10-05_NoCAGESignal_ROC-PRC.png)
