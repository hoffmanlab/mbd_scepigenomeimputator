#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

./make-run.sh ENCFF001UUD.bed 102 mappable_no_dups.txt
./make-run.sh UW.Fetal_Thymus.ChromatinAccessibility.H-23964.DS17876.narrowPeak.bed 103 mappable_no_dups.txt
