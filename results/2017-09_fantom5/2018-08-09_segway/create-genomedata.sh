#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -e e.txt
#$ -o o.txt

# verbose failures
set -o nounset -o pipefail -o errexit

IDX=$((SGE_TASK_ID - 1))

IN_DIR=($1/*.gz)
TRACK_PREFIX=${2:-DNase}
OUT_DIR=${3:-genomedata/}
AGP=${4:-"AGP/*.agp"}
TMP_DIR=${5:-unzipped-data/}

IN_FILE=${IN_DIR[$IDX]}
BASENAME=`basename $IN_FILE`
TRACK_NAME=$TRACK_PREFIX-${BASENAME%%.bedGraph.gz}

## Move to temp directory, unzip
#cp $IN_DIR $TMP_DIR/$BASENAME
#gunzip $TMP_DIR/$BASENAME
#
#TMP_FILE=${BASENAME%%.gz}

# Given an input bedgraph, generate the genomedata file
echo genomedata-load --assembly -s "$AGP" -t ${TRACK_NAME}=$IN_FILE $OUT_DIR/${TRACK_NAME}.genomedata
genomedata-load --assembly -s "$AGP" -t ${TRACK_NAME}=$IN_FILE $OUT_DIR/${TRACK_NAME}.genomedata

#rm $TMP_DIR/$TMP_FILE
