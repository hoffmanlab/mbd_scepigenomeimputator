#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

# Train segway on a given cell type
# (This is for training one cell type at a time)

# Input
#SCORE_GENOMEDATA_DIR=${1:-genomedata-score}
#CHROM_GENOMEDATA_DIR=${2:-genomedata-dnase}
#SCORE_BED_DIR=${3:-binned-score}
#TRAIN_LINE=${4:-stomach}
EXCLUDE_FILE=${1:-hg19-blacklist.bed} #-failures.bed}
IN_DIR=${2:-genomedata/}
VALIDATE_DIR=${2:-genomedata-validate/}
TRACKS=($IN_DIR/*.genomedata)
VALIDATE_TRACKS=($VALIDATE_DIR/*.genomedata)
#VALIDATE_FILE=${6:-validation-set.bed}
#TRUTH=${7:-stomach_fantom5-nonquies.bed}
#SEG_TABLE=${8:-seg_table.tsv}

#SCORE_TRACKS=($SCORE_GENOMEDATA_DIR/*)
#TRAINING_TRACKS=(${SCORE_TRACKS[@]/$SCORE_GENOMEDATA_DIR\/${TRAIN_LINE}.genomedata/})
#CHROM_ACCESS_TRACK=$CHROM_GENOMEDATA_DIR/${TRAIN_LINE}.genomedata
#TRUTH=$SCORE_BED_DIR/${TRAIN_LINE}_fantom5.score.bed

# Workaround for Segway not submitting jobs from non-head-nodes cluster
export DRMAA_LIBRARY_PATH=/sge/lib/linux-x64/libdrmaa.so
export SEGWAY_RAND_SEED=22426492
export SEGWAY_NUM_LOCAL_JOBS=4
export NUM_THREADS=4

# Train the model into the directory train_results/
segway --resolution=100 \
    --exclude-coords=$EXCLUDE_FILE \
    --split-sequences=500000 \
    --cluster-opt="-q hoffmangroup" \
    --tracks-from="tracks.txt" \
    --num-instances=10 \
    --num-labels=10 \
    --minibatch-fraction=0.001 \
    --max-train-rounds=10 \
    --mem-usage=2,3,4,6,8,10,12,16,32 \
    train \
    ${TRACKS[@]} \
    train_results_chrom_only
    #--validation-coords=${VALIDATE_FILE} \

# Identify the entirety of the genome
segway \
    --exclude-coords=hg19-blacklist.bed \
    --cluster-opt="-q hoffmangroup" \
    --recover annotate_results \
    identify \
    ${TRACKS[@]} ${VALIDATE_TRACKS[@]} \
    train_results_chrom_only \
    annotate_results_chrom_only
#    --exclude-coords=$EXCLUDE_FILE \
#    --bigBed=annotate_results/segway.layered.bigBed \

# Visualize various model metrics
segtools-gmtk-parameters train_results/params/params.params
segtools-length-distribution annotate_results/segway.bed.gz
