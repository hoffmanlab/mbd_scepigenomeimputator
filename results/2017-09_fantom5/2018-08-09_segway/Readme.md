
# Ablation analysis 

This folder contains the Segway ablation analysis. 

In order to map Segway annotations to real locations, we maximize the 3-class MCC when deciding which labels to assign which function

## Notes 

train_results_on_everything and annotate_results_on_everything are runs mistakenly performed on training+validation sets, instead of just training 
