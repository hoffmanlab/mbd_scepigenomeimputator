# Enhancer vs TSS Prediction from Chromatin Accessibility
I'm trying to take a look and see whether or not chromatin accessibility coresponds to a TSS, an Enhancer, or no signal 

Or maybe I should take a look at whether or not it can just take signal and differentiate b/w TSSes and Enhancers?

Note that I've fixed the quick reduce hack

## Methods
I have a CAGE signal predictor from last analysis. Now I need to further classify each peak by whether or not it shows up in Slidebase's enhancer dataset (See the processed data folder for more info) 

However, Slidebase is using something called "facets", which means i need to match each sample from my training set w/ the facet is belongs to. This is from tables S11 and S10.csv

CNhs10650   ROADMAP UW.Fetal_Thymus.ChromatinAccessibility.H-23964.DS17876.narrowPeak.bed.gz   CNhs10650   FETAL_THYMUS    10043-101F7 thymus, fetal, pool1   thymus  counts.thymus%2c%20fetal%2c%20pool1.CNhs10650.10043-101F7  

