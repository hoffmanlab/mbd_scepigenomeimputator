#!/usr/bin/env python
import argparse
import logging
import os
import sys

import numpy as np
from pybedtools import BedTool

NUM_CLASSES = 3

# PARSE ARGS ###################################################################
def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Finds the nearest neighbour for each of the training sets and the
    output set for things within the given window
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "processed_dir",
        help = """Processed directory"""
        )
    parser.add_argument(
        "validation_dir",
        help = """Validation directory"""
        )
    parser.add_argument(
        "validation_mapping",
        help = """Validation mapping file"""
        )
    parser.add_argument(
        "output_file",
        help = "output jaccard matrix filename"
        )
    parser.add_argument(
        "--start-idx",
        type=int,
        default=0,
        help="Which line of the mapping file to start operating on"
        )
    parser.add_argument(
        "--end-idx",
        type=int,
        default=-1,
        help="Which line of the mapping file to stop operating on (-1 for all)"
        )
    parser.add_argument(
        "--by",
        type=int,
        default=1,
        help="How many lines of the mapping file to do at once"
        )
    return parser.parse_args()

def load_processed(cache_directory, start_idx=0, end_idx=-1, by=1):
    """
    Load all of the training sets (Assuming preprocessed files)

    Returns (features, annotations, num_cell_types)
    """
    # Run on all rows if end_idx negative
    if end_idx < 0:
        end_idx = len([fn for fn in os.listdir(cache_directory) if fn.endswith("_X_formatted.npy")])

    # Load up the first file to determine how large our output matrix will be
    X_filename = "{}-{}_X_formatted.npy".format(start_idx, start_idx+by)
    X_filename = os.path.join(cache_directory, X_filename)
    try:
        with open(X_filename) as X_file:
            first_line = np.load(X_file)
    except:
        logging.warn("Missing formatted npy files: please rerun train")
        raise

    (num_cell_types, num_features) = first_line.shape
    num_cell_types = num_cell_types // by
    num_entries = (end_idx-start_idx)*num_cell_types

    X = np.empty(shape=(num_entries, num_features))
    y = np.empty(shape=(num_entries, 1))

    for i in range(start_idx, end_idx, by):
        next_idx = i+by
        thispos = (i - start_idx)*num_cell_types
        nextpos = (next_idx - start_idx)*num_cell_types
        X_filename = "{}-{}_X_formatted.npy".format(i, next_idx)
        X_filename = os.path.join(cache_directory, X_filename)
        y_filename = "{}-{}_y_formatted.npy".format(i, next_idx)
        y_filename = os.path.join(cache_directory, y_filename)

        try:
            with open(X_filename) as X_file:
                X[thispos:nextpos, :] = np.load(X_file)
            with open(y_filename) as y_file:
                y[thispos:nextpos, :] = np.load(y_file)
        except:
            logging.warn("Missing formatted npy files: please rerun train")
            raise

    return(X, y, num_cell_types)

def distance(X1, X2):
    return np.linalg.norm(X1-X2)

def find_nearest(X, y, otherX, num_cell_types):
    num_obs = X.shape[0]//num_cell_types
    other_y = np.empty(shape=(1, num_obs))
    for i in range(num_obs):
        # Find the nearest neighbour according to the euclidean distance along
        # ATAC-seq data near this observation
        best_cell_type = 0
        best_cell_dist = -1
        for j in range(num_cell_types):
            # Find the distance between these two features
            this_dist = distance(X[i*num_cell_types+j, :], otherX)
            if this_dist < best_cell_dist or best_cell_dist < 0:
                best_cell_dist = this_dist
                best_cell_type = j

        # Fill the best observation
        other_y[0, i] = y[i*num_cell_types+best_cell_type, :]
    return(other_y)

def output_predicted_observations(mappable_file, y):
    # Determine the names of the files we're outputting
    cell_lines = []
    with open(mappable_file) as in_file:
        for line in in_file:
            info = line.split("\t")
            cell_lines.append(info[0])

    # Write out each prediction
    for (i, filename) in enumerate(cell_lines):
        np.savetxt(
                "{}.bed.gz".format(filename),
                y[:, i],
                delimiter="\t",
                fmt="%u"
                )

def output_jaccard(y, real_y, output_file):
    """
    How well did we actually do at predicting it?
    Note: Assumes there are NUM_CLASSES classes, numbered 1 to NUM_CLASSES
    """
    # First, make both ys the same format
    pred_y = y.ravel()
    real_y = real_y.ravel()

    # Now we can test our solution
    jaccard_matrix = np.zeros((NUM_CLASSES, NUM_CLASSES))
    for i in range(NUM_CLASSES):
        for j in range(NUM_CLASSES):
            jaccard_matrix[i][j] = sum((pred_y == i+1) & (real_y == j+1))

    np.savetxt(output_file,
            jaccard_matrix,
            delimiter="\t",
            fmt="%u"
            )

# MAIN #########################################################################
def main():
    args = parse_args()
    (X, y, num_cell_types) = load_processed(
        args.processed_dir,
        start_idx=args.start_idx,
        end_idx=args.end_idx,
        by=args.by
        )
    (v_X, v_y, _) = load_processed(
        args.validation_dir,
        start_idx=args.start_idx,
        end_idx=args.end_idx,
        by=args.by
        )

    num_obs = X.shape[0]//num_cell_types
    num_validation_types = v_y.shape[0] // num_obs
    v_y_pred = np.empty(shape=(num_obs, num_validation_types))
    for i in range(num_validation_types):
        v_y_pred[:, i] = find_nearest(X, y, v_X[i, :], num_cell_types)

    output_predicted_observations(
        args.validation_mapping,
        v_y_pred
        )

    output_jaccard(v_y_pred, v_y, args.output_file)

if __name__ == "__main__":
    sys.exit(main())

