#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -e e.txt
#$ -o o.txt

# verbose failures
set -o nounset -o pipefail -o errexit

MAPPABLE=${1:-mappable_impute_training.tsv}

ID=$(( SGE_TASK_ID - 1 ))

python train.py binned-score/ varying-locations-merged.bed binned-data/ $MAPPABLE 20300 --start-idx $ID --end-idx $SGE_TASK_ID --rna-dir combined-rna/ --processing-dir processed-impute/ --train-only
