#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

KERNEL=${1:-0}
OUT_FN=${2:-2018-06-05-PRC-SVC.png}

conda activate py27-2
python train.py binned-score/ varying-locations-merged.bed binned-data/ mappable_impute_training.tsv 20300 --start-idx 0 --end-idx 6500 --rna-dir combined-rna/ --validation mappable_validation.tsv --kernel $KERNEL --processing-dir processed-impute/ --output-fn $OUT_FN
