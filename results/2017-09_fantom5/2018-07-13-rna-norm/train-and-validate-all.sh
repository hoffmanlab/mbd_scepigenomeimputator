#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

#qsub -hold_jid 2321926 -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N QuiesLinearSVC ./train-and-validate.sh 0 2018-07-20-imputedrna-keepquies-SVC.png
#qsub -hold_jid 2321926 -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N QuiesExtraTrees ./train-and-validate.sh 1 2018-07-20-imputedrna-keepquies-ExtraTrees.png
#qsub -hold_jid 2321926 -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N QuiesRandomForest ./train-and-validate.sh 2 2018-07-20-imputedrna-keepquies-RandomForest.png
qsub -hold_jid 2321926 -pe smp 31 -N QuiesKNN ./train-and-validate.sh 3 2018-07-20-imputedrna-keepquies-KNN.png
#qsub -hold_jid 2321926 -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N QuiesBernoulliNB ./train-and-validate.sh 4 2018-07-20-imputedrna-keepquies-BernoulliNB.png
#qsub -hold_jid 2321926 -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N QuiesGaussianNB ./train-and-validate.sh 5 2018-07-20-imputedrna-keepquies-GaussianNB.png
#qsub -hold_jid 2321926 -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N QuiesLabelProp ./train-and-validate.sh 6 2018-07-20-imputedrna-keepquies-LabelProp.png
#qsub -hold_jid 2321926 -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N QuiesLabelSpread ./train-and-validate.sh 7 2018-07-20-imputedrna-keepquies-LabelSpread.png
#qsub -hold_jid 2321926 -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N QuiesLinearDiscrim ./train-and-validate.sh 8 2018-07-20-imputedrna-keepquies-LinearDiscriminant.png
#qsub -hold_jid 2321926 -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N QuiesLogRegression ./train-and-validate.sh 9 2018-07-20-imputedrna-keepquies-LogRegression.png
#qsub -hold_jid 2321926 -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N QuiesAdaBoost ./train-and-validate.sh 11 2018-07-20-imputedrna-keepquies-AdaBoost.png
#qsub -hold_jid 2321926 -pe smp 32 -N QuiesMultiLayerPerceptron ./train-and-validate.sh 10 2018-07-20-imputedrna-keepquies-MultiLayerPerceptron.png
