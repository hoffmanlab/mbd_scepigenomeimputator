# All Chromosome Enh/Prom/TSS Analysis
This analysis is essentially the same as the 2017-11-02_enhancer-promoter-tss analysis, but with all chromosomes rather than just chr17 

As before, the validation sets are skin_fibroblast (ENCFF001WFF.bed.gz/CL:0002620) and thymus (ENCFF278XGA/UBERON:0002370) testing sets are t-cell (ENCFF345YDG/CL:0000084) and pancreas (ENCFF231QBI/UBERPON:0001264) 


