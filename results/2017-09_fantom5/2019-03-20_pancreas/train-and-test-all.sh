#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

PREFIX=2019-04-18-

qsub -hold_jid 5061074 -l mem_requested=10G -pe smp 8 -N PancLinearSVC ./train-and-test.sh 0 ${PREFIX}SVC.png ${PREFIX}SVC_roc.txt ${PREFIX}SVC_pr.txt ModelSVC.pkl
qsub -hold_jid 5061074 -l mem_requested=10G -pe smp 8 -N PancExtraTrees ./train-and-test.sh 1 ${PREFIX}ExtraTrees.png ${PREFIX}ExtraTrees_roc.txt ${PREFIX}ExtraTrees_pr.txt ModelExtraTrees.pkl
qsub -hold_jid 5061074 -l mem_requested=10G -pe smp 16 -N PancRandomForest ./train-and-test.sh 2 ${PREFIX}RandomForest.png ${PREFIX}RandomForest_roc.txt ${PREFIX}RandomForest_pr.txt ModelRandomForest.pkl
qsub -hold_jid 5061074 -pe smp 16 -N PancKNN ./train-and-test.sh 3 ${PREFIX}KNN.png ${PREFIX}KNN_roc.txt ${PREFIX}KNN_pr.txt ModelKNN.pkl
qsub -hold_jid 5061074 -l mem_requested=10G -pe smp 8 -N PancBernoulliNB ./train-and-test.sh 4 ${PREFIX}BernoulliNB.png ${PREFIX}BernoulliNB_roc.txt ${PREFIX}BernoulliNB_pr.txt ModelBernoulliNB.pkl
qsub -hold_jid 5061074 -l mem_requested=10G -pe smp 8 -N PancGaussianNB ./train-and-test.sh 5 ${PREFIX}GaussianNB.png ${PREFIX}GaussianNB_roc.txt ${PREFIX}GaussianNB_pr.txt ModelGaussianNB.pkl
qsub -hold_jid 5061074 -l mem_requested=10G -pe smp 8 -N PancLinearDiscrim ./train-and-test.sh 8 ${PREFIX}LinearDiscriminant.png ${PREFIX}LinearDiscriminant_roc.txt ${PREFIX}LinearDiscriminant_pr.txt ModelLinearDiscriminant.pkl
qsub -hold_jid 5061074 -l mem_requested=10G -pe smp 8 -N PancLogRegression ./train-and-test.sh 9 ${PREFIX}LogRegression.png ${PREFIX}LogRegression_roc.txt ${PREFIX}LogRegression_pr.txt ModelLogRegression.pkl
qsub -hold_jid 5061074 -l mem_requested=10G -pe smp 8 -N PancAdaBoost ./train-and-test.sh 11 ${PREFIX}AdaBoost.png ${PREFIX}AdaBoost_roc.txt ${PREFIX}AdaBoost_pr.txt ModelAdaBoost.pkl
#qsub -l mem_requested=10G -pe smp 8 -N PancLabelProp ./train-and-test.sh 6 ${PREFIX}LabelProp.png ${PREFIX}LabelProp_roc.txt ${PREFIX}LabelProp_pr.txt ModelLabelProp.pkl
#qsub -l mem_requested=10G -pe smp 8 -N PancLabelSpread ./train-and-test.sh 7 ${PREFIX}LabelSpread.png ${PREFIX}LabelSpread_roc.txt ${PREFIX}LabelSpread_pr.txt ModelLabelSpread.pkl
#qsub -pe smp 32 -N QuiesMultiLayerPerceptron ./train-and-test.sh 10 2018-12-18-imputedrna-keepquies-MultiLayerPerceptron.png
