# Plotting commands
`python plot_merged_et.py --roc 2019-01-02-ET-*_roc.txt --pr 2019-01-02-ET-*_pr.txt --top-n 5 2019-03-07-merged-ET.svg 0.1911`
`python plot_merged_roc_pr.py 2019-04-18-panc.svg --roc 2019-04-18-*_roc.txt --pr 2019-04-18-*_pr.txt --alg-names "Adaptive boosting" "Bernoulli naive Bayes" "Extremely randomized trees" "Gaussian naive Bayes" "K-nearest neighbours" "Linear discriminant" "Logistic regression" "Random forest" --bold "Extremely randomized trees"`
