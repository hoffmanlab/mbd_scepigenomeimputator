#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

MAPPABLE=${1:-mappable_validation.tsv}

ID=$(( SGE_TASK_ID - 1 ))

START_ID=$(( $ID*15+41 ))
END_ID=$(( $ID*15+56 ))
# 41-6536
python train.py binned-score/ varying-locations-merged.bed binned-data/ $MAPPABLE 20300 --start-idx $START_ID --end-idx $END_ID --processing-dir validation/ --train-only
