#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

python test.py binned-score/ varying-locations.bed binned-data/ mappable_test.tsv 20300
