#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N LinearSVC ./train-and-validate.sh 0 2018-07-03-exclude-SVC.png
qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N ExtraTrees ./train-and-validate.sh 1 2018-07-03-exclude-ExtraTrees.png
qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N RandomForest ./train-and-validate.sh 2 2018-07-03-exclude-RandomForest.png
qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N KNN ./train-and-validate.sh 3 2018-07-03-exclude-KNN.png
qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N BernoulliNB ./train-and-validate.sh 4 2018-07-03-exclude-BernoulliNB.png
qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N GaussianNB ./train-and-validate.sh 5 2018-07-03-exclude-GaussianNB.png
qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N LabelProp ./train-and-validate.sh 6 2018-07-03-exclude-LabelProp.png
qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N LabelSpread ./train-and-validate.sh 7 2018-07-03-exclude-LabelSpread.png
qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N LinearDiscrim ./train-and-validate.sh 8 2018-07-03-exclude-LinearDiscriminant.png
qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N LogRegression ./train-and-validate.sh 9 2018-07-03-exclude-LogRegression.png
qsub -pe smp 32 -N MultiLayerPerceptron ./train-and-validate.sh 10 2018-07-03-exclude-MultiLayerPerceptron.png
qsub -l h_vmem=32G -l mem_requested=32G -pe smp 4 -N AdaBoost ./train-and-validate.sh 11 2018-07-03-exclude-AdaBoost.png
