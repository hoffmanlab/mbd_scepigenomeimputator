#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

# Find any gaps in dir/#-#_X_formatted.npy
OUT_DIR=${1:-processed/}
NUM_LINES=`ls -al $OUT_DIR/*_X_formatted.npy | wc -l`

for ((i=0; i < NUM_LINES-1; i++)); do
    if [ ! -e "$OUT_DIR/$i-$(($i+1))_X_formatted.npy" ]; then
        echo $i
        NUM_LINES=$NUM_LINES+1
    fi
done
