#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

MAPPABLE=${1:-mappable_training.tsv}

ID=$(( SGE_TASK_ID - 1 ))

# Need to rerun 1501-6536
python train.py binned-score/ varying-locations-merged.bed binned-data/ $MAPPABLE 20300 --start-idx $ID --end-idx $SGE_TASK_ID --train-only
