#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -e e.txt
#$ -o o.txt
#$ -l hostname=!n22vm1
#$ -t 2-4812
#$ -tc 100

# verbose failures
set -o nounset -o pipefail -o errexit

MAPPABLE=${1:-mappable_tcell.tsv}

ID=$(( SGE_TASK_ID - 1 ))

# For our 481,299-line file, split into jobs of 100 lines apiece, and then do 4812 jobs
START_ID=$(( $ID*100 ))
END_ID=$(( $ID*100+100 ))

python train.py binned-score/ chr19-locations.bed binned-data/ $MAPPABLE 20300 --start-idx $START_ID --end-idx $END_ID --rna-dir combined-rna/ --processing-dir processed-impute/ --train-only --binary-case
