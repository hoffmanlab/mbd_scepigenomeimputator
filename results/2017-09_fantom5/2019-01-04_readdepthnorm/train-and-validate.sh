#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l hostname=!n22vm1

# verbose failures
set -o nounset -o pipefail -o errexit

KERNEL=${1:-0}
OUT_FN=${2:-2018-06-05-PRC-SVC.png}
OUT_PKL=${6:-${OUT_FN%.*}.pkl}

OUT_ROC=${4:-${OUT_FN%.*}_valid_roc.txt}
OUT_PR=${5:-${OUT_FN%.*}_valid_pr.txt}

if [ -z "$3" ]; then
    SCORING_OPT=""
else
    SCORING_OPT="--scoring ${3:-accuracy}"
fi

conda activate py27-2

python train.py binned-score/ varying-locations-merged.bed binned-data/ mappable_impute_training.tsv 20300 --start-idx 0 --end-idx 6546 --rna-dir combined-rna/ --validation mappable_validation.tsv --kernel $KERNEL --processing-dir processed-impute/ --output-fn $OUT_FN --binary-case --output-model-fn ${OUT_PKL} --save-roc $OUT_ROC --save-pr $OUT_PR $SCORING_OPT
