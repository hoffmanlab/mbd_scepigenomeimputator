#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

PREFIX=${1:-2019-04-10-}

qsub -l mem_requested=5G -pe smp 8 -N TestLinearSVC ./train-and-test.sh 0 ${PREFIX}SVC.png
qsub -l mem_requested=5G -pe smp 8 -N TestExtraTrees ./train-and-test.sh 1 ${PREFIX}ExtraTrees.png
qsub -l mem_requested=5G -pe smp 16 -N TestRandomForest ./train-and-test.sh 2 ${PREFIX}RandomForest.png
qsub -pe smp 16 -N TestKNN ./train-and-test.sh 3 ${PREFIX}KNN.png
qsub -l mem_requested=5G -pe smp 8 -N TestBernoulliNB ./train-and-test.sh 4 ${PREFIX}BernoulliNB.png
qsub -l mem_requested=5G -pe smp 8 -N TestGaussianNB ./train-and-test.sh 5 ${PREFIX}GaussianNB.png
qsub -l mem_requested=5G -pe smp 8 -N TestLabelProp ./train-and-test.sh 6 ${PREFIX}LabelProp.png
qsub -l mem_requested=5G -pe smp 8 -N TestLabelSpread ./train-and-test.sh 7 ${PREFIX}LabelSpread.png
qsub -l mem_requested=5G -pe smp 8 -N TestLinearDiscrim ./train-and-test.sh 8 ${PREFIX}LinearDiscriminant.png
qsub -l mem_requested=5G -pe smp 8 -N TestLogRegression ./train-and-test.sh 9 ${PREFIX}LogRegression.png
qsub -l mem_requested=5G -pe smp 8 -N TestAdaBoost ./train-and-test.sh 11 ${PREFIX}AdaBoost.png
#qsub -pe smp 32 -N QuiesMultiLayerPerceptron ./train-and-test.sh 10 ${PREFIX}MultiLayerPerceptron.png
