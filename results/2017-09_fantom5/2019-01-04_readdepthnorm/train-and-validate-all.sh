#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

SCORING="balanced_accuracy"
PREFIX=2019-04-17_balanced-acc_
qsub -l mem_requested=5G -pe smp 8 -N Train${SCORING}LinearSVC ./train-and-validate.sh 0 ${PREFIX}SVC.png $SCORING
qsub -l mem_requested=5G -pe smp 8 -N Train${SCORING}ExtraTrees ./train-and-validate.sh 1 ${PREFIX}ExtraTrees.png $SCORING
qsub -l mem_requested=5G -pe smp 16 -N Train${SCORING}RandomForest ./train-and-validate.sh 2 ${PREFIX}RandomForest.png $SCORING
qsub -l mem_requested=5G -pe smp 8 -N Train${SCORING}KNN ./train-and-validate.sh 3 ${PREFIX}KNN.png $SCORING
qsub -l mem_requested=5G -pe smp 8 -N Train${SCORING}BernoulliNB ./train-and-validate.sh 4 ${PREFIX}BernoulliNB.png $SCORING
qsub -l mem_requested=5G -pe smp 8 -N Train${SCORING}GaussianNB ./train-and-validate.sh 5 ${PREFIX}GaussianNB.png $SCORING
#qsub -l mem_requested=5G -pe smp 8 -N Train${SCORING}LabelProp ./train-and-validate.sh 6 ${PREFIX}LabelProp.png $SCORING
#qsub -l mem_requested=5G -pe smp 8 -N Train${SCORING}LabelSpread ./train-and-validate.sh 7 ${PREFIX}LabelSpread.png $SCORING
qsub -l mem_requested=5G -pe smp 8 -N Train${SCORING}ExtraTrees ./train-and-validate.sh 8 ${PREFIX}LinearDiscriminant.png $SCORING
qsub -l mem_requested=5G -pe smp 8 -N Train${SCORING}LogRegression ./train-and-validate.sh 9 ${PREFIX}LogRegression.png $SCORING
qsub -l mem_requested=5G -pe smp 8 -N Train${SCORING}AdaBoost ./train-and-validate.sh 11 ${PREFIX}AdaBoost.png $SCORING
#qsub -pe smp 32 -N QuiesMultiLayerPerceptron ./train-and-validate.sh 10 ${PREFIX}MultiLayerPerceptron.png $SCORING
