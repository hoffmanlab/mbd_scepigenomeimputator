#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
##$ -t 1-36

# verbose failures
set -o nounset -o pipefail -o errexit

# Determine the best performing parameters on ExtraTrees

ID=$((SGE_TASK_ID-1))

KERNEL=1
OUT_FN=2019-01-23-RF-$ID.png

OUT_ROC=${3:-${OUT_FN%.*}_roc.txt}
OUT_PR=${4:-${OUT_FN%.*}_pr.txt}

python train-and-plot-RF.py binned-score/ varying-locations-merged.bed binned-data/ mappable_impute_training.tsv 20300 $ID --start-idx 0 --end-idx 6546 --rna-dir combined-rna/ --validation mappable_test.tsv --validation-dir validation-test/ --kernel $KERNEL --processing-dir processed-impute/ --output-fn $OUT_FN --binary-case --save-roc $OUT_ROC --save-pr $OUT_PR
