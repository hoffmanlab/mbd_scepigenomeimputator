## Creating the merged enhancers dataset
1) ```qsub -t 1-44 get_global_matches.sh reparsed-scores "RegPermissive" enhancer-score```

2) ```./merge_matches.sh enhancer-score/ enhancers_merged.bed combinations_to_find.txt 'skin_fibroblast|thymus|t-cell|pancreas'``` 

3) ```qsub -t 1-25 ./get_unique_accessibility.sh combinations_to_find.txt mappable_wide_leave4out_noheader.tsv binned-data/```

## Creating the merged dnase dataset
1) ```qsub -t 1-44 ./get_global_matches_insert_dummy.sh binned-data/ "chr17" dnase-score```

2) ```./merge_matches.sh dnase-score/ dnase_merged.bed combinations_to_find_dnase.txt 'ENCFF001WFF|ENCFF278XGA|ENCFF345YDG|ENCFF231QBI'```

3) ```qsub -t 1-25 ./get_unique_accessibility.sh combinations_to_find_dnase.txt mappable_wide_leave4out_noheader.tsv binned-data/```
