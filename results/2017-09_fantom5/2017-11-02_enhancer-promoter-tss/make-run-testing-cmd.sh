#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

./make-run.sh ENCFF345YDG.bed 102 mappable_wide_leave4out_noheader.tsv
./make-run.sh ENCFF231QBI.bed 103 mappable_wide_leave4out_noheader.tsv
