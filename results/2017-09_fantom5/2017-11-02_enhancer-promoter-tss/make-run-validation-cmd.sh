#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

./make-run.sh ENCFF001WFF.bed 100 mappable_wide_leave4out_noheader.tsv
./make-run.sh ENCFF001WFF.bed 1000 mappable_wide_leave4out_noheader.tsv
./make-run.sh ENCFF001WFF.bed 10000 mappable_wide_leave4out_noheader.tsv
./make-run.sh ENCFF001WFF.bed 50000 mappable_wide_leave4out_noheader.tsv
./make-run.sh ENCFF001WFF.bed 100000 mappable_wide_leave4out_noheader.tsv
./make-run.sh ENCFF278XGA.bed 101 mappable_wide_leave4out_noheader.tsv
./make-run.sh ENCFF278XGA.bed 1001 mappable_wide_leave4out_noheader.tsv
./make-run.sh ENCFF278XGA.bed 10001 mappable_wide_leave4out_noheader.tsv
./make-run.sh ENCFF278XGA.bed 50001 mappable_wide_leave4out_noheader.tsv
./make-run.sh ENCFF278XGA.bed 100001 mappable_wide_leave4out_noheader.tsv
