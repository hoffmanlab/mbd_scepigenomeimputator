# Reprediction of TSS vs Enhancer sites from Chromatin Accessibility via interpreting CAGE data from FANTOM
This is using the new dataset in order to determine TSS (labelled "Promoters" in my output) vs Enhancer regions

### Left out samples:
Validation: 

~~~
skin_fibroblast	ENCODE	ENCFF001WFF.bed.gz	skin_fibroblast_fantom5	CL:0002620_skin_fibroblast_expressed_enhancers.bed 

thymus	ENCODE	ENCFF278XGA.bed.gz	thymus_fantom5	UBERON:0002370_thymus_expressed_enhancers.bed 
~~~

Testing: 

~~~
t-cell	ENCODE	ENCFF345YDG.bed.gz	t-cell_fantom5	CL:0000084_T_cell_expressed_enhancers.bed 

pancreas	ENCODE	ENCFF231QBI.bed.gz	pancreas_fantom5	UBERON:0001264_pancreas_expressed_enhancers.bed 
~~~

## Steps of this analysis:
1. Redo scores/distances for the four left out samples: match-region.sh 

2. Recreate the catalogue: rebuild-catalogue.sh 

3. mv score unparsed-score

4. python3 reparse-score-facet.sh on each

5. ./make-run-validation-cmd.sh
