#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

qsub -l h_vmem=16G -l mem_requested=16G -pe smp 8 -N TrainLinearSVC ./train-and-validate.sh 0 2018-11-21-imputedrna-keepquies-SVC.png
qsub -l h_vmem=16G -l mem_requested=16G -pe smp 8 -N TrainExtraTrees ./train-and-validate.sh 1 2018-11-21-imputedrna-keepquies-ExtraTrees.png
qsub -l h_vmem=8G -l mem_requested=8G -pe smp 16 -N TrainRandomForest ./train-and-validate.sh 2 2018-11-21-imputedrna-keepquies-RandomForest.png
qsub -l h_vmem=16G -l mem_requested=16G -pe smp 8 -N TrainKNN ./train-and-validate.sh 3 2018-11-21-imputedrna-keepquies-KNN.png
qsub -l h_vmem=16G -l mem_requested=16G -pe smp 8 -N TrainBernoulliNB ./train-and-validate.sh 4 2018-11-21-imputedrna-keepquies-BernoulliNB.png
qsub -l h_vmem=16G -l mem_requested=16G -pe smp 8 -N TrainGaussianNB ./train-and-validate.sh 5 2018-11-21-imputedrna-keepquies-GaussianNB.png
#qsub -l h_vmem=16G -l mem_requested=16G -pe smp 8 -N TrainLabelProp ./train-and-validate.sh 6 2018-11-21-imputedrna-keepquies-LabelProp.png
#qsub -l h_vmem=16G -l mem_requested=16G -pe smp 8 -N TrainLabelSpread ./train-and-validate.sh 7 2018-11-21-imputedrna-keepquies-LabelSpread.png
qsub -l h_vmem=16G -l mem_requested=16G -pe smp 8 -N TrainLinearDiscrim ./train-and-validate.sh 8 2018-11-21-imputedrna-keepquies-LinearDiscriminant.png
qsub -l h_vmem=16G -l mem_requested=16G -pe smp 8 -N TrainLogRegression ./train-and-validate.sh 9 2018-11-21-imputedrna-keepquies-LogRegression.png
qsub -l h_vmem=16G -l mem_requested=16G -pe smp 8 -N TrainAdaBoost ./train-and-validate.sh 11 2018-11-21-imputedrna-keepquies-AdaBoost.png
#qsub -pe smp 32 -N QuiesMultiLayerPerceptron ./train-and-validate.sh 10 2018-11-21-imputedrna-keepquies-MultiLayerPerceptron.png
