#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

qsub -l h_vmem=30G -l mem_requested=30G -pe smp 8 -N TrainLinearSVC ./train-and-test.sh 0 2018-12-18-imputedrna-keepquies-SVC.png
qsub -l h_vmem=30G -l mem_requested=30G -pe smp 8 -N TrainExtraTrees ./train-and-test.sh 1 2018-12-18-imputedrna-keepquies-ExtraTrees.png
qsub -l h_vmem=15G -l mem_requested=15G -pe smp 16 -N TrainRandomForest ./train-and-test.sh 2 2018-12-18-imputedrna-keepquies-RandomForest.png
qsub -l h_vmem=30G -l mem_requested=30G -pe smp 8 -N TrainKNN ./train-and-test.sh 3 2018-12-18-imputedrna-keepquies-KNN.png
qsub -l h_vmem=30G -l mem_requested=30G -pe smp 8 -N TrainBernoulliNB ./train-and-test.sh 4 2018-12-18-imputedrna-keepquies-BernoulliNB.png
qsub -l h_vmem=30G -l mem_requested=30G -pe smp 8 -N TrainGaussianNB ./train-and-test.sh 5 2018-12-18-imputedrna-keepquies-GaussianNB.png
#qsub -l h_vmem=30G -l mem_requested=30G -pe smp 8 -N TrainLabelProp ./train-and-test.sh 6 2018-12-18-imputedrna-keepquies-LabelProp.png
#qsub -l h_vmem=30G -l mem_requested=30G -pe smp 8 -N TrainLabelSpread ./train-and-test.sh 7 2018-12-18-imputedrna-keepquies-LabelSpread.png
qsub -l h_vmem=30G -l mem_requested=30G -pe smp 8 -N TrainLinearDiscrim ./train-and-test.sh 8 2018-12-18-imputedrna-keepquies-LinearDiscriminant.png
qsub -l h_vmem=30G -l mem_requested=30G -pe smp 8 -N TrainLogRegression ./train-and-test.sh 9 2018-12-18-imputedrna-keepquies-LogRegression.png
qsub -l h_vmem=30G -l mem_requested=30G -pe smp 8 -N TrainAdaBoost ./train-and-test.sh 11 2018-12-18-imputedrna-keepquies-AdaBoost.png
#qsub -pe smp 32 -N QuiesMultiLayerPerceptron ./train-and-test.sh 10 2018-12-18-imputedrna-keepquies-MultiLayerPerceptron.png
