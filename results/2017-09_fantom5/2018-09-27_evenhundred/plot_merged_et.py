#!/usr/bin/env python

"""
Docstring
"""

import argparse
import re
import sys

import matplotlib.pyplot as plt
import numpy as np
from palettable.colorbrewer.qualitative import Set2_6
from matplotlib.patches import Rectangle


def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Reduce columns in the given input file into a less memory
    intensive form
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "--roc",
        nargs="+",
        help="""*_roc.txt files from train-and-plot-ET.py"""
        )
    parser.add_argument(
        "--pr",
        nargs="+",
        help="""*_pr.txt files from train-and-plot-ET.py"""
        )
    parser.add_argument(
        "out_fn",
        default="Merged_ET_ROC_PR.svg",
        help="Output file name"
        )
    parser.add_argument(
        "class_pct",
        default=0.1911,
        type=float,
        help="Percentage of true values in the training set"
        )
    parser.add_argument(
        "--top-n",
        default=None,
        help="Top n algorithms to plot"
        )
    parser.add_argument(
        "--one-nn",
        action="store_true",
        help="Add the one-NN points"
        )
    args = parser.parse_args()
    if len(args.roc) == 0:
        raise ValueError("No ROC files given")
    if len(args.pr) == 0:
        raise ValueError("No PR files given")
    if len(args.roc) != len(args.pr):
        raise ValueError("Unequal number of ROC and PR files given")
    return args


def read_files(filenames):
    """
    Read input curve files.
    Takes as input a list of filenames, containing tab-delimited data on two
    rows. The first row is the Y values, the second row is the X values.
    """
    # TODO: Input validation?
    x = list()
    y = list()
    areas = list()
    for filename in filenames:
        with open(filename, 'r') as in_file:
            areas_txt = in_file.readline().strip()
            areas.append(float(re.search(r'0: ([0-9.]+)', areas_txt).group(1)))
            x.append(map(float, in_file.readline().strip().split("\t")))
            y.append(map(float, in_file.readline().strip().split("\t")))
    return(x, y, areas)


def _plot_curve(ax, x, y, auc, label_text, xlabel, ylabel,
                bbox_to_anchor, colors, alg_names, to_bold=None):
    # Plot the given curve
    lines = []
    labels = []

    # Insert the bolded class
    colors_copy = colors[:]
    #if to_bold:
    #    idx = alg_names.index(to_bold)
    #    colors_copy.insert(idx, "#000000")

    # Plot each class seperately
    num_classes = len(np.unique(y))
    for i, color in zip(range(num_classes), colors_copy):
        lw = 2
        fmt = '-'   # default
        if (alg_names[i] == 'Baseline'):
            fmt = 'k:'
            color = "#000000"
        elif (alg_names[i] == to_bold):
            lw = 5
            color = "#000000"
        l, = ax.plot(x[i], y[i], fmt, color=color, lw=lw)
        lines.append(l)
        labels.append(label_text.format(alg_names[i], auc[i]))

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_ylim([0.0, 1.0])
    ax.set_xlim([0.0, 1.0])

    return lines

    ## Shrink current axis's height by 40% on the bottom
    #box = ax.get_position()
    #ax.set_position([box.x0, box.y0 + box.height * 0.4,
    #                 box.width, box.height * 0.6])

    ## Put a legend below current axis
    #ax.legend(lines, labels, loc='upper center', bbox_to_anchor=bbox_to_anchor,
    #          fancybox=True)


def plot_all(output_fn, roc_fp, roc_tp, recall, precision, au_roc,
             au_pr, class_pct, max_depth, split_criterion,
             bagged_features, alg_names, to_bold=None, dpi=100, one_nn=False):
    """
    Plot the roc/auc, saving it as output_fn
    """

    # Set up pyplot
    plt.switch_backend('agg')
    colors = Set2_6.hex_colors
    fig = plt.figure(figsize=(20, 14))
    font = {'size': 20}
    plt.rc('font', **font)

    # Insert the random-case lines
    roc_fp.append([0, 1])
    roc_tp.append([0, 1])
    au_roc.append(0.50)
    recall.append([0, 1])
    precision.append([class_pct, class_pct])
    au_pr.append(class_pct)
    colors.append('black')
    alg_names.append('Baseline')
    # Plot the ROC curve and random-case line
    ax_roc = plt.subplot(1, 2, 1)
    _plot_curve(
        ax_roc,
        roc_fp,
        roc_tp,
        au_roc,
        '{0} (auROC = {1:0.2f})',
        r'False positive rate$ = \frac{\mathrm{FP}}{\mathrm{FP}+\mathrm{TN}}$',
        r'True positive rate$ = \frac{\mathrm{TP}}{\mathrm{TP}+\mathrm{FN}}$',
        (0.45, -0.15),
        colors,
        alg_names,
        to_bold=to_bold
        )
    #ax_roc.plot([0, 1], [0, 1], 'k:', alpha=0.75)

    # Plot the PR curve and random-case lines per class
    ax_pr = plt.subplot(1, 2, 2)
    lines = _plot_curve(
        ax_pr,
        recall,
        precision,
        au_pr,
        '{0} (auPR = {1:0.2f})',
        r'Recall$ = \frac{\mathrm{TP}}{\mathrm{TP}+\mathrm{FN}}$',
        r'Precision$ = \frac{\mathrm{TP}}{\mathrm{TP}+\mathrm{FP}}$',
        (0.55, -0.15),
        colors,
        alg_names,
        to_bold=to_bold
        )
    #ax_pr.axhline(class_pct, 0.0, 1.0, linestyle=':', alpha=0.75, color='black')

    # 1-NN analysis
    if one_nn:
        ax_roc.plot(0.0404, 0.1639, color="#000000", marker='o')
        ax_pr.plot(0.1639, 0.4569, color="#000000", marker='o')

    # performance_str = 'auPR={0:0.2f}'.format(au_pr[0])
    # plt.suptitle(
    #     '3-class ROC/PR curves: {0}\n{1}'.format(
    #         performance_str,
    #         str(model.best_params_)
    #         )
    #     )

    # Shrink each axis's height by 40% on the bottom
    for ax in fig.axes:
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.4,
                         box.width, box.height * 0.6])

    # Generate a tabular legend
    # code from https://stackoverflow.com/a/25995730/2148998
    # create blank rectangle
    empty = Rectangle((0, 0), 0.1, 0.1, fc="w", fill=False, edgecolor='none', linewidth=0)

    # Create organized list containing all handles for table.
    # empty is used for places where text will go
    # Please excuse the mesiness of the following - it makes it clearer
    # what the final layout will be
    num_columns = 5
    num_rows = 7
    legend_handle = [empty, lines[0], lines[1], lines[2], lines[3], lines[4], lines[5],
                     empty, empty, empty, empty, empty, empty, empty,
                     empty, empty, empty, empty, empty, empty, empty,
                     empty, empty, empty, empty, empty, empty, empty,
                     empty, empty, empty, empty, empty, empty, empty]

    # Fill in the left column of labels
    lines = [""] * num_rows
    split_criterion = ["Split criterion"] + split_criterion + ["Baseline"]
    bagged_features = ["Features per bootstrap"] + bagged_features + [""]
    au_roc = ["auROC"] + ["{:0.2f}".format(roc) for roc in au_roc]
    au_pr = ["auPR"] + ["{:0.2f}".format(pr) for pr in au_pr]
    labels = np.concatenate([lines, split_criterion, bagged_features,
                             au_roc, au_pr])

    legend = plt.legend(legend_handle, labels, loc='upper center',
                        ncol=num_columns, bbox_to_anchor=(0.0, -0.15),
                        frameon=False)

    # Set numeric text and auROC/auPR to be right-aligned
    for cell in legend.get_texts():
        text = cell.get_text()
        try:
            if text in ["auROC", "auPR"] or float(text):
                cell.set_ha('right')
                cell.set_width
        except:
            # Don't affect anything else
            pass

    fig1 = plt.gcf()
    fig1.savefig(output_fn, dpi=dpi, bbox_inches='tight')


def main():
    """Execute this program from command line
    """
    args = parse_args()

    # Read input
    (roc_fp, roc_tp, au_roc) = read_files(args.roc)
    (recall, precision, au_pr) = read_files(args.pr)

    # Reorder according to descending auPR
    order = np.argsort(au_pr).tolist()
    order.reverse()  # To descending order
    if args.top_n:
        order = order[0:int(args.top_n)]
    roc_fp = [roc_fp[i] for i in order]
    roc_tp = [roc_tp[i] for i in order]
    au_roc = [au_roc[i] for i in order]
    recall = [recall[i] for i in order]
    precision = [precision[i] for i in order]
    au_pr = [au_pr[i] for i in order]

    # Construct the alg_names
    alg_names = []
    max_depth = []
    max_features = []
    criterion = []
    to_bold = ""
    for idx in order:
        i = int(re.search(r'(\d+)_pr.txt', args.pr[idx]).group(1))
        n_estimators = [5, 10, 100][i % 3]
        criterion.append(['Gini impurity', 'Information gain'][i//3 % 2])
        max_depth.append(['3', 'None'][i//6 % 2])
        max_features.append([r'$\sqrt{p}=20$',
                             r'$\log_2p=8$', '$p=406$'][i//12 % 3])

        alg_names.append('{} trees with max depth {}, split by {}\non {} features'.format(
            n_estimators, max_depth[-1], criterion[-1], max_features[-1]))
        if i == 8:
            to_bold = alg_names[-1]
    #alg_names = [args.alg_names[i] for i in order]

    # Plot all of them
    plot_all(args.out_fn, roc_fp, roc_tp, recall, precision, au_roc,
             au_pr, args.class_pct, max_depth, criterion, max_features,
             alg_names, to_bold=to_bold, dpi=100, one_nn=args.one_nn)

if __name__ == "__main__":
    sys.exit(main())
