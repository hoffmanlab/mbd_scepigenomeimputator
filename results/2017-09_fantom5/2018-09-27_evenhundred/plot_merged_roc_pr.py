#!/usr/bin/env python

"""
Docstring
"""

import argparse
import re
import sys

import matplotlib.pyplot as plt
import numpy as np
from palettable.colorbrewer.qualitative import Set2_7
from matplotlib.patches import Rectangle


def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Reduce columns in the given input file into a less memory
    intensive form
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "--roc",
        nargs="+",
        help="""*_roc.txt files from train.py"""
        )
    parser.add_argument(
        "--pr",
        nargs="+",
        help="""*_pr.txt files from train.py"""
        )
    parser.add_argument(
        "--alg-names",
        nargs="+",
        help="""Names of the algorithms performed"""
        )
    parser.add_argument(
        "--bold",
        help="""Name of the algorithm to bold"""
        )
    parser.add_argument(
        "--one-nn",
        help="Include the point from the one-nearest-neighbour analysis",
        action="store_true"
        )
    parser.add_argument(
        "out_fn",
        default="Merged_ROC_PR.svg",
        help="Output file name"
        )
    args = parser.parse_args()
    if len(args.roc) == 0:
        raise ValueError("No ROC files given")
    if len(args.pr) == 0:
        raise ValueError("No PR files given")
    if len(args.roc) != len(args.pr):
        raise ValueError("Unequal number of ROC and PR files given")
    if len(args.roc) != len(args.alg_names):
        raise ValueError("Unequal number of algorithm names and files given")
    return args


def read_files(filenames):
    """
    Read input curve files.
    Takes as input a list of filenames, containing tab-delimited data on two
    rows. The first row is the Y values, the second row is the X values.
    """
    # TODO: Input validation?
    x = list()
    y = list()
    areas = list()
    for filename in filenames:
        with open(filename, 'r') as in_file:
            areas_txt = in_file.readline().strip()
            areas.append(float(re.search(r'0: ([0-9.]+)', areas_txt).group(1)))
            #areas.append(float(in_file.readline().strip()))
            x.append(map(float, in_file.readline().strip().split("\t")))
            y.append(map(float, in_file.readline().strip().split("\t")))
    return(x, y, areas)


def _plot_curve(ax, x, y, auc, label_text, xlabel, ylabel,
                bbox_to_anchor, colors, alg_names, to_bold=None):
    # Plot the given curve
    lines = []
    labels = []


    # Insert the bolded class
    colors_copy = colors[:]
    if to_bold:
        idx = alg_names.index(to_bold)
        colors_copy.insert(idx, "#000000")

    # Plot each class seperately
    num_classes = len(np.unique(y))
    for i, color in zip(range(num_classes), colors_copy):
        lw = 2
        fmt = '-'   # default
        if (alg_names[i] == to_bold):
            lw = 5
        elif (alg_names[i] == 'Baseline'):
            fmt = 'k:'
        l, = ax.plot(x[i], y[i], fmt, color=color, lw=lw)
        lines.append(l)
        labels.append(label_text.format(alg_names[i], auc[i]))

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_ylim([0.0, 1.0])
    ax.set_xlim([0.0, 1.0])

    return lines
    ## Shrink current axis's height by 40% on the bottom
    #box = ax.get_position()
    #ax.set_position([box.x0, box.y0 + box.height * 0.4,
    #                 box.width, box.height * 0.6])

    ## Put a legend below current axis
    #ax.legend(lines, labels, loc='upper center', bbox_to_anchor=bbox_to_anchor,
    #          fancybox=True)


def plot_all(output_fn, roc_fp, roc_tp, recall, precision, au_roc,
             au_pr, alg_names, to_bold=None, dpi=100, one_nn=False):
    """
    Plot the roc/auc, saving it as output_fn

    Keyword arguments:
    to_bold -- name of an algorithm to bold, (None or non-existent names will
                result in no bolded line)
    dpi -- output dpi, used when output_fn is a .png
    one_nn -- whether or not to include the dot from the one_nn analysis
    """

    # Set up pyplot
    plt.switch_backend('agg')
    colors = Set2_7.hex_colors
    fig = plt.figure(figsize=(16, 13))
    font = {'size': 20}
    plt.rc('font', **font)

    # Determine the class percentage
    class_pct = precision[0][-1]

    # Insert the random-case lines
    roc_fp.append([0, 1])
    roc_tp.append([0, 1])
    au_roc.append(0.50)
    recall.append([0, 1])
    precision.append([class_pct, class_pct])
    au_pr.append(class_pct)
    colors.append('black')
    alg_names.append('Baseline')
    # Plot the ROC curve and random-case line
    ax_roc = plt.subplot(1, 2, 1)
    _plot_curve(
        ax_roc,
        roc_fp,
        roc_tp,
        au_roc,
        '{0} (auROC = {1:0.2f})',
        r'False positive rate$ = \frac{\mathrm{FP}}{\mathrm{FP}+\mathrm{TN}}$',
        r'True positive rate$ = \frac{\mathrm{TP}}{\mathrm{TP}+\mathrm{FN}}$',
        (0.45, -0.15),
        colors,
        alg_names,
        to_bold=to_bold
        )
    #ax_roc.plot([0, 1], [0, 1], 'k:', alpha=0.75)
    ax_roc.set_title('A', loc='left')

    # Plot the PR curve and random-case lines per class
    ax_pr = plt.subplot(1, 2, 2)
    lines = _plot_curve(  # capture (lines, labels) for the legend
        ax_pr,
        recall,
        precision,
        au_pr,
        '{0} (auPR = {1:0.2f})',
        r'Recall$ = \frac{\mathrm{TP}}{\mathrm{TP}+\mathrm{FN}}$',
        r'Precision$ = \frac{\mathrm{TP}}{\mathrm{TP}+\mathrm{FP}}$',
        (0.55, -0.15),
        colors,
        alg_names,
        to_bold=to_bold
        )
    #ax_pr.axhline(class_pct, 0.0, 1.0, linestyle=':', alpha=0.75, color='black')
    ax_pr.set_title('B', loc='left')

    # 1-NN analysis
    if one_nn:
        ax_roc.plot(0.0404, 0.1639, color="#000000", marker='o')
        ax_pr.plot(0.1639, 0.4569, color="#000000", marker='o')

    # Shrink each axis's height by 40% on the bottom
    for ax in fig.axes:
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.4,
                         box.width, box.height * 0.6])

    # Generate a tabular legend
    # code from https://stackoverflow.com/a/25995730/2148998
    # create blank rectangle
    empty = Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor='none', linewidth=0)

    # Create organized list containing all handles for table.
    # empty is used for places where text will go
    num_columns = 3
    #num_rows = 10
    legend_handle = [empty] + lines + [
                     empty, empty, empty, empty, empty, empty, empty, empty, empty, empty,
                     empty, empty, empty, empty, empty, empty, empty, empty, empty, empty]

    alg_names = [""] + alg_names
    au_roc = ["auROC"] + ["{:0.2f}".format(roc) for roc in au_roc]
    au_pr = ["auPR"] + ["{:0.2f}".format(pr) for pr in au_pr]
    labels = np.concatenate([alg_names, au_roc, au_pr])

    #oneline_labels = ["{0} (auROC = {1:0.2f}; auPR = {2:0.2f})".format(
    #                    name, roc, pr) for (name, roc, pr)
    #                    in zip(alg_names, au_roc, au_pr)]
    legend = plt.legend(legend_handle, labels, loc='upper center', ncol=num_columns,
                        bbox_to_anchor=(-0.15, -0.15), frameon=False)

    # Set numeric text and auROC/auPR to be right-aligned
    for cell in legend.get_texts():
        text = cell.get_text()
        try:
            if text in ["auROC", "auPR"] or float(text):
                cell.set_ha('right')
        except:
            # Don't affect anything else
            pass

    fig1 = plt.gcf()
    fig1.savefig(output_fn, dpi=dpi, bbox_inches='tight')


def main():
    """Execute this program from command line
    """
    args = parse_args()

    # Read input
    (roc_fp, roc_tp, au_roc) = read_files(args.roc)
    (recall, precision, au_pr) = read_files(args.pr)

    # Reorder according to descending auPR
    order = np.argsort(au_pr).tolist()
    order.reverse()  # To descending order
    roc_fp = [roc_fp[i] for i in order]
    roc_tp = [roc_tp[i] for i in order]
    au_roc = [au_roc[i] for i in order]
    recall = [recall[i] for i in order]
    precision = [precision[i] for i in order]
    au_pr = [au_pr[i] for i in order]
    alg_names = [args.alg_names[i] for i in order]

    # Plot all of them
    plot_all(args.out_fn, roc_fp, roc_tp, recall, precision, au_roc,
             au_pr, alg_names, to_bold=args.bold, dpi=100,
             one_nn=args.one_nn)

if __name__ == "__main__":
    sys.exit(main())
