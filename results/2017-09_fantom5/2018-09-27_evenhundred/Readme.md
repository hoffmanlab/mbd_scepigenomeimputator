Steps: 

1) Figure out which regions can vary, using `bedtools intersect -a genome.100.sorted.bed -b 

RF-analysis: `python rf-get-feature-importance.py binned-score/ varying-locations-merged.bed binned-data/ mappable_impute_training.tsv 20300 --start-idx 0 --end-idx 6500 --rna-dir combined-rna/ --validation mappable_test.tsv --processing-dir processed-impute/ --output-fn 2018-12-17-ET-Feature-Importance.svg --output-model-fn model-et.pkl`
