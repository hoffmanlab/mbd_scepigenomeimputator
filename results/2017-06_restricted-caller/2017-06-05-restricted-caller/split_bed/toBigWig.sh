#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

module load ucsctools/315
CHROM_SIZES=../../../data/2017-01-23/hg19.chrom.sizes.txt
#bedGraphToBigWig Bivalent.bedGraph  Bivalent.bw
for INFILE in *.bedGraph; do
    BASENAME=`basename $INFILE`
    ROOT=${BASENAME%%.bedGraph}
    bedClip $INFILE $CHROM_SIZES ${ROOT}_tmp.bedGraph
    bedGraphToBigWig ${ROOT}_tmp.bedGraph $CHROM_SIZES ${ROOT}.bw
    rm ${ROOT}_tmp.bedGraph
done
