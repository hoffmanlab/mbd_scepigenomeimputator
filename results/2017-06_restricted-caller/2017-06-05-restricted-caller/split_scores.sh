#!/bin/bash
#$ -cwd
#$ -q hoffmangroup
#$ -t 1-10

# verbose failures
set -o nounset -o pipefail -o errexit

#for SGE_TASK_ID in `seq 1 10`; do
COL_OF_INTEREST=`expr $SGE_TASK_ID + 3`
OUT_FILE=`sed "${SGE_TASK_ID}q;d" mapping.txt`.bedGraph

cut -f 1,2,3,$COL_OF_INTEREST all_scores_collapsed.tsv >$OUT_FILE
#done
