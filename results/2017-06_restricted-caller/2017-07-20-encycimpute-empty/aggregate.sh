#!/bin/bash
#$ -cwd
#$ -q hoffmangroup
#$ -l h_vmem=16G
#$ -l mem_requested=16G
#$ -N aggregate-EP
# Aggregates the results of windower.sh

# verbose failures
set -o nounset -o pipefail -o errexit

MAPPING_FILE=mappable.txt
FIRST_FILE=`head -n 1 $MAPPING_FILE | cut -f 1`
cut -f 1,2,3 score/Catalogue-EP-${FIRST_FILE}-score.bed >reduced.scores.tsv
SCORE_FILES=`cut -f 1 $MAPPING_FILE`
for f in $SCORE_FILES; do
    echo ${f}.score.bed
    cut -f 4 score/Catalogue-EP-${f}-score.bed | paste reduced.scores.tsv - >reduced.scores.tmp.tsv
    mv reduced.scores.tmp.tsv reduced.scores.tsv
done
