#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

#./split.sh A549_repredict.tsv
#./mergeSimilar.sh
#rm -r split/
#mv merged A549-merged/
#mv all.bedGraph A549.bedGraph

./split.sh FetalKidney_repredict.tsv
./mergeSimilar.sh
rm -r split/
mv merged FetalKidney-merged/
mv all.bedGraph FetalKidney.bedGraph
