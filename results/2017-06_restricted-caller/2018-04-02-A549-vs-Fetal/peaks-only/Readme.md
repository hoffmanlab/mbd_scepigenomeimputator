# History

zgrep "[^.]$" A549-peaks.bed.gz >A549-peaks-only.bed
ln -s binned-data/UW.Fetal_Kidney.ChromatinAccessibility.H-24510.DNase.DS20786.narrowPeak.bed.gz FetalKidney-peaks.bed.gz
vim FetalKidney-peaks.bed.gz 
zgrep "[^.]$" FetalKidney-peaks.bed.gz >FetalKidney-peaks-only.bed
head FetalKidney-peaks-only.bed 
ll ../../2017-09-07-leave2out/
ln -s ../../../../data/processed/2017-07-26_chr17only/hg19.chrom.sizes.sorted.txt .
bedtools slop -l 500 -r 500 -i FetalKidney-peaks-only.bed -g hg19.chrom.sizes.sorted.txt >FetalKidney-slop.bed
cat FetalKidney-slop.bed 
bedtools merge -i FetalKidney-slop.bed >FetalKidney-slop-merged.bed
vim FetalKidney-slop-merged.bed 
bedtools slop -l 500 -r 500 -i A549-peaks-only.bed -g hg19.chrom.sizes.sorted.txt >A549-slop.bed
bedtools merge -i A549-slop.bed >A549-slop-merged.bed
history
bedtools intersect -wa -a A549_repredict.tsv -b A549-slop.bed >A549_repredict_trim.bed
bedtools intersect --sorted -wa -a A549_repredict.tsv -b A549-slop.bed >A549_repredict_trim.bed
bedtools intersect -sorted -wa -a A549_repredict.tsv -b A549-slop.bed >A549_repredict_trim.bed
vim A549_repredict_trim.bed 
bedtools intersect -sorted -wa -a A549_repredict.tsv -b A549-slop-merged.bed >A549_repredict_trim.bed
bedtools intersect -sorted -wa -a FetalKidney_repredict.tsv -b FetalKidney-slop-merged.bed >FetalKidney_repredict_trim.bed
cat A549_repredict_trim.bed FetalKidney_repredict_trim.bed >testing_repredict_trim.bed

