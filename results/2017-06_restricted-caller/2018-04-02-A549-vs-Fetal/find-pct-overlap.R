# Test

labels <- c(
    "LowConfidence",
    "Bivalent",
    "Enhancer",
    "FacultativeHet",
    "ConstitutiveHet",
    "Promoter",
    "RegPermissive",
    "Quiescent",
    "Transcribed"
    )

all.TP <- 0
total.cov <- 0
for (label in labels) {
    # Compare the two
    print(label)
    asdf <- system2(
        "/mnt/work1/users/home2/fnguyen/anaconda3/envs/py27/bin/bedtools",
        args = c(
            "jaccard",
            "-a",
            paste("A549_", label, ".bed", sep=""),
            "-b",
            paste("FetalKidney_", label, ".bed", sep="")
            ),
        stdout = TRUE
        )
    stats <- strsplit(asdf[2], "\t")[[1]]
    all.TP <- all.TP + as.integer(stats[1])
    total.cov <- total.cov + as.integer(stats[2])
}
