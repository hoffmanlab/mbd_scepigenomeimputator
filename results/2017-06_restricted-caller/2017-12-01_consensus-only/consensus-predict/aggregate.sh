#!/bin/bash
#$ -cwd
#$ -q hoffmangroup
#$ -l h_vmem=16G
#$ -l mem_requested=16G
#$ -N aggregate
# Aggregates the results of windower.sh

# verbose failures
set -o nounset -o pipefail -o errexit

PREFIX=$1
MAPPING_FILE=$2 #mappable.txt

FIRST_FILE=`head -n 1 $MAPPING_FILE | cut -f 1`
gunzip -cd score/${FIRST_FILE}.score.bed.gz | cut -f 1,2,3 - >reduced_scores.tsv
#gunzip -cd score/$PREFIX-${FIRST_FILE}.score.bed.gz | cut -f 1,2,3 - >reduced_scores.tsv
SCORE_FILES=`cut -f 4 $MAPPING_FILE`
for f in $SCORE_FILES; do
    echo ${f}.score.bed
    gunzip -cd score/${f}.score.bed.gz | cut -f 4,5 - | paste reduced_scores.tsv - >reduced_scores_tmp.tsv
    #gunzip -cd score/$PREFIX-${f}.score.bed.gz | cut -f 4,5 - | paste reduced_scores.tsv - >reduced_scores_tmp.tsv
    mv reduced_scores_tmp.tsv reduced_scores.tsv
done
