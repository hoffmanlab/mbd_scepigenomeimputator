# Consensus-only look
This is an attempt to redo the PR curves for a consensus-only look at the genome
using the same cell lines as the leave-4-out analysis.

### Validation Set 
~~~
NB4	ENCODE	ENCFF001BYP.bed.gz	NB4 

BREAST_VHMEC	ROADMAP	GSM753973_UW.Breast_vHMEC.ChromatinAccessibility.RM035.DS18406.bed.gz	BREAST_VHMEC 
~~~

### Testing Set
~~~
A549	ENCODE	ENCFF001APE.bed.gz	A549 

FETAL_KIDNEY	ROADMAP	GSM1024607_UW.Fetal_Kidney_Right.ChromatinAccessibility.H-24568.DS20917.bed.gz	FETAL_KIDNEY 
~~~

## Commands
Redid everything manually because my scripts weren't so great at not having the score values

~~~
./aggregate.sh '' mappable_leave4out.txt 

python3 reduce.py reduced_scores.tsv reduced_scores_reduced.tsv 

python3 scripts/collapse.py --rectangularkernel reduced_scores_reduced.tsv reduced_scores_collapsed.tsv 

./scripts/split.sh reduced_scores_collapsed.tsv 

./scripts/mergeSimilar.sh
~~~

## Replotting
./validate-runs.sh A549.remap.bed consensus-predict/ 

mv consensus-predict/validate A549Validate 

./validate-runs.sh FETAL_KIDNEY.remap.bed consensus-predict/ 

mv consensus-predict/validate FetalKidneyValidate 

Rscript ./scripts/validate-cohenskappa-merge.R --output="merge" consensus-predict/*/_cohens_matrix.tsv 

## Results 
Combined Cohen's Kappa of 0.2505 -- basically identical to my ATAC-Seq tries

