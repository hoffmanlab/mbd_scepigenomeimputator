# Sliding-window analysis
This analysis is meant to explore the effects of applying a sliding window onto the predictions, such that proximal predictions are allowed to affect each bin
I'm imagining this as working in the same way as neural net approaches to the problem

Generated genome.100.offset.bed in an interactive R session by adding 50 to the start/end and removing the last position

# Command Log
qsub ./windower.sh 100 segway-annot/ 1,2,3,7 score -f 0.50
