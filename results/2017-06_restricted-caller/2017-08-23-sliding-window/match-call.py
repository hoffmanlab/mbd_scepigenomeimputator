"""
Uses a sliding window approach to determine the call for a particular file
"""

import gzip

# Enum of the various parts of the reduced_scores_collapsed file
CHR=0
START=1
END=2

def parse_args():
    parser = argparse.ArgumentParser(description = """
        Uses a sliding window approach to determine the call at each peak
        """)
    parser.add_argument('infile', nargs = 1)
    parser.add_argument(
        'base_call',
        nargs = '1',
        help = """The base reduced_scores_collapsed.tsv.gz file""")
    parser.add_argument(
        'sliding_call',
        nargs = '1',
        type = int,
        default = 4,
        help = "The sliding window reduced_scores_collapsed.tsv.gz file"
        )
    parser.add_argument(
        'output',
        nargs = '1'
        )
    return vars(parser.parse_args())

def load_file(filename, mode = 'rt'):
    """Loads the given file into a list, handling gzipped files properly"""
    if filename.endsWith('.gz'):
        with gzip.open(filename, mode) as infile:
            return infile.readlines()

    with open(filename, mode) as infile:
        return infile.readlines()

def recall_position(base_call, prev_slice, next_slice):
    with open(

def main():
    args = parse_args()

    base_file = load_file(args['base_call'])
    slide_file = load_file(args['sliding_call'])

    prev_slide = slide_file[0]
    for i in range(0, len(base_file)):
        # Parse this line from the base and sliding files
        this_base = base_file[i].strip().split("\t")
        next_slice = slide_file[i].strip().split("\t")

        # Ensure the next slice is part of the same chromosome
        if next_slice[CHR] != this_base[CHR]:
            # TODO: What do I do around the edges?
            continue

        recall_position()

    close(base_file)
    close(slide_file)

if __name__ == "__main__":
    main()
