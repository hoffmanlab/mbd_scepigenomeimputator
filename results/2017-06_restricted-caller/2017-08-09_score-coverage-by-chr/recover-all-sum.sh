#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail # -o errexit

for i in `seq 1 23`; do
    CHR_OF_INTEREST=$i
    if [ "$i" -eq "23" ]; then
        CHR_OF_INTEREST="X"
    fi
    FULL_CHR="chr$CHR_OF_INTEREST"

    sed "s/a\\[/chr.$i\\[/g" "$FULL_CHR/all-sum.txt" >"$FULL_CHR-all-sum.txt"
done
