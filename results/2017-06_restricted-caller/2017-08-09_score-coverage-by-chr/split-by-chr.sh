#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -t 2-22

# verbose failures
set -o nounset -o pipefail -o errexit

# Parse out the chromosome to check out
CHR_OF_INTEREST=$SGE_TASK_ID
if [ "$SGE_TASK_ID" -eq "23" ]; then
    CHR_OF_INTEREST="X"
fi
FULL_CHR="chr$CHR_OF_INTEREST"

mkdir -p $FULL_CHR/mappable-score

for FILE in mappable-score/*.gz; do
    BASE=`basename $FILE`
    gzip -cd $FILE | grep -P "$FULL_CHR\t" - | gzip -c - >$FULL_CHR/mappable-score/$BASE
done

cd $FULL_CHR
ln -s ../*.sh .
ln -s ../scripts .
ln -s ../hg19.trim.chrom.sizes .
qsub -N ${FULL_CHR}-a a-remap-all.sh mappable-score remap-score
qsub -hold_jid ${FULL_CHR}-a -N ${FULL_CHR}-b b-merge-all.sh remap-score split-score
qsub -hold_jid ${FULL_CHR}-b -N ${FULL_CHR}-c c-split-by-label.sh split-score
qsub -hold_jid ${FULL_CHR}-c -N ${FULL_CHR}-d d-genomecov-labels.sh 
qsub -hold_jid ${FULL_CHR}-d -N ${FULL_CHR}-e e-sum-all.sh
