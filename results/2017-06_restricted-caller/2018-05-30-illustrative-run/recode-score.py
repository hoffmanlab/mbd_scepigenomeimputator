#!/usr/bin/env python
# recode-score.py ##############################################################
# Color codes the given score file

import argparse
import re

COLORS={
    "Bivalent": "0,0,0",
    "ConstitutiveHet": "0,0,100",
    "Enhancer": "0,100,100",
    "FacultativeHet": "0,100,0",
    "LowConfidence": "100,100,0",
    "NoCall": "100,0,0",
    "Promoter": "100,0,100",
    "Quiescent": "255,0,0",
    "RegPermissive": "0,255,0",
    "Transcribed": "0,0,255"
    }

NUM_TO_LABEL=[
    "NoCall",
    "Quiescent",
    "ConstitutiveHet",
    "FacultativeHet",
    "Transcribed",
    "Promoter",
    "Enhancer",
    "RegPermissive",
    "Bivalent",
    "LowConfidence"
    ]

BED_HEADER = 'track type=bed name="{}" description="{}" visibility=full autoScale=off height=30\n'

def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Takes the ConfidentCalls bedGraph and colourizes it according to the call
    Output will be a .bed file
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('infile')
    parser.add_argument('outfile')
    return vars(parser.parse_args())


def main():
    args = parse_args()

    get_score = re.compile(r"_(.+)")

    # Convert line by line
    with open(args['infile']) as infile, open(args['outfile'], 'w') as outfile:
        # Figure out what to call this track
        track_name = re.search(r"(.+).bed.gz", args['infile'])
        if track_name is None:
            track_name = args['infile']
        else:
            track_name = track_name.group(1)

        outfile.write(BED_HEADER.format(track_name, track_name))
        for line in infile:
            if line == "\n":
                outfile.write(line)
                continue
            to_edit = line.strip().split()

            # Keep the genomic coords (chr, start, end)
            outfile.write("\t".join(to_edit[0:3]))

            # Convert this label into key stats
            name = ""
            if to_edit[3] == ".":
                name = "NoCall"
            else:
                name = get_score.search(to_edit[3])
                if name is None:
                    print('"{}" can\'t be parsed'.format(line))
                name = name.group(1)
            color = COLORS[name]
            thick_start = to_edit[1]
            thick_end = to_edit[2]

            # Write all of the required elements of the .bed file
            outfile.write("\t" + "\t".join(
                [
                    name,
                    "1000",   # score
                    ".",    # strand
                    thick_start,
                    thick_end,
                    color
                ]))
            outfile.write("\n")

if __name__ == "__main__":
    main()
