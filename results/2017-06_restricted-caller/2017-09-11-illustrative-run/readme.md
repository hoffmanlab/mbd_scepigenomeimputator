# Temporary illustrative run
Used to create images of how the process works. 

Runs off of four features in chr17 -- This simultaneously generates data for a nice illustration of how the algorithm looks, as well as acting as a sanity check 

The locus of interest is chr17:37,838,328-37,844,925
