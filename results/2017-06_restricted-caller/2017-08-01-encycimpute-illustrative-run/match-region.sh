#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l h_vmem=8G
#$ -l mem_requested=8G
#$ -N match-patient
#$ -t 1-2

# match-region.sh:
# Grabs mappable regions for a given input catalogue, for every cell line in mappable.txt
# Meant to be qsub'd from an SGE cluster
# Requires Catalogue-EP.bed and Catalogue-IM.bed in the same folder
# qsub match-region.sh MAPPABLE_SAMPLE_FILE ENCYCLOPEDIA_DIR ACCESSIBILITY_FILES_DIR
# MAPPABLE_SAMPLE_FILE: four column file of cell-line	ROADMAP-OR-ENCODE-FILE	score-file-name	chrom-accessibility-file-prefix
# ENCYCLOPEDIA_DIR: directory of the Segway encyclopedia .gz files (as .score.bed files output via windower.sh)
# ACCESSIBILITY_FILES_DIR: directory of the chromatin accessibility experiments (as .bed files)

# verbose failures
set -o nounset -o pipefail 

module load python3

NUM_CELL_LINES=2

IN_MAPPABLE=$1	#./mappable.txt
SCORE_DIR=$2	#../mappable-score/ 
DATA_DIR=$3	# ../mappable-data/
IN_CATALOGUE=$4 # Input data file

# Determine the name of the cell line
CELL_LINE_NUM=`expr \( $SGE_TASK_ID - 1 \) % $NUM_CELL_LINES + 1`
CELL_LINE=`sed "${CELL_LINE_NUM}q;d" mappable.txt | cut -f 1 -`

mkdir -p collision
python3 match-region.py $IN_CATALOGUE $IN_MAPPABLE $SCORE_DIR $DATA_DIR $CELL_LINE
