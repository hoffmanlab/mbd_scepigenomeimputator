#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

module load python3

LABELS=( NonVoter Quiescent ConstitutiveHet FacultativeHet Transcribed Promoter Enhancer RegPermissive Bivalent LowConfidence )

# Blank out all-sum.txt
if [ -e all-sum.txt ]; then
    mv all-sum.txt >.all-sum.txt
fi
echo "" >all-sum.txt

# Sum everything together
for label in ${LABELS[@]}; do
    bp_correct=`tail -n 1 $label/*_genomecov.txt | grep -P "genome\t1" | cut -f 3 | awk '{s+=$1} END {print s}'`
    echo "a[[\"$label\"]] <- $bp_correct" >>all-sum.txt
done
