#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -t 1-34
#$ -tc 10

# verbose failures
set -o nounset -o pipefail -o errexit

module load python3

IN_DIR=($1/*.bed.gz)

IDX=$(($SGE_TASK_ID -1))
IN_FILE=${IN_DIR[$IDX]}
echo $IN_FILE
BASE_NAME=`basename $IN_FILE`

LABELS=( NonVoter Quiescent ConstitutiveHet FacultativeHet Transcribed Promoter Enhancer RegPermissive Bivalent LowConfidence )

UNZIPPED_IN=${BASE_NAME%%.gz}
gzip -cd $IN_FILE >$UNZIPPED_IN
for label in ${LABELS[@]}; do
    mkdir -p $label
    set +o errexit  # Don't send errors if the annotation doesn't exist
    grep "$label" $UNZIPPED_IN >$label/$UNZIPPED_IN
    set -o errexit
done
rm $UNZIPPED_IN
