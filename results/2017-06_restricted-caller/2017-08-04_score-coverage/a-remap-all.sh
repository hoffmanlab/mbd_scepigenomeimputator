#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -t 1-34
#$ -tc 10

# verbose failures
set -o nounset -o pipefail -o errexit

module load python3

IN_DIR=($1/*.bed*)
OUT_DIR=$2

set +o errexit  # SGE_TASK_ID=1 triggers this silently
IN_FILE=${IN_DIR[`expr $SGE_TASK_ID - 1`]}
set -o errexit
echo $IN_FILE
BASE_NAME=`basename $IN_FILE`
BASE_NAME=${BASE_NAME%%.gz}

mkdir -p $OUT_DIR
python3 scripts/remap.py $IN_FILE $OUT_DIR/$BASE_NAME 4
