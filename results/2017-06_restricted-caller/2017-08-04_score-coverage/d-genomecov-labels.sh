#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l h_vmem=5G
#$ -l mem_requested=5G
#$ -t 1-340
#$ -tc 5

# verbose failures
set -o nounset -o pipefail -o errexit

module load python3

# 0-base-index SGE_TASK_ID
IN_ID=$(($SGE_TASK_ID - 1))
FILES_TO_PROCESS=(*/*.bed)
TO_PROCESS=${FILES_TO_PROCESS[$IN_ID]}

bedtools genomecov -g hg19.trim.chrom.sizes -i $TO_PROCESS >${TO_PROCESS}_genomecov.txt
