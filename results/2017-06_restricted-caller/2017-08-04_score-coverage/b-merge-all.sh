#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -t 1-34
#$ -tc 10

# verbose failures
set -o nounset -o pipefail -o errexit

module load python3

IN_DIR=($1/*.bed)
OUT_DIR=$2

IDX=$(($SGE_TASK_ID -1))
IN_FILE=${IN_DIR[$IDX]}
echo $IN_FILE
BASE_NAME=`basename $IN_FILE`

mkdir -p $OUT_DIR
python3 scripts/mergeSimilar.py $IN_FILE | gzip -c - >$OUT_DIR/$BASE_NAME.gz
gzip $IN_FILE
