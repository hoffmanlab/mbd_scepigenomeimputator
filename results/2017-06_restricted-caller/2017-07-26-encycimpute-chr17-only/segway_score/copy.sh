#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

for i in ../../../data/segway-annot/*.bed.gz; do 
    set +o errexit
    Z=`file $i | grep "ASCII"`; 
    set -o errexit
    BASE=`basename $i`
    echo $BASE
    if [ "$Z" == "" ]; then
        gzip -cd $i | grep "chr17" | gzip - >$BASE;
    else 
        grep "chr17" $i | gzip - >$BASE;
    fi 
done
