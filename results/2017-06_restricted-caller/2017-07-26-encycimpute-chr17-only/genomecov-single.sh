#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l h_vmem=8G
#$ -l mem_requested=8G
#$ -N genomecov-patient
#$ -t 1-36
#$ -tc 40

# verbose failures
set -o nounset -o pipefail -o errexit

module load bedtools/2.23.0

# Calculate all of the g
IN_DIR=(mappable-data/*.bed.gz)
IN_FILE=${IN_DIR[$SGE_TASK_ID-1]}
IN_ROOT=${IN_FILE%%.bed}

bedtools genomecov -g hg19.chrom.sizes.sorted.txt -i $IN_FILE >${IN_ROOT}_genomecov.txt
