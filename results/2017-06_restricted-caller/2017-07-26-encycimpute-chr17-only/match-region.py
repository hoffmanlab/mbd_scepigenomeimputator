# match-region.py ##############################################################
# Attempts to match the peaks from a given run with those of the known
# encyclopedia and gives a list of all possible voters for that region

# PREAMBLE #####################################################################
import argparse
import os
import re

import pybedtools
from pybedtools import BedTool

WINDOW_SIZE = 100
#VOTER_WINDOW_SIZE = 5000 # this * WINDOW_SIZE = number of bp to consider when
                         # looking for matching peaks. 500k chosen b/c of
                         # Cicero paper (Pliner et al. preprint)
# BEDTOOLS WRAPPERS ############################################################
# Forward/reverse matching
class Encyclopedia:
    def __init__(self):
        self.cell_types = []
        self.cell_type_2_score = {}
        self.cell_type_2_dnase = {}
        self.score_bed = {}
        self.dnase_bed = {}

    def load_mapping(self, mapping_file):
        with open(mapping_file) as mapping:
            for line in mapping:
                line_split = line.strip().split()
                cell_type = line_split[0]
                self.cell_type_2_dnase[cell_type] = line_split[2]
                self.cell_type_2_score[cell_type] = line_split[3]
                self.cell_types.append(cell_type)

    def load_score_files(self, score_folder):
        """Loads the score files into memory
        load_mapping must be called first"""
        if len(self.cell_types) <= 0:
            raise Exception("No cell types loaded")
        for cell_type in self.cell_types:
            score_file = self.cell_type_2_score[cell_type] + ".score.bed.gz"
            file_path = os.path.join(score_folder, score_file)
            if os.path.isfile(file_path):
                self.cell_type_2_score[cell_type] = BedTool(file_path)
            else:
                raise Exception("Couldn't find score file for " + cell_type)

    def load_dnase_files(self, dnase_folder):
        """Loads the dnase files into memory.
        load_mapping must be called first"""
        if len(self.cell_types) <= 0:
            raise Exception("No cell types loaded")

        for cell_type in self.cell_types:
            file_path = os.path.join(dnase_folder, self.cell_type_2_dnase[cell_type])
            if os.path.isfile(file_path):
                self.cell_type_2_dnase[cell_type] = BedTool(file_path)
            else:
                raise Exception("Couldn't find dnase file for " + cell_type)

    def get_unmappable(self, run_fn, cell_type):
        """Tests the given peak against one of the score files"""
        # I suppose it doesn't actually matter what happens around edges
        # Since the annotation for those spots is always just "."
        if not os.path.isfile(run_fn):
            raise Exception("Could not open given file: " + run_fn)
        run_bt = BedTool(run_fn)

        # Each region counts as a match so long as all peaks VOTER_WINDOW_SIZE
        # away from run_bt overlaps with a peak from to_match and vice versa.
        # It's actually easier to just annotate regions where it's discordant
        to_match = self.cell_type_2_dnase[cell_type]
        first_intersect = run_bt.intersect(b=to_match, v=True, sorted=True)
        second_intersect = to_match.intersect(b=run_bt, v=True, sorted=True)

        # From a trial run, the first intersection generally has a high % of hits
        # So regions that are OK are those that aren't within VOTER_WINDOW_SIZE
        # of those within either intersection are OK to be voted in
        # Merge the two - these discordant reads together (plus VOTER_WINDOW_SIZE on
        # both ends) form the "unmappable" region for this cell-type and input
        all_intersect = first_intersect.cat(
            second_intersect,
            postmerge=True,
            force_truncate=True
            )
        #all_intersect = all_intersect.slop(
        #    genome="hg19",
        #    b=VOTER_WINDOW_SIZE
        #    ).remove_invalid().sort().merge()

        # Regions without an label in the annotation file are also unmappable
        #no_label = self.cell_type_2_score[cell_type].complement(genome='./hg19.chrom.sizes.sorted.txt')
        #all_intersect = all_intersect.cat(
        #    no_label,
        #    postmerge=True,
        #    force_truncate=True
        #    )

        # Last cleanup
        all_intersect = all_intersect.remove_invalid().sort()
        return(all_intersect)

    def get_all_unmappable(self, run_fn):
        """Grab all of the unmappable regions for a given input file"""
        unmappability = {}
        base_fn = os.path.splitext(run_fn)[0]
        for cell_type in self.cell_types:
            print(cell_type + "\n")
            unmappability[cell_type] = self.get_unmappable(run_fn, cell_type)
            unmappability[cell_type].saveas(base_fn + "-" + cell_type + ".bed.gz")
            pybedtools.cleanup()
        return unmappability

# MAIN #########################################################################
def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Attempts to match the peaks from a given run with those of the known
    encyclopedia and gives a list of all possible voters for that region
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('infile')   # input chromatin accessibility run
    parser.add_argument('mapping')  # mappable.txt
    parser.add_argument('score_folder') # mappable-score/
    parser.add_argument('dnase_folder') # mappable-data/
    parser.add_argument('cellline') # GM12878/
    return vars(parser.parse_args())

def create_encyclopedia(args):
    """Set up the encyclopedia object for further use
    Requires args: a dict with keys 'mapping', 'score_folder', 'dnase_folder'"""
    encyclopedia = Encyclopedia()
    encyclopedia.load_mapping(args["mapping"])
    encyclopedia.load_score_files(args["score_folder"])
    encyclopedia.load_dnase_files(args["dnase_folder"])
    return encyclopedia

def main():
    args = parse_args()
    encyclopedia = create_encyclopedia(args)
    test = encyclopedia.get_unmappable(args['infile'], args['cellline'])
    test.saveas('collision/' + os.path.splitext(args['infile'])[0] + "-" + args['cellline'] + '.bed.gz')
    #unmappable = encyclopedia.get_all_unmappable(args['infile'])

if __name__ == '__main__':
    main()
