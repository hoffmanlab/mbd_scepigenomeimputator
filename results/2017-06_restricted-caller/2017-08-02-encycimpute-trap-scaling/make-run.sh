#!/bin/bash
# This script creates a directory to run process-catalogue.sh with a different
# trapezoidal window size

# verbose failures
set -o nounset -o pipefail -o errexit


CATALOGUE_PREFIX=$1 #I.E. filename of the bed data file except for the .gz 
WINDOW_SIZE=$2  # e.g. 100

mkdir -p $WINDOW_SIZE
cd $WINDOW_SIZE
SCRIPTS=(aggregate.sh call.py collapse.sh collapse.py mergeSimilar.sh mergeSimilar.py process-catalogue.sh reduce.sh reduce.py split.sh)
for SCRIPT in "${SCRIPTS[@]}"; do
    if [ ! -e $SCRIPT ]; then
        ln -s ../scripts/$SCRIPT .
    fi
done

if [ ! -e score ]; then
    ln -s ../score .
fi

qsub process-catalogue.sh $CATALOGUE_PREFIX ../mappable.txt $WINDOW_SIZE
