# Trapezoidal Scaling analysis
This analysis was meant to look at the difference between different trapezoidal cutoffs to see their effect on the Jaccard/F1/Precision/Recall metrics. 

This is heavily based off of the leave-one-out analysis previously performed.
./validate-runs.sh 100 500 1000 5000 10000 20000 30000 40000 80000
