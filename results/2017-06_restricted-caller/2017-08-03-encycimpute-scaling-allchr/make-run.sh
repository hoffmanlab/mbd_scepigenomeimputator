#!/bin/bash
# This script creates a directory to run process-catalogue.sh with a different
# trapezoidal window size

# verbose failures
set -o nounset -o pipefail -o errexit


CATALOGUE_PREFIX="ENCFF000ACZ.bed"
WINDOW_SIZE=$1  # e.g. 100

mkdir $WINDOW_SIZE
cd $WINDOW_SIZE
ln -s ../scripts/aggregate.sh .
ln -s ../scripts/call.py .
ln -s ../scripts/collapse.sh .
ln -s ../scripts/collapse.py .
ln -s ../scripts/mergeSimilar.sh .
ln -s ../scripts/mergeSimilar.py .
ln -s ../scripts/process-catalogue.sh .
ln -s ../scripts/reduce.sh .
ln -s ../scripts/reduce.py .
ln -s ../scripts/split.sh .
ln -s ../score .

qsub process-catalogue.sh $CATALOGUE_PREFIX ../mappable.txt $WINDOW_SIZE
