#!/bin/bash
# Grabs the jaccard accuracy of all of the given runs
# Pass it the window sizes you used previously

# verbose failures
set -o nounset -o pipefail -o errexit

for i in $@; do
    cd $i
    mkdir -p validate
    Rscript validate-jaccard.R -l merged/ConfidentCalls_merged.bedGraph -t ../GM12891.remap.bed --genome=../hg19.chrom.sizes.sorted.txt -o validate/
    cd ../
done

for i in $@; do
    echo "$i: `grep "jaccard" $i/validate/_all_stats.tsv`"
done
