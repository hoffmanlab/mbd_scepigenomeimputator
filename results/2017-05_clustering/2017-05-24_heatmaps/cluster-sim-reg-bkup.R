# cluster-sim-reg.R ############################################################
# Creates a heatmap comparing similar segway notations between different samples

# PREAMBLE #####################################################################
library(pheatmap)
#library(ggplot2)
library(BoutrosLab.plotting.general)
library(Cairo)

# CONSTANTS ####################################################################
# I could load this from _mapping.txt, but I've already found all of those files
# to be the same  sooo
ANNOT_MAPPING <- c(
    ".",
    "Ctcf",
    "Elon",
    "Enh",
    "Gen",
    "Low",
    "Prom",
    "Quies",
    "Repr",
    "Tss"
    )

# HANDLE BATCH ARGUMENT ########################################################
KMEANS = "kmeans"
DIST.METHODS <- c("binary", "manhattan", "euclidean")
CLUST.METHODS <- c("diana", "ward.D2", KMEANS)
CLUST.METHODS.READABLE <- c("DIANA", "AGNES", "K-means")
#args <- commandArgs(trailingOnly = TRUE)
#idx <- as.numeric(args[1])
idx <- 6
dist.method <- DIST.METHODS[((idx - 1) %% 3) + 1]
clust.method <- CLUST.METHODS[((idx - 1) %/% 3) + 1]
clust.method.readable <- CLUST.METHODS.READABLE[((idx - 1) %/% 3) + 1]

# LOAD FILES ###################################################################
gm12878.files <- list.files("gm12878-final/", ".+\\.final\\.bed", full.names=TRUE)
h1esc.files <- list.files("h1esc-final/", ".+\\.final\\.bed", full.names=TRUE)
k562.files <- list.files("k562-final/", ".+\\.final\\.bed", full.names=TRUE) 
#hl60.files <- list.files("hl60-final/", ".+\\.final\\.bed", full.names=TRUE) 
tf1.files <- list.files("tf1-final/", ".+\\.final\\.bed", full.names=TRUE)   

all.files <- c(gm12878.files, h1esc.files, k562.files, tf1.files)
all.raw <- lapply(all.files,  read.table, stringsAsFactors=FALSE, sep="\t")
NUM.SEGMENTS <- nrow(all.raw[[1]])

# Grab the ATAC-seq peaks  annotation from each file
grab.atac.peak <- function(atac.file) {
    return(atac.file[,5])
}
all.peaks <- lapply(all.raw, grab.atac.peak)
all.peaks <- data.frame(matrix(unlist(all.peaks), nrow=NUM.SEGMENTS, byrow=T))
colnames(all.peaks) <- basename(all.files)

# Grab the the Segway-identified annotation from each file
# This should be the same for all files from the same cell line, so we'll only
# do it once per cel line
grab.seg.peak <- function(atac.file) {
    return(atac.file[,6])
}
all.annot <- lapply(all.raw, grab.seg.peak) # I lied - let's do it for errything
all.annot <- data.frame(matrix(unlist(all.annot), nrow=NUM.SEGMENTS, byrow=T))
colnames(all.annot) <- basename(all.files)
gm12878.annot <- all.annot[,1]
h1esc.annot <- all.annot[, length(gm12878.files) + 1]
k562.annot <- all.annot[, length(gm12878.files) + length(h1esc.files) + 1]

# REFINE DATA ##################################################################
# Removal of rows that were completely or mostly quiet 
# (this removes 90% of the rows)
mostly.quiet <- 10
dup.quiet <- apply(all.peaks, 1, sum) <= mostly.quiet
all.peaks <- all.peaks[!dup.quiet,]
dup.quiet <- apply(all.peaks, 1, sum) >= ncol(all.peaks)-mostly.quiet
all.peaks <- all.peaks[!dup.quiet,]

# This is going to get memory-intensive, really quickly
gc()
print(dim(all.peaks))

# CLUSTER ANALYSIS #############################################################
# Test: shuffle around the columns and see if the alg catches it
new.order <- sample(1:ncol(all.peaks))
bool.peaks <- all.peaks[,new.order]
bool.peaks <- bool.peaks > 0

# Attempt at doing some manual clustering of the columns
all.distances <- dist(
    bool.peaks,
    diag = FALSE,
    upper = FALSE,
    method="euclidean"
)
hc <- hclust(all.distances, method="ward.D2")
dend <- as.dendrogram(hc)
ord.hc <- order.dendrogram(dend)
bool.peaks <- bool.peaks[ord.hc,]

# PLOTTING #####################################################################
colour.scheme <- default.colours(number.of.colours = 4)
cell.lines <- c("gm12878", "h1esc", "k562", "tf1")

# Ok let's start with GM12878 vs K562, for funsies
# For each of the gm12878 Segway annotations...
for (chosen.gm12878.annot in unique(gm12878.annot)) {
    gm12878.peaks <- all.peaks[gm12878.annot == chosen.gm12878.annot,]
    # Match it up with one of K562's Segway annotations...
    for (chosen.k562.annot in unique(k562.annot)) {
        k562.peaks <- all.peaks[gm1288.annot == chosen.k562.annot,]

        # And now plot one vs the other to see their similarity... somehow
    }
}
#sample.cov.legend <- list(
#    legend = list(
#        colours = colour.scheme,
#        labels = cell.lines,
#        title = "Cell line"
#    )
#)
#
#covariates <- c(
#    rep(1, length(gm12878.files)),
#    rep(2, length(h1esc.files)),
#    rep(3, length(k562.files)),
#    rep(4, length(tf1.files))
#)
#covariates <- covariates[new.order]
#
#sample.covariate <- list(
#    rect = list(
#        col = "black",
#        fill = colour.scheme[covariates],
#        lwd = 0
#    )
#)
#
#plot.title <- paste(
#    "scATAC-seq 50k bins: ",
#    dist.method,
#    "/",
#    clust.method.readable
#)
#filename.prefix = paste(
#    format(Sys.time(), "%Y-%m-%d"),
#    dist.method,
#    clust.method,
#    "largeplot",
#    sep="-"
#)
#
#filename = paste(filename.prefix, "plot.png", sep="-")
#CairoPNG(filename, width=30000, height=1024)
#trellis.plot <- create.heatmap(
#    x = bool.peaks,
#    xaxis.lab = NULL,
#    yaxis.lab = NULL,
#    clustering.method = clust.method,
#    cluster.dimensions = 'rows',
#    main = plot.title,
#    main.cex = 3,
#    legend.title.cex = 2,
#    legend.cex = 2,
#    covariates = sample.covariate, 
#    covariate.legend = sample.cov.legend,
#    width=20,
#    legend.side = "right"
#)
#print(trellis.plot)
#dev.off()
## Aghhhh I don't know what I'm doing right now
