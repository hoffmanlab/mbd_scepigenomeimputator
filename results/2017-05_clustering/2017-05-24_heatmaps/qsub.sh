#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

Rscript cluster-sim-reg.R $SGE_TASK_ID
