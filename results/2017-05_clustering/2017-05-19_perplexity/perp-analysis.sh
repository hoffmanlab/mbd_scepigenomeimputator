#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

module load R;
Rscript perp-analysis.R $SGE_TASK_ID
