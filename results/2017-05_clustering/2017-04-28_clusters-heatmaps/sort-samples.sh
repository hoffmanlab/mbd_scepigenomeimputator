#!/bin/bash

# verbose failures
set -o nounset -o pipefail 
# -o errexit

mkdir bj-final
while read FILE_NAME; do
    SRR_BASE=${FILE_NAME%%.*}
    mv final/$SRR_BASE.final.bed bj-final/
done <BJ-samples.txt

#mkdir k562-final
#while read FILE_NAME; do
#    SRR_BASE=${FILE_NAME%%.*}
#    mv final/$SRR_BASE.final.bed k562-final/
#done <K562-samples.txt
#
## hl60 didn't complete successfully for some reason
#mkdir hl60-final
#while read FILE_NAME; do
#    SRR_BASE=${FILE_NAME%%.*}
#    mv final/$SRR_BASE.final.bed hl60-final/
#done <HL60-samples.txt

