qsub -N K562 -t 1-288 -tc 25 -q hoffmangroup -cwd -l h_vmem=8G -l mem_requested=8G windower.sh 50000 ATAC_bed K562-samples.txt
qsub -N HL60 -t 1-96 -tc 15 -q hoffmangroup -cwd -l h_vmem=8G -l mem_requested=8G windower.sh 50000 ATAC_bed HL60-samples.txt
qsub -N TF1 -t 1-96 -tc 15 -q hoffmangroup -cwd -l h_vmem=8G -l mem_requested=8G windower.sh 50000 ATAC_bed TF1-samples.txt
