qsub -N H1ESC -t 1-96 -tc 25 -q hoffmangroup -cwd -l h_vmem=8G -l mem_requested=8G windower.sh 50000 ATAC_bed h1esc-samples.txt
qsub -N GM18278 -t 1-384 -tc 25 -q hoffmangroup -cwd -l h_vmem=8G -l mem_requested=8G windower.sh 50000 ATAC_bed gm12878-samples.txt
