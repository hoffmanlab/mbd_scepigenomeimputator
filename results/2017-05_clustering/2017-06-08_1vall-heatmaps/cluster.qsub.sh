#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

module load R/3.3.0
Rscript cluster-batch.R $SGE_TASK_ID
