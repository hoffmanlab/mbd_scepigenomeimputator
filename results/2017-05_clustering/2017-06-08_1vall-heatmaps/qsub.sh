#!/bin/bash
#$ -N OnevAllPlot
#$ -q hoffmangroup
#$ -t 1-1000
#$ -tc 50
#$ -l h_vmem=32G
#$ -l mem_requested=32G
#$ -cwd


# verbose failures
set -o nounset -o pipefail -o errexit

Rscript cluster-sim-reg.R $SGE_TASK_ID
