#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

qsub -N OnevAllPlot -q hoffmangroup -t 1-10 -l h_vmem=32G -l mem_requested=32G -cwd qsub.sh
