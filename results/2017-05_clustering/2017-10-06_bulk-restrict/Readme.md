# Bulk-restrict: 

Single cell runs are noisy. Lupien suggested restricting the called peaks to ONLY regions available in a bulk ATAC/DNase-seq run. These peaks are from data/raw/2017-02-17_BulkATACSeq. 

Took the peaks from ENCODE (notes in the raw directory). Now we're going to see whether or not they still cluster 

## Results

### DIANA/Binary dendrogram
![dend](2017-10-11-binary-diana-1-dend.png)

### PCA clustering

![pca](2017-10-11-binary-diana-1-pca-clust.png)
![pca2](2017-10-11-binary-diana-1-pca-real.png)

### t-SNE clustering

![tsne1](2017-10-11-binary-diana-1-perp-30-agnes.png)
![tsne2](2017-10-11-binary-diana-1-perp-30-real.png)

### MCA analysis/clustering

Percentage variance explained requires over 100 dimensions to get at even half. 

The first two dimensions (pictured below) only explain 1.7% of the variance. 

Full details in MCA-eig.tsv

![mca1](2017-10-12-binary-diana-1-mca-real.png) 

Realized it looks really similar to PCA when flipped, see below: 

![mca2](2017-10-12-binary-diana-1-mca-real-flip.png)
![pca2](2017-10-11-binary-diana-1-pca-real.png) 

### Leave-10%-out

44 samples left out: 

![mca2](2017-10-12-binary-diana-10pct-pca-real.png)
