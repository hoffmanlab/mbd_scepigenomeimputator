# mca-all.R ###################################################################
# Uses MCA decomposition, leaving some out, to assess the clustering of
# the given scATAC-seq runs

# PREAMBLE ####################################################################
library(cluster)
library(ggplot2)
library(Cairo)
library(tsne)
library(FactoMineR)

KMEANS = "kmeans"
DIST.METHODS <- c("binary", "manhattan", "euclidean")
CLUST.METHODS <- c("diana", "ward.D2", KMEANS)
CLUST.METHODS.READABLE <- c("DIANA", "AGNES", "K-means")
CELL.LINES <- c("GM12878", "H1hESC", "K562")
MASTER.FILE <- "all-merged.bed"

set.seed(1)

# HANDLE BATCH ARGUMENT #######################################################
args <- commandArgs(trailingOnly = TRUE)
perplex <- as.numeric(args[1])
idx <- 1
dist.method <- DIST.METHODS[((idx - 1) %% length(DIST.METHODS)) + 1]
clust.method <- CLUST.METHODS[((idx - 1) %/% length(CLUST.METHODS)) + 1]
clust.method.readable <- CLUST.METHODS.READABLE[((idx - 1) %/% length(CLUST.METHODS)) + 1]

# LOAD FILES ##################################################################
master.peaks <- read.table(MASTER.FILE, stringsAsFactors=FALSE, sep="\t")
gm12878.files <- list.files("GM12878/", ".+\\.bed", full.names=TRUE)
h1esc.files <- list.files("H1hESC/", ".+\\.bed", full.names=TRUE)
k562.files <- list.files("K562/", ".+\\.bed", full.names=TRUE)

all.files <- gm12878.files
all.raw <- lapply(all.files,  read.table, stringsAsFactors=FALSE, sep="\t")
NUM.SEGMENTS <- nrow(master.peaks)

# Convert each row to peak format
convert.peaks <- function(atac.file) {
    return(apply(atac.file[,1:2], MARGIN=1, paste, collapse=":"))
}

# Grab which peaks from the master set appear in this set
grab.peak <- function(atac.file, master.file) {
    return(master.file %in% convert.peaks(atac.file))
}
all.peaks <- lapply(all.raw, grab.peak, convert.peaks(master.peaks))
all.peaks <- data.frame(matrix(unlist(all.peaks), nrow=NUM.SEGMENTS, byrow=T))
colnames(all.peaks) <- basename(all.files)

# REFINE DATA #################################################################
# Removal of rows that were completely or mostly quiet (this removes 90% of the rows)
mostly.quiet <- 50
dup.quiet <- apply(all.peaks, 1, sum) <= mostly.quiet
all.peaks <- all.peaks[!dup.quiet,]
dup.quiet <- apply(all.peaks, 1, sum) >= ncol(all.peaks)-mostly.quiet
all.peaks <- all.peaks[!dup.quiet,]

# CLUSTER ANALYSIS ############################################################
cell.lines <- c("gm12878", "h1esc", "k562")
colour.scheme <- rainbow(length(cell.lines))

sample.cov.legend <- list(
    legend = list(
        colours = colour.scheme,
        labels = cell.lines,
        title = "Cell line"
    )
)

# Test: shuffle around a the columns and see if the alg catches it
i <- 1
new.order <- sample(1:ncol(all.peaks))
bool.peaks <- all.peaks[,new.order]
bool.peaks <- bool.peaks > 0

distances <- dist(t(bool.peaks), diag = FALSE, upper = FALSE, method=dist.method)
distance.matrix <- as.matrix(distances)

covariates <- c(
    rep(1, length(gm12878.files)),
    rep(2, length(h1esc.files)),
    rep(3, length(k562.files))
)
names(covariates) <- colnames(all.peaks)
covariates <- covariates[new.order]

plot.title <- paste(
    "scATAC-seq truncated to bulk ATAC-Seq bins: ",
    dist.method,
    "/",
    clust.method.readable
    )
filename.prefix = paste(
    format(Sys.time(), "%Y-%m-%d"),
    dist.method,
    clust.method,
    i,
    sep="-"
    )

# Also print out a dendrogram
hc <- NULL
if (clust.method == "diana") {
    hc <- as.hclust(diana(distances))
} else if (clust.method == "ward.D2") {
    hc <- hclust(distances, clust.method);
} else if (clust.method == KMEANS) {
    # TODO: Implement k-means clustering
    stop("K-means not yet completed")
}

# function to get color labels
colLab <- function(n) {
    if (is.leaf(n)) {
        a <- attributes(n)
        labCol <- colour.scheme[covariates[which(colnames(bool.peaks) == a$label)]]
        attr(n, "nodePar") <- c(a$nodePar,
                                lab.col = labCol,
                                lab.bg = labCol,
                                pch = c(NA, 19),
                                col = labCol)
        attr(n, "label") <- " "   # HACKS
    }
    return(n)
}

# Cluster Validation: determine lowest k s.t. Normalized Mutual Information max
# Formula @ https://nlp.stanford.edu/IR-book/html/htmledition/evaluation-of-clustering-1.html
MAX_CLUST <- 20
MIN_CLUST <- 2
clusters <- cutree(hc, k = MIN_CLUST:MAX_CLUST)
num.in.class <- table(covariates)
N <- nrow(distance.matrix)
# Add in H(C), entropy from the number of classes
class.H <- 0
for (class.j in 1:max(covariates)) {
    prob.class.j <- num.in.class[class.j]/N
    class.H <- class.H - prob.class.j*log2(prob.class.j)
}
NMI <- 1:(MAX_CLUST-MIN_CLUST+1)
for (num.clust.test in 1:(MAX_CLUST-MIN_CLUST+1)) {
    I <- 0  # Mutual information
    H <- 0 # Normalized Entropy
    for (cluster.k in 1:(num.clust.test+MIN_CLUST-1)) {
        this.clust <- clusters[clusters[,num.clust.test] == cluster.k,]
        samples.called <- rownames(this.clust)
        covariates.called <- table(covariates[samples.called])
        num.in.cluster <- length(covariates.called)
        # H(Omega) = Entropy for the number of clusters
        H <- H - (num.in.cluster/N)*log2(num.in.cluster/N)
        for (class.j in 1:max(covariates)) {
            intersect <- covariates.called[class.j]
            if (!is.na(intersect)) {
                # Add mutual information from this cluster & class
                I <- I + (intersect/N)*log2(N*intersect/(num.in.cluster * num.in.class[class.j]))
            }
        }
    }
    H <- (H + class.H)/2
    NMI[num.clust.test] <- I/H
}

# Which number of clusters did the best?
best.num.clusters <- which.max(NMI)
best.num.clusters <- best.num.clusters + MIN_CLUST - 1

# Next, I want to see how well the dt clusters in t-SNE
cluster <- cutree(hc, k = best.num.clusters)
tmp <- tsne::tsne(distances, perplexity=perplex)
dt <- as.data.frame(tmp)
rownames(dt) <- colnames(bool.peaks)
colnames(dt) <- c("x", "y")
dt$real <- covariates[rownames(dt)]
dt$cluster <- cluster[rownames(dt)]
filename.base <- paste(filename.prefix, "perp", as.character(perplex), sep="-")
CairoPNG(paste(filename.base, "real.png", sep="-"))
p <- ggplot(dt, aes(x, y)) + geom_point(aes(colour = factor(real)))
p <- p + scale_color_hue(labels = CELL.LINES, name = "Cell line")
p <- p + labs(title = "Bulk-restricted scATAC-seq tSNE")
print(p)
dev.off()
CairoPNG(paste(filename.base, "agnes.png", sep="-"))
p <- ggplot(dt, aes(x, y)) + geom_point(aes(colour = factor(cluster)))
p <- p + scale_color_hue(labels = as.character(1:6), name = "Agnes Cluster #")
p <- p + labs(title = "Bulk-restricted scATAC-seq tSNE")
print(p)
#legend("topright", legend = as.character(1:6))
dev.off()

# Next, I want to see how well the dt clusters under PCA
pca <- prcomp(t(bool.peaks))
df <- as.data.frame(pca$x)
cov.fact <- factor(covariates[rownames(dt)])
levels(cov.fact) <- CELL.LINES
CairoPNG(paste(filename.prefix, "pca-clust.png", sep="-"))
p <- ggplot(df, aes(PC1, PC2)) + 
    geom_point(aes(colour = factor(clusters[rownames(dt),best.num.clusters-1])))
p <- p + labs(title = "Bulk-restricted scATAC-seq PCA", x = "PCA1", y = "PCA2")
p <- p + scale_color_hue(labels = as.character(1:best.num.clusters), name = "Agnes Cluster #", h = c(0, 180))
print(p)
dev.off()

# Plot it again, but coloured with the real category
CairoPNG(paste(filename.prefix, "pca-real.png", sep="-"))
p <- ggplot(df, aes(PC1, PC2)) + geom_point(aes(colour = factor(dt[rownames(df), "real"])))
p <- p + labs(title = "Bulk-restricted scATAC-seq PCA", x = "PCA1", y = "PCA2", h = c(180, 360))
p <- p + scale_color_hue(labels = CELL.LINES, name = "Cell line")
print(p)
dev.off()

# Now plot the dendrogram
color.dendro <- dendrapply(as.dendrogram(hc, hang = -1), colLab)
dend.filename = paste(filename.prefix, "dend.png", sep="-")
CairoPNG(dend.filename, width=1024, height=1024)
p <- plot(color.dendro, main = plot.title)
print(p)
print(legend("topleft", legend = cell.lines,
       fill = colour.scheme, border = colour.scheme, bty = "n"))
print(rect.hclust(hc, k = best.num.clusters))
dev.off()

### MCA Analysis ###############################################################
# Perform MCA on the scATAC-seq peaks
mca <- MCA(t(bool.peaks), ncp=40, graph=FALSE)
write.table(
    format(mca$eig, digits=2),
    "MCA-eig.tsv",
    sep="\t",
    row.names=FALSE,
    quote=FALSE
    )

# Plot the first two dimensions against one another
mca.dt <- as.data.frame(mca$ind$coord)
mca.dt$real <- factor(covariates[rownames(mca.dt)])
CairoPNG(paste(filename.prefix, "mca-real.png", sep="-"))
p <- ggplot(mca.dt, aes(`Dim 1`, `Dim 2`)) + geom_point(aes(colour = real))
p <- p + labs(title = "Bulk-restricted scATAC-seq MCA", x = "Dim 1", y = "Dim 2")
p <- p + scale_color_hue(labels = CELL.LINES, name = "Cell line", h = c(0, 180))
print(p)
dev.off()

# Same w/ clusters
mca.dt$clust <- factor(clusters[rownames(mca.dt), best.num.clusters-1])
CairoPNG(paste(filename.prefix, "mca-clust.png", sep="-"))
p <- ggplot(mca.dt, aes(`Dim 1`, `Dim 2`)) + geom_point(aes(colour = clust))
p <- p + labs(title = "Bulk-restricted scATAC-seq MCA", x = "Dim 1", y = "Dim 2")
p <- p + scale_color_hue(labels = as.character(1:best.num.clusters), name = "Agnes Cluster #", h = c(0, 180))
print(p)
dev.off()

# Noting that the plot looks identical to the PCA analysis,
# let's try rotating it to the same way
mca.dt$`Dim 1` <- -(mca.dt$`Dim 1`)
mca.dt$`Dim 2` <- -(mca.dt$`Dim 2`)
CairoPNG(paste(filename.prefix, "mca-real-flip.png", sep="-"))
p <- ggplot(mca.dt, aes(`Dim 1`, `Dim 2`)) + geom_point(aes(colour = real))
p <- p + labs(title = "Bulk-restricted scATAC-seq MCA", x = "-(Dim 1)", y = "-(Dim 2)")
p <- p + scale_color_hue(labels = CELL.LINES, name = "Cell line", h = c(180, 360))
print(p)
dev.off()
