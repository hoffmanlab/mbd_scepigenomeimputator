#!/usr/bin/env python

import argparse
import gzip
import re
import sys

import pandas as pd

# CONSTANTS ####################################################################
CHR=0
START=1
END=2
NONVOTER=3
VOTING_COLUMNS=slice(4,13)
CALL_COLUMN=14

# KERNEL #######################################################################
def additive_kernel(scores_to_alter, enhancer_score):
    """Adds a scaled probability of the input kernel"""
    return

def apply_kernel(original_score, site_score, row_id, column_num):
    original_score.loc[row_id, column_num] = 1
    original_score.loc[row_id, CALL_COLUMN] = column_num

# PARSE ARGS ###################################################################
def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Reduce columns in the given input file into a less memory
    intensive form
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('reduced_scores_collapsed.tsv')
    parser.add_argument('reprediction_scores.tsv')
    parser.add_argument('reproduction_column_change')
    parser.add_argument('out_file')
    return vars(parser.parse_args())

def slurp_safe(file):
    """Reads the input file into a list of strings (handles gz files)"""
    if file.endswith('.gz'):
        with gzip.open(file, 'rt') as infile:
            return infile.readlines()
    with open(file) as infile:
        return infile.readlines()

def repredict(scores, sites, column_num):
    """For each line of scores, fixes each site given in sites"""
    for idx in sites.T:
        # Parse out this site
        line = sites.iloc[idx]
        pos = line[0]
        #scores.loc[enh[0]][column_num] += enh[1]
        apply_kernel(scores, sites.iloc[idx], pos, column_num)

def generate_rownames_for_bed(bed):
    bed['rownames'] = bed.loc[:,CHR].str.cat(
        bed.loc[:,START].astype(str),
        sep=":"
        ).str.cat(
        bed.loc[:,END].astype(str),
        sep="-"
        )
    return bed.set_index('rownames')

# MAIN #########################################################################
def main():
    vars = parse_args()
    original = pd.read_table(vars['reduced_scores_collapsed.tsv'], header=None)
    reprediction = pd.read_table(vars['reprediction_scores.tsv'], header=None)

    # Massage the original scores into something easily searchable
    original = generate_rownames_for_bed(original)

    repredict(original, reprediction, vars['reproduction_column_change'])
    #print(original[1].split("\t")[VOTING_COLUMNS])

if __name__ == "__main__":
    sys.exit(main())

