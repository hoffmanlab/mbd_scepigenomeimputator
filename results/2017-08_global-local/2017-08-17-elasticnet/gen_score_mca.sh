#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l h_vmem=5G
#$ -l mem_requested=5G

# verbose failures
set -o nounset -o pipefail -o errexit

module load R/3.3.0

NUM_PC=$1

Rscript gen_score_mca.R --numpc $NUM_PC --output score_mca_${NUM_PC}.dat
