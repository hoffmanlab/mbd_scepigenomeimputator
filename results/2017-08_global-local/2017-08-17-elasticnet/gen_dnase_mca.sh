#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l h_vmem=32G
#$ -l mem_requested=32G

# verbose failures
set -o nounset -o pipefail -o errexit

module load R/3.3.0

NUM_PC=$1

Rscript gen_dnase_mca.R --numpc $NUM_PC --output dnase_mca_${NUM_PC}.dat
