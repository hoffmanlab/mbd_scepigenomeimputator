# Global analysis
The goal of the global analysis is use every dnase bin to predict every enhancer bin, via the following process: 

1. Reduce dimensionality on the enhancer bins through a [Multiple Correspondence Analysis](https://en.wikipedia.org/wiki/Multiple_correspondence_analysis)
2. Do the same for the dnase bins
3. Train an [elastic net](http://scikit-learn.org/stable/modules/linear_model.html#elastic-net) to map the dnase dimensions to the enhancer onnes

## 2017-08-15-global-patterns: Uniquely-identifying enhancer analysis 
I wanted to find what enhancer and DNase-seq bins uniquely identified every combination of input cell lines, for use in downstream analysis. 

### Enhancer bins to cell-lines it is unique to


For every score file we care about, I passed it through get_global_matches.sh in order to obtain only the label I cared about

~~~
get_global_matches.sh mappable-score enhancer out_dir
~~~

Where out_dir was enhancer_score for my run. I then ran it through merge_matches.sh:

~~~
merge_matches.sh IN_DIR OUT_BED_FILE OUT_ENTRIES_FILE
IN_DIR=$1           # enhancer_score/
OUT_BED_FILE=$2     # enhancers_merged.bed
OUT_ENTRIES_FILE=$3 # combinations_to_find.txt
DO_NOT_PREDICT=$4   # A549 (or 'A549|NB4' for multiple matches)
~~~

where IN_DIR was my enhancer_score from earlier, and I got enhancers_merged.bed (a bed file of every enhancer bin across all samples, and which cell lines they belonged to), and combinations_to_find (A txt file with every cell line combination I care about)

For reference, enhancers_merged.bed looks like the following:

~~~
chr17   0       99      FETAL_ADRENAL_GLAND
chr17   100     199     FETAL_ADRENAL_GLAND
chr17   500     599     IPS_DF_19.11_CELL_LINE
chr17   600     699     IPS_DF_19.11_CELL_LINE
chr17   700     799     FETAL_ADRENAL_GLAND|IPS_DF_19.11_CELL_LINE
chr17   800     899     FETAL_ADRENAL_GLAND
chr17   1700    1799    IPS_DF_19.11_CELL_LINE
chr17   1800    1899    IPS_DF_19.11_CELL_LINE
chr17   2000    2099    IPS_DF_6.9_CELL_LINE
chr17   2100    2199    IPS_DF_6.9_CELL_LINE
~~~

And combinations_to_find is a unique list of everything from that fourth column. I then grabbed the DNase-seq narrowPeaks that corresponded to all of those combinations via get_unique_accessibility.sh

~~~
# get_unique_accessibility.sh ##################################################
(Meant to be called from the cluster -- edit -t to fit how many lines there are in combinations_to_find.txt divided by 10)
COMBINATIONS_FILE=$1    # combinations_to_find.txt
MAPPING_FILE=$2         # mappable.txt            
DATA_DIR=$3             # binned-data/            

For reference, it relies on get_unique_accessibility.R, which looks like the following:
# get_unique_accessibility.R ###################################################
# Takes an entry from the combined cell lines of interest and obtains           
# peaks unique to that combination                                              
Usage: get_unique_accessibility.R [options]


Options:
        -m CHARACTER, --mappability=CHARACTER
                input mappability file

        -d CHARACTER, --datafiles=CHARACTER
                directory with binned reference chromatin accessibility files

        -c CHARACTER, --combination=CHARACTER
                input combined cell lines of interest file

        -i NUMERIC, --index=NUMERIC
                index of the combination file to process

        -o CHARACTER, --output=CHARACTER
                output png to write the final files to

        -h, --help
                Show this help message and exit
~~~

This grabbed the DNase peaks that were unique to each enhancer bin 

## 2017-08-17-elasticnet: Elastic net regularization (the global model)
I used the merged_enhancer.bed file generated above, along with a second run of get_global_matches.sh and merge_matches to get a DNase-seq merged_dnase.bed file (positive-dnase folder)

The majority of my analysis is contained in mca_analysis.R, with two helper scripts in gen_dnase_mca.R/.sh and gen_score_mca.R/sh

Both require you to qsub the .sh file with the number of dimensions you wish to keep in the output file. I am unaware of why you would want to have anything less than the maximum number of components, but the option was available from the MCA analysis library I am using (FactoMineR) so I left it exposed. 

Remember you cannot have more dimensions than the number of input cell lines 

## 2017-08-31_two-models: Combining global model with local model

By combining the results from the elastic net with the local restricted model used up until now, I aim to improve the results by re-weighting the probabilities assigned. 

Ignore repredict.py -- it is an old script 

~~~
Usage: repredict.R [options]


Options:
        -s CHARACTER, --score=CHARACTER
                input reduced_scores_collapsed.tsv file

        --reprediction=CHARACTER
                input reprediction scores file

        --column=INTEGER
                Column number that the reprediction scores file applies to

        -o CHARACTER, --output=CHARACTER
                output png to write the final files to

        -h, --help
                Show this help message and exit
~~~

Still need to tune how much it affects the probabilities
