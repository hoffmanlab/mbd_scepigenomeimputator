# Jaccard distances between ENCODE DNAse-Seq accessions

## Data source

For each scATAC-Seq dataset, I've taken two DNAse-Seq accessions from ENCODE to compare to: the "bed narrowPeak" file (which contains peaks) and the "bed broadPeak" file (which contains hotspots 

For the record, all of these files are Bed files with 1) varying numbers of columns, and 2) no column headers, meaning I was doing guesswork for anything beyond the
3 fixed columns (the additional information pointed me at source code, which was inscrutable). 

|          | scATAC-Seq                      |                                   |    | DNAse-Seq                       |                                  |
|----------|:-------------------------------:|:---------------------------------:|:--:|:-------------------------------:|:--------------------------------:|
|Cell Line | Data Type                       | File Name                         |    | Data Type                       | File Name                        |
|K562      | Peaks (500bp width)             | GSE65360_single-K562.peaks.bed    | vs | Peaks (150bp width)             | K562-ENCFF567LWS.bed             |
|          |                                 |                                   |    | Hotspots (287bp median width)   | K562-ENCFF223YQI.hotspots.bed    |
|hESC      | Peaks (500bp width)             | GSE65360_single-H1ESC.peaks.bed   | vs | Peaks (150bp width)             | hESC-ENCFF574LKL.bed             |
|          |                                 |                                   |    | Hotspots (246bp median width)   | hESC-ENCFF197IXZ.hotspots.bed    |
|GM12878   | Peaks (500bp width)             | GSE65360_single-GM12878.peaks.bed | vs | Peaks (150bp width)             | GM12878-ENCFF590NWM.bed          |
|          |                                 |                                   |    | Hotspots (246bp width)          | GM12878-ENCFF566BLY.hotspots.bed |

### Definitions
**Hotspots** = DNaseI sensitive zones identified using the HotSpot algorithm 

**Peaks** = DNaseI hypersensitive sites (DHSs) identified as signal peaks within FDR 1.0% hypersensitive zones 

## Analysis
Interestingly, the median length of the hotspots are ~250bps wide, which is half the width of the scATAC-Seq peaks. The peak files are a very defined 150bp wide peak
