#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

qsub -N BJ-downsample downsampling.sh BJ-samples-trunc.txt
qsub -N H1Esc-downsample downsampling.sh H1Esc-samples-trunc.txt
qsub -N K562-downsample downsampling.sh K562-samples-trunc.txt
qsub -N gm12878-downsample downsampling.sh gm12878-samples-trunc.txt
#qsub -N TF1-downsample downsampling.sh TF1-samples-trunc.txt
