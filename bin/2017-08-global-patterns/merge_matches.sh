#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

module load bedtools/2.23.0

IN_DIR=$1           # enhancer_score/
OUT_BED_FILE=$2     # enhancers_merged.bed
OUT_ENTRIES_FILE=$3 # combinations_to_find.txt
DO_NOT_PREDICT=$4   # A549 (or 'A549|NB4' for multiple matches)

TMP_FILE=${OUT_BED_FILE}.tmp

# Glob the enhancer location files together
gzip -cd $IN_DIR/*.bed.gz | sort -k1,1 -k2,2n >${TMP_FILE}

# Remove our cell lines to predict
echo grep -v -E $DO_NOT_PREDICT $TMP_FILE
grep -v -E "$DO_NOT_PREDICT" $TMP_FILE >${TMP_FILE}.2
mv ${TMP_FILE}.2 $TMP_FILE

# Merge identical entries together
bedtools merge -i ${TMP_FILE} -d -1 -c 5 -o distinct -delim "|" >${OUT_BED_FILE}
#rm ${TMP_FILE}
cut -f 4 ${OUT_BED_FILE} | sort | uniq >$OUT_ENTRIES_FILE
