#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -tc 50
#$ -o olog.txt
#$ -e elog.txt
#$ -l h_vmem=32G
#$ -l mem_requested=32G
##$ -t 2-1449

# Grabs the dnase peaks unique to the given cominations of cell lines in
# each index of combinations_to_find.txt

# verbose failures
set -o nounset -o pipefail -o errexit

COMBINATIONS_FILE=$1    # combinations_to_find.txt
MAPPING_FILE=$2         # mappable.txt
DATA_DIR=$3             # binned-data/

IDX_PER_TASK=10

MAX_IDX=`wc -l $COMBINATIONS_FILE | cut -d ' ' -f 1`
TSTART=$(( (SGE_TASK_ID - 1) * IDX_PER_TASK + 1))
TEND=$((SGE_TASK_ID * IDX_PER_TASK))

if [ "$TEND" -gt "$MAX_IDX" ]; then
    TEND=$MAX_IDX
fi

echo $TSTART $TEND
mkdir -p out/
set +e errexit
for idx in `seq $TSTART $TEND`; do
    Rscript get_unique_accessibility.R -m "$MAPPING_FILE" -d "$DATA_DIR" -c "$COMBINATIONS_FILE" -i $idx --output out/
done
