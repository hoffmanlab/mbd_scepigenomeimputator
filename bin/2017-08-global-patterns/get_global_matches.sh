#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
##$ -t 1-35

# verbose failures
set -o nounset -o pipefail -o errexit

SCORE_FILES=($1/*.bed.gz)   # mappable-score/
TO_SEARCH=$2                # "enhancer"
OUT_DIR=$3                  # enhancer-score/
DUMMY=""          # Whether or not to insert a dummy column
if [ $# -gt 3 ]; then
    ADDITIONAL_ARGS="${@:4}"
fi

IDX=$((SGE_TASK_ID - 1))

# Obtain the location of every enhancer bin
SCORE_FILE=${SCORE_FILES[$IDX]}
BASENAME=`basename "$SCORE_FILE"`
BASENAME=${BASENAME%%.gz}
TYPENAME=${BASENAME%%.bed}
TYPENAME=${TYPENAME%%_}

mkdir -p $OUT_DIR
gzip -cd "$SCORE_FILE" | grep -P $TO_SEARCH - | awk "{print \$0 \"\\t\" \"$TYPENAME\"}" - | gzip - >"$OUT_DIR/$BASENAME.gz"
