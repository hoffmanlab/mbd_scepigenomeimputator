#!/usr/bin/env python
# gen_dnase_mca.py generates an MCA object of the DNase bins in all of the cell
# lines identified in mappable.txt

import argparse
import gzip
import sys

import mca
import numpy as np
import sklearn.decomposition

# CONSTANTS ####################################################################
CHR = 0
START = 1
END = 2
CELL_LINES = 3
NUM_CELL_LINES = 35

# FILE HANDLING ################################################################
def slurp_file(filename, mode="rt"):
    """Slurps up a file safely, handling gzip"""
    if filename.endswith(".gz"):
        with gzip.open(filename, mode) as infile:
            return infile.readlines()

    with open(filename, mode) as infile:
        return infile.readlines()

def parse_bed(file):
    """Converts the given slurped bed file into a complete disjunctive table"""
    nrows = len(file)
    out = np.empty((nrows, 35), dtype=np.dtype('?'))
    bins = []
    cell_line_map = {}
    cell_lines = []
    seen_cell_line_combinations = {}
    bin_mapping = {}
    i = 0
    for line in file:
        split_line = line.strip().split("\t")
        cell_lines = split_line[CELL_LINES].split("|")

        # Replace the first three columns with a genomic location
        bin = "{}:{}-{}".format(
            split_line[CHR],
            split_line[START],
            split_line[END]
            )
        if split_line[CELL_LINES] in seen_cell_line_combinations:
            bin_mapping[bin] = seen_cell_line_combinations[split_line[CELL_LINES]]
            continue
        bins.append(bin)
        bin_mapping[bin] = i
        seen_cell_line_combinations[split_line[CELL_LINES]] = i

        # Figure out which cell lines are at this position
        for cell_line in cell_lines:
            if cell_line not in cell_line_map:
                cell_line_map[cell_line] = len(cell_line_map)
                cell_lines.append(cell_line)
            out[i, cell_line_map[cell_line]] = True

        i += 1
    out = out[:i]
    return (out, bins, cell_lines, bin_mapping)

def truth_to_cdt(truth):
    """Transforms the CDT into a form suitable for PCA analysis"""
    tcdt = np.empty(truth.shape, dtype=np.float32)
    n_categories = truth.shape[1]
    for (i, row) in enumerate(truth):
        factor = sum(row) / n_categories
        tcdt[i][row] = 1 / factor - 1
        tcdt[i][np.logical_not(row)] = -1
    return(tcdt)

# PARSE ARGS ###################################################################
def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Generates an MCA object of the DNase bins in all of the cell lines
    identified in mappable.txt
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('merged_enhancers.bed')
    parser.add_argument('merged_dnase.bed')
    parser.add_argument('mappable.txt')
    parser.add_argument('outfile')
    return vars(parser.parse_args())

# MAIN #########################################################################
def main():
    args = parse_args()
    (enh, enh_bin, enh_cell_lines, enh_map) = parse_bed(slurp_file(args['merged_enhancers.bed']))
    (dnase, dnase_bin, dnase_cell_lines, dnase_map) = parse_bed(slurp_file(args['merged_dnase.bed']))
    enh_tcdt = truth_to_cdt(enh)
    dnase_tcdt = truth_to_cdt(dnase)
    mapping = slurp_file(args['mappable.txt'])

if __name__ == '__main__':
    sys.exit(main())

