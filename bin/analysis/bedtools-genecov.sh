#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

ROOT_DIR=/mnt/work1/users/home2/fnguyen/working/Projects/mbd_scepigenomeimputator
for DNASE_FILE in $ROOT_DIR/data/2017-02-17/*.bed ; do
    DNASE_BN=`basename $DNASE_FILE`;
    qsub -q hoffmangroup -N $DNASE_BN $ROOT_DIR/bin/bedtools-genecov-single.sh $DNASE_FILE
done

#for ATAC_FILE in $ROOT_DIR/data/2017-01-23/scATAC-Seq/*.bed ; do
#    ATAC_BN=`basename $ATAC_FILE`;
#    qsub -q hoffmangroup -N $ATAC_BN $ROOT_DIR/bin/bedtools-genecov-single.sh $ATAC_FILE
#done
