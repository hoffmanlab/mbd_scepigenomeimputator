#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit
DNASE_FILE=$1
bedtools genomecov -i $DNASE_FILE -g hg19.chrom.sizes.txt -max 1 >${DNASE_FILE}_coverage.txt
