#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

IN_FILE=${1:-annotate_results/segway.bed.gz}
OUT_PREFIX=${2:-cohens_data/cohens_data}

module load R

# Window
bedtools intersect -loj -wb -a genome.100.sorted.bed -b $IN_FILE | cut -f1,2,3,7 | gzip - >segway.binned.bed.gz
DIR=$(dirname "$OUT_PREFIX")
mkdir -p $DIR
Rscript validate-cohenskappa.R --labels segway.binned.bed.gz --truth stomach_fantom5.bed.gz --n 3 --alternatemapping --output $OUT_PREFIX
