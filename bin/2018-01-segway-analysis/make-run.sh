#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

ln -s ../../../data/processed/2018-01-30_fantom5-allchr-permissive/binned-score-tpm-0.2-0index/ binned-score
ln -s ../../../data/processed/2017-12-11_sample-mapping-perceptron/genome.100.sorted.bed
ln -s ../2017-11-23_prep-data/genomedata-dnase
ln -s ../2018-02-26_merged-score/genomedata-score
cp ../2017-12-14_segway/.gitignore .
ln -s ../2018-02-26_merged-score/balanced-blacklist.bed.gz
ln -s ../../../data/processed/2017-05-23-realignment/hg19.trim.chrom.sizes
ln -s ../../../data/processed/2018-01-30_fantom5-allchr-permissive/mappable_wide.tsv
ln -s ../../../bin/2018-01-segway-analysis/recolour.R
cp ../2017-12-14_segway/seg_table.tsv .
ln -s ../2018-02-26_merged-score/stomach_fantom5.score.bed .
ln -s ../2018-02-26_merged-score/stomach_fantom5-nonquies.bed
#ln -s ../2017-11-23_prep-data/test-set.bed
#ln -s ../2017-11-23_prep-data/test-set-exclude.bed
cp ../2017-12-14_segway/tracks.txt .
cp ../2018-01-17_doubleweight/train.sh .
ln -s ../../../bin/2017-05-restriction-analysis/validate-cohenskappa.R .

