#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l h_vmem=16G
#$ -l mem_requested=16G

# verbose failures
set -o nounset -o pipefail -o errexit

module load MACS

IN_FILE=$1

# Call summits given <x> peaks?
KNOWN_FILES=""
NUM_FILES=0
IN_ROOT=${IN_FILE%%-trunc.txt}
echo $IN_ROOT
while read line; do
    BAM_FILE="bam/${line}.bam"
    if [ -e $BAM_FILE ]; then
        KNOWN_FILES="$KNOWN_FILES $BAM_FILE"
        NUM_FILES=`expr $NUM_FILES + 1`
        printf -v j "%03d" $NUM_FILES
        # Use p-value, then perform testing correction afterwards
        macs2 callpeak --nomodel --nolambda --keep-dup all --call-summits --seed 1 -p 0.99 -t $KNOWN_FILES -n $IN_ROOT-$j
    else
        echo "$BAM_FILE skipped: file not found"
    fi
done <$IN_FILE
