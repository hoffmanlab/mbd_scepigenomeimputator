#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

for i in *-trunc.txt; do
    BASE=${i%%-trunc.txt}
    qsub -N $BASE ./downsampling.sh $i
done
