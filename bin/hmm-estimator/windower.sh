#!/bin/bash
# Makes the input dataset for the learner from the output of bowtie-align.sh
# Requires: hg19.trim.chrom.sizes, segway_gm12878.bed, gm12878-samples.txt

# verbose failures
set -o nounset -o pipefail -o errexit

module load bedtools/2.26.0
module load R

# Read input arguments
WINDOW_SIZE=$1
ATAC_DIR=$2
SAMPLES_FILE=$3 # List of files within ATAC_DIR to process

#FILE_NUM=$(($SGE_TASK_ID - 1))
FILE_NAME=`sed "${SGE_TASK_ID}q;d" $SAMPLES_FILE`
ATAC_FILE=$ATAC_DIR/$FILE_NAME
ATAC_BASE=`basename $ATAC_FILE`
ATAC_BASE=${ATAC_BASE%%.*}

# Create a "window" bed file with ranges every $WINDOW_SIZE apart
if [ ! -e genome.${WINDOW_SIZE}.sorted.bed ]; then
    bedtools makewindows -g hg19.trim.chrom.sizes -w $WINDOW_SIZE >genome.${WINDOW_SIZE}.ranges.bed
    sortBed -i genome.${WINDOW_SIZE}.ranges.bed >genome.${WINDOW_SIZE}.sorted.bed
fi

# Ensure all bed files are sorted
if [ ! -e ${ATAC_BASE}.sorted.bed ]; then
    sortBed -i $ATAC_FILE >${ATAC_BASE}.sorted.bed
fi

# Grab the presence of ATAC-Seq peaks signal at this location
bedtools intersect -loj -wa -c -sorted -a genome.${WINDOW_SIZE}.sorted.bed -b ${ATAC_BASE}.sorted.bed >${ATAC_BASE}.score.bed

# Parse out the necessary columns
Rscript windower-1.R ${ATAC_BASE}.score.bed ${ATAC_BASE}.cut.bed

# Append the Segway annotation for this position
bedtools intersect -loj -wb -sorted -F 0.5 -a ${ATAC_BASE}.cut.bed -b segway_gm12878.bed >${ATAC_BASE}.seg.bed

# Obtain the most important Segway annotation
Rscript windower-2.R ${ATAC_BASE}.seg.bed ${ATAC_BASE}.final.bed

# Parse out the necessary columns for training
#Rscript windower.R ${ATAC_BASE}.seg.bed ${ATAC_BASE}.tsv
