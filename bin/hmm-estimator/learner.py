# learner.py ##################################################################
# This script trains a perceptron using seqlearn to obtain epigenomic state

# PREAMBLE ####################################################################
import numpy as np
#from hmmlearn import hmm
import seqlearn.hmm
import argparse
import pickle

# INPUT PARSING ###############################################################
def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(
        description = "Learn, from a set of input files, how to map to output"
        )
    parser.add_argument(
        "training_set",
        nargs = 1,
        help = "Location of training set"
        )
    return parser.parse_args()

def load_datasets(config):
    """
    Load the training dataset
    """
    datasets = {}
    datasets["train"] = np.loadtxt(config["training_set"][0])
    return datasets

def reorder_datasets(config, datasets):
    """
    Reorder the input datasets for input into sklearn
    config: dict of settings, loaded via safe_load_config()
    datasets: dict of the BedTools datasets, loaded via load_datasets()
    returns (X, Y), which are for svm to classify
    """
    # Transform X needs to become one-hot encoded for seqlearn
    X = datasets["train"][:,(3,4)].astype(int)
    Y = datasets["train"][:,5]
    return (X, Y)

def learn(config, X, Y):
    """
    Learn the input using a HMM, optimizing across possible parameters
    using grid_search
    X: Input predictors to give to svm.SVC().fit()
    Y: Output classifications to give to svm.SVC().fit()
    """
    clf = seqlearn.hmm.MultinomialHMM()
    clf.fit(X, Y, [1])
    return clf

def validate(config, model):
    pass

def run():
    config = vars(parse_args())
    #config = safe_load_config(args.config)
    print(config)
    datasets = load_datasets(config)
    print("Datasets loaded!")
    (X, Y) = reorder_datasets(config, datasets)
    print("Datasets prepped!")
    classifier = learn(config, X, Y)
    # uhh... pickle it or something
    with open('classifier.pickle', 'wb') as f:
        pickle.dump(classifier, f, pickle.HIGHEST_PROTOCOL)
    print("Done!")

if __name__ == '__main__':
    run()
