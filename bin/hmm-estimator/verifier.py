# verifier.py #################################################################
# hastily constructed verification script to be run on output of learner.py

# PREAMBLE ####################################################################
import numpy as np
#from hmmlearn import hmm
import seqlearn.hmm
import argparse
import pickle
def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(
        description = "Learn, from a set of input files, how to map to output"
        )
    parser.add_argument(
        "classifier",
        nargs = 1,
        help = "Classifier pickle, output by learner.py"
        )
    parser.add_argument(
        "verification",
        nargs = 1,
        help = "Verification set, usually a single scATAC-seq run parsed through windower.sh"
        )
    return parser.parse_args()

def main():
    args = vars(parse_args())
    # load classifier
    with open(args["classifier"][0], 'rb') as f:
        up = pickle.Unpickler(f)
        clf = up.load()

    # Load/reorder verification set
    verify = np.loadtxt(args["verification"][0])
    X = verify[:,(3,4)].astype(int)

# Verify and save output
    Y = clf.predict(X)
    np.savetxt(args["verification"][0] + ".reclass.txt", Y, fmt="%d", delimiter="\t")

if __name__ == "__main__":
    main()
