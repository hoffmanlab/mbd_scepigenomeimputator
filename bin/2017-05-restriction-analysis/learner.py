# learner.py ##################################################################
# Learn Segway annotations from scATAC-seq data
#
# Contruct the learning dataset by collecting:
# 1) Chromosome number
# 2) Distance from nearest ATAC-Seq peak
# 3) RNA-Seq signal strength
# 4) What can possibly be at a particular point (from known Segway annotations)

import argparse
import os
import re

import numpy as np
from sklearn import svm
from sklearn import grid_search
from sklearn.externals import joblib

# CONSTANTS ###################################################################

# INITIALIZATION ##############################################################
def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(
        description = "Learn Segway annotations from scATAC-seq data"
        )
    parser.add_argument(
        "training_set",
        nargs = 1,
        help = """Location of training set
        (format: chrX\tstart\tend, where each row is a scATAC-seq peak"""
        )
    parser.add_argument(
        "annot_likelihoods",
        nargs = 1,
        help = """Location of the annotation likelihoods
        (format: chrX\tstart\tend\tchance_of_annot_0\tchance_of_annot_1...)"""
        )
    return parser.parse_args()

def load_datasets(config):
    """
    Load the training dataset
    """
    datasets = {}
    datasets["train"] = np.loadtxt(config["training_set"][0])
    datasets["annot"] = np.loadtxt(config["annot_likelihoods"][0])
    return datasets

def reorder_datasets(config, datasets):
    """
    Reorder the input datasets for input into sklearn
    config: dict of settings, loaded via safe_load_config()
    datasets: dict of the BedTools datasets, loaded via load_datasets()
    returns (X, Y), which are for svm to classify
    """
    X1 = datasets["train"][:,(3,4)]
    X2 = datasets["annot"][:,:]
    X = np.concatenate((X1, X2), axis=1)
    Y = datasets["train"][:,5]
    return (X, Y)

def learn(config, X, Y):
    """
    Learn the input using an SVC, optimizing across possible parameters
    using grid_search
    X: Input predictors to give to svm.SVC().fit()
    Y: Output classifications to give to svm.SVC().fit()
    """
    param_grid = {
        "kernel": ['linear', 'rbf']
        }

    # In parallel, determine the best kernel type to use
    gs = grid_search.GridSearchCV(
        svm.SVC(), param_grid, n_jobs=4, cv=3)
    gs.fit(X, Y)
    print("Kernel used: {0}".format(gs.best_params_["kernel"]))

    # Use the best kernel type to create our classifier
    classifier = svm.SVC(gs.best_params_["kernel"])
    classifier.fit(X, Y)
    return classifier

def validate(config, model):
    pass

def run():
    config = vars(parse_args())
    print(config)
    datasets = load_datasets(config)
    print("Datasets loaded!")
    (X, Y) = reorder_datasets(config, datasets)
    print("Datasets prepped!")
    classifier = learn(config, X, Y)
    joblib.dump(classifier, 'classifier.pkl')
    print("Done!")

if __name__ == '__main__':
    run()

