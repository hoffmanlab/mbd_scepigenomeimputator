#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

mkdir -p split
TO_SPLIT=$1

cut -f 1,2,3,4 $TO_SPLIT >split/NonVoter.bedGraph
#cut -f 1,2,3,5 $TO_SPLIT >split/CageON.bedGraph
#cut -f 1,2,3,6 $TO_SPLIT >split/CageOFF.bedGraph
#cut -f 1,2,3,7 $TO_SPLIT >split/ConfidentCalls.bedGraph
cut -f 1,2,3,5 $TO_SPLIT >split/Quiescent.bedGraph
cut -f 1,2,3,6 $TO_SPLIT >split/ConstitutiveHet.bedGraph
cut -f 1,2,3,7 $TO_SPLIT >split/FacultativeHet.bedGraph
cut -f 1,2,3,8 $TO_SPLIT >split/Transcribed.bedGraph
cut -f 1,2,3,9 $TO_SPLIT >split/Promoter.bedGraph
cut -f 1,2,3,10 $TO_SPLIT >split/Enhancer.bedGraph
cut -f 1,2,3,11 $TO_SPLIT >split/RegPermissive.bedGraph
cut -f 1,2,3,12 $TO_SPLIT >split/Bivalent.bedGraph
cut -f 1,2,3,13 $TO_SPLIT >split/LowConfidence.bedGraph
cut -f 1,2,3,14 $TO_SPLIT >split/ConfidentCalls.bedGraph
