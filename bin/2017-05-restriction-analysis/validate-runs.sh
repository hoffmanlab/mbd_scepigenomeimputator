#!/bin/bash
# Grabs the jaccard accuracy of all of the given runs
# Pass it the window sizes you used previously

# verbose failures
set -o nounset -o pipefail -o errexit

TRUTH_FILE=$1
shift 

REAL_FILE=`realpath $TRUTH_FILE` #../GM12891.remap.bed

for i in $@; do
    cd $i
    mkdir -p validate
    #Rscript ../scripts/validate-jaccard.R -l merged/ConfidentCalls_merged.bedGraph -t ../GM12891.remap.bed --genome=../hg19.chrom.sizes.sorted.txt -o validate/
    Rscript ../scripts/validate-cohenskappa.R -l split/ConfidentCalls.bedGraph -t $REAL_FILE -o validate/
    cd ../
done

for i in $@; do
    echo "$i `cat $i/validate/_cohens_kappa.tsv`"
    grep "NonVoter" $i/split/ConfidentCalls.bedGraph | wc -l
done
