#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# This script is used to bin the given data files

# verbose failures
set -o nounset -o pipefail -o errexit

if [ "$#" -lt "3" ]; then
    echo "Usage: bin-all.sh out_dir genome_file [bed_files_to_bin]"
fi

OUT_DIR=$1
GENOME_FILE=$2
IN_DIR=($3/*.bed.gz)

set +o errexit
FILE_NUM=`expr \( $SGE_TASK_ID - 1 \)`
set -o errexit
i=${IN_DIR[$FILE_NUM]}
#for i in ${@:3}; do #./mappable-data/*.bed.gz; do
    BASE=`basename $i`
    echo $BASE
    bedtools intersect -sorted -wa -a $GENOME_FILE -b $i | gzip - >$OUT_DIR/${BASE}
#done
