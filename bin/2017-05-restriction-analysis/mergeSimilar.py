"""
Merges together identical features from an input bedGraph, given via split.sh
"""

import argparse
import gzip

# Column numbers for various parts of the bed file
CHR=0
START=1
END=2
NUM_VOTERS=3

# Various parts of the bed(Graph) header
BEDGRAPH_HEADER="track type={} name=\"{}\" description=\"{}\""
BEDGRAPH_OTHER="visibility=full autoScale=off height=30"
NONVOTER_NAME="NonVoter"
NONVOTER_LIMIT="viewlimits=0:34"
CALLS_NAME="ConfidentCalls"
LABEL_LIMIT="viewlimits=0:1"
COLOR_PREFIX="color={}"
ITEM_RGB="itemRgb=On"
COLORS={
    "CageON": "255,0,0",
    "CageOFF": "0,0,0",
    "Bivalent": "0,0,0",
    "ConstitutiveHet": "0,0,100",
    "Enhancer": "0,100,100",
    "FacultativeHet": "0,100,0",
    "LowConfidence": "100,100,0",
    "NonVoter": "100,0,0",
    "Promoter": "100,0,100",
    "Quiescent": "255,0,0",
    "RegPermissive": "0,255,0",
    "Transcribed": "0,0,255"
    }

def determine_header(label_type):
    header = []

    # Add in view limits
    if label_type == CALLS_NAME:
        header.append(BEDGRAPH_HEADER.format("bed", label_type, label_type))
        header.append(ITEM_RGB)
    else:
        header.append(BEDGRAPH_HEADER.format("bedGraph", label_type, label_type))

    if label_type == NONVOTER_NAME:
        header.append(NONVOTER_LIMIT)
    else:
        header.append(LABEL_LIMIT)

    # Add in the color scale
    if label_type != CALLS_NAME:
        header.append(COLOR_PREFIX.format(COLORS[label_type]))

    header.append(BEDGRAPH_OTHER)
    return " ".join(header)

def parse_args():
    parser = argparse.ArgumentParser(description="""
        Merge regions of the genome with the same value in the given column.
        Assumes all features are continuous, nonoverlapping bins of the genome
        """)
    parser.add_argument('infile', nargs=1)
    parser.add_argument(
        'label_type',
        nargs='?',
        help="""Annotation label of the bedfile to generate. If none is given,
        no header will be provided""")
    parser.add_argument(
        'col_to_merge',
        nargs='?',
        type=int,
        default=4,
        help="Column number to check for sameness when merging"
        )
    return vars(parser.parse_args())

def open_by_suffix(in_fn, mode='r'):
    if in_fn.endswith(".gz"):
        return gzip.open(in_fn, '{}t'.format(mode))
    else:
        return open(in_fn, mode)

def main():
    args = parse_args()

    to_merge = args['col_to_merge']
    with open_by_suffix(args['infile'][0]) as infile:
        if args['label_type'] is not None and args['label_type'] is not "":
            print(determine_header(args['label_type']))
        last_feature = infile.readline().strip().split()
        for line in infile:
            to_edit = line.strip().split()
            if last_feature[to_merge-1] != to_edit[to_merge-1] \
                or last_feature[CHR] != to_edit[CHR] \
                or abs(int(last_feature[END]) - int(to_edit[START])) > 1:
                # Print out the inferred, merged line
                print("\t".join(last_feature))
                last_feature = to_edit
            else:
                last_feature[END] = to_edit[END]
        print("\t".join(last_feature))
        print("\n")

if __name__ == '__main__':
    main()
