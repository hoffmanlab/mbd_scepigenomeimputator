#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

module load python3

# Cut down on the size of every bed file and convert to bedGraph
mkdir -p merged
for i in split/*.bedGraph; do
    BASE=`basename $i`
    BASE=${BASE%%.bedGraph}
    python3 mergeSimilar.py $i $BASE 4 >merged/${BASE}_merged.bedGraph
done

# Parse the special ConfidentCall files
python3 call.py split/ConfidentCalls.bedGraph ConfidentCalls.bedGraph
mv ConfidentCalls.bedGraph split/ConfidentCalls.bedGraph
python3 call.py merged/ConfidentCalls_merged.bedGraph ConfidentCalls.bedGraph
mv ConfidentCalls.bedGraph merged/ConfidentCalls_merged.bedGraph

# Merge all of the bedGraphs together
cat merged/*_merged.bedGraph >all.bedGraph

## Optional: Grab a reduced chunk at chr17
#sed '/chr.[^7]/ d' all.bedGraph >all_chr17.bedGraph
