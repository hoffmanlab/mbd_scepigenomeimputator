#!/bin/bash
#$ -N windowing
#$ -tc 50
##$ -t 2-33
#$ -q hoffmangroup
#$ -l h_vmem=16G
#$ -l mem_requested=16G
#$ -cwd
# Makes the input dataset for the learner from the output of bowtie-align.sh
# Requires: genome.${WINDOW_SIZE}.sorted.bed

# verbose failures
set -o nounset -o pipefail -o errexit

module load bedtools/2.26.0
module load R

# Read input arguments
WINDOW_SIZE=$1
SEG_DIR=$2
CUT_SEGMENTS=$3 # Usually 1,2,3,7
OUT_DIR=$4
ADDITIONAL_ARGS=""  # Given to bedtools intersect; can be -f 0.50 to ensure that the match must be at least 50%
                    # or -loj. Both of those are used for score files, data files use neither, fantom5 uses
                    # neither
if [ $# -gt 4 ]; then
    ADDITIONAL_ARGS="${@:5}"
fi
#SAMPLES_FILE=$3 # List of files within SEG_DIR to process

FILE_NUM=$(($SGE_TASK_ID - 1))
#FILE_NAME=`sed "${SGE_TASK_ID}q;d" $SAMPLES_FILE`
SEG_DIR=($SEG_DIR/*.bed.gz)
SEG_FILE=${SEG_DIR[$FILE_NUM]}
SEG_BASE=`basename $SEG_FILE`
SEG_BASE=${SEG_BASE%%.bed.gz}

## Create a "window" bed file with ranges every $WINDOW_SIZE apart
#if [ ! -e genome.${WINDOW_SIZE}.sorted.bed ]; then
#    bedtools makewindows -g hg19.trim.chrom.sizes -w $WINDOW_SIZE >genome.${WINDOW_SIZE}.ranges.bed
#    sortBed -i genome.${WINDOW_SIZE}.ranges.bed >genome.${WINDOW_SIZE}.sorted.tmp.bed
#    awk '{print $1 "\t" $2 "\t" ($3 - 1)}' genome.${WINDOW_SIZE}.sorted.tmp.bed >genome${WINDOW_SIZE}.sorted.bed
#    rm genome.${WINDOW_SIZE}.sorted.tmp.bed
#fi
#
# Ensure all bed files are sorted
mkdir -p sorted-bed
#if [ ! -e sorted-bed/${SEG_BASE}.sorted.bed ]; then
    sortBed -i $SEG_FILE >sorted-bed/${SEG_BASE}.sorted.bed
#fi

# Grab the presence of SEG-Seq peaks signal at this location
echo ${SEG_FILE}
#bedtools intersect -loj -wa -wb -sorted -a genome.${WINDOW_SIZE}.sorted.bed -b sorted-bed/${SEG_BASE}.sorted.bed | cut -f $CUT_SEGMENTS | gzip - >${SEG_BASE}.bed.gz
bedtools intersect -wa -sorted $ADDITIONAL_ARGS -a genome.${WINDOW_SIZE}.sorted.bed -b sorted-bed/${SEG_BASE}.sorted.bed | cut -f $CUT_SEGMENTS | gzip - >${OUT_DIR}/${SEG_BASE}.bed.gz
