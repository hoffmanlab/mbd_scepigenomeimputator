#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l h_vmem=16G
#$ -l mem_requested=16G
#$ -N rebuild-catalogue-
##$ -t 1-31

# verbose failures
set -o nounset -o pipefail -o errexit

# To be run after match-region.sh

MAPPABLE=$1     # mappable.txt
SCORE_DIR=$2    # mappable-score
GENOME_FILE=$3  # hg19.chrom.sizes
WINDOW_FILE=$4  # genome.100.sorted.bed
PREFIX=$5       # collision input file prefix

IN_DIR=(./collision/${PREFIX}*.bed.gz)
IN_FILE=${IN_DIR[$SGE_TASK_ID-1]}
IN_BASE=`basename $IN_FILE`
IN_BASE=${IN_BASE%%.bed.gz}

# Determine the cell line we're looking at
CELL_LINE=${IN_FILE##*-}
CELL_LINE=${CELL_LINE%%.bed.gz}

# Ok so we have the negatives... we need to negative-match the associated thingymobob with this
echo "$IN_FILE"
SCOREFN=`grep -P "^$CELL_LINE\t" $MAPPABLE | cut -f 4`
SCOREFILE=$SCORE_DIR/${SCOREFN}.score.bed.gz
#SCOREFILE=$SCORE_DIR/${CELL_LINE}.bed.gz

echo $IN_FILE   # For debugging
if [[ -e $SCOREFILE ]]; then
    mkdir -p score
    # Grab closeness to negative
    bedtools closest -a $WINDOW_FILE -b ${IN_FILE} -d -t first | cut -f 1,2,3,7 >${IN_BASE}_tmp.bed
    echo "bedtools closest complete"

    # Then we combine the distance-to-nearest-positive with the annotation for this region
    bedtools intersect -loj -wa -wb -f 0.51 -sorted -a ${IN_BASE}_tmp.bed -b ${SCOREFILE} | cut -f 1,2,3,4,8 | gzip >score/${IN_BASE}-score.bed.gz   # Rebuild catalogue from reduced info

    rm ${IN_BASE}_tmp.bed
else
    echo "Could not find matching file for: $CELL_LINE (Searching for $SCOREFILE based on mapping)"
    exit 1
fi
