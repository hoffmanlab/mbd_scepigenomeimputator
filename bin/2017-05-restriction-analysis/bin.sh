#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

REGEX='s/^\(chr[0-9XY]\+\t[0-9]\+\)[0-9]\{2\}\t\([0-9]\+\)[0-9]\{2\}/\100\t\200/' # Welcome to the escape party

# Make sure we have enough arguments to proceed, otherwise error out
if [ "$#" -eq 0 ]; then
    echo "bin.sh needs one or more files to bin"
    exit 1
fi

for IN_FILE in $@; do
    if file --mime-type "$IN_FILE" | grep -q gzip$; then
        gzip -cd $IN_FILE | sed $REGEX $IN_FILE - | gzip - >${IN_FILE}_tmp
        mv ${IN_FILE}_tmp $IN_FILE
    else
        sed -i $REGEX $IN_FILE
    fi
done
