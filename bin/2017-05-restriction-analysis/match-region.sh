#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l h_vmem=16G
#$ -l mem_requested=16G
#$ -N match-patient
##$ -t 1-31

# match-region.sh:
# Grabs mappable regions for a given input catalogue, for every cell line in mappable.txt
# Meant to be qsub'd from an SGE cluster
# qsub match-region.sh MAPPABLE_SAMPLE_FILE ENCYCLOPEDIA_DIR ACCESSIBILITY_FILES_DIR CATALOGUE_FILE
# MAPPABLE_SAMPLE_FILE: four column file of cell-line	ROADMAP-OR-ENCODE-FILE	score-file-name	chrom-accessibility-file-prefix
# ENCYCLOPEDIA_DIR: directory of the Segway encyclopedia .gz files (as .score.bed files output via windower.sh)
# ACCESSIBILITY_FILES_DIR: directory of the chromatin accessibility experiments (as .bed files)
# CATALOGUE_FILE: The file to compare against

# verbose failures
set -o nounset -o pipefail 

module load python3

IN_MAPPABLE=$1	#./mappable.txt
SCORE_DIR=$2	#../mappable-score/ 
DATA_DIR=$3	# ../mappable-data/
IN_CATALOGUE=$4 #Catalogue file

# Determine the name of the cell line
CELL_LINE=`sed "${SGE_TASK_ID}q;d" $IN_MAPPABLE | cut -f 1 -`

mkdir -p collision
python3 match-region.py $IN_CATALOGUE $IN_MAPPABLE $SCORE_DIR $DATA_DIR $CELL_LINE
