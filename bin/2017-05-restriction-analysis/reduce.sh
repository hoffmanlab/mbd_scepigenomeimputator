#!/bin/bash
#$ -N reducer
#$ -cwd
#$ -l h_vmem=32G
#$ -l mem_requested=32G
#$ -q hoffmangroup

# verbose failures
set -o nounset -o pipefail -o errexit

module load python3;
python3 reduce.py $1 $2
