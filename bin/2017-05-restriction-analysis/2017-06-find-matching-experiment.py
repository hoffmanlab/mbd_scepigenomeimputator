"""Finds a matching DNase-seq experiment for the given biosample-term-name
Meant to be used with python3.5+ for the pathlib library"""
import argparse
import json
import os
import pathlib
import re
import sys


def parse_args():
    """Grab command line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument('segway-folder', help='Location of Segway folder')
    parser.add_argument('encode-map', help='Encode mappability file')
    parser.add_argument('roadmap-folder', help='Roadmap Chromatin Accessibility folder')
    parser.add_argument(
        '--enable-dups',
        action = 'store_true',
        help = 'Whether or not to allow duplicate entries per cell type'
        )
    parser.add_argument(
        '--report-unmappable',
        action = 'store_true',
        help = 'Reports unmappable samples to stderr'
        )
    return parser.parse_args()

class DirectoryCheck:
    """Holds all of the files in a folder for easy name checking"""
    def __init__(self, folder):
        self.folder = folder
        self.files = [x.name for x in folder.iterdir() if x.is_file()]

    def is_here(self, biosample_term_name):
        """Determines if this folder has a sample matching the given
        biosample_term_name"""
        biosample_re = re.compile(
            re.escape(biosample_term_name),
            flags=re.IGNORECASE
            )
        matches = []
        for to_match in self.files:
            match = biosample_re.search(to_match)
            if match:
                matches.append(to_match)
        return matches

class EncodeCheck:
    def __init__(self, mapping_file):
        self.cell_line2file = {}
        self.parse_mapping(mapping_file)

    def parse_mapping(self, mapping_file):
        """Parses a mapping file"""
        with open(mapping_file) as mapping:
            for line in mapping:
                info = line.strip().split("\t")
                cell_lines = info[1].split("|")
                for cell_line in cell_lines:
                    if cell_line not in self.cell_line2file:
                        self.cell_line2file[cell_line] = []
                    self.cell_line2file[cell_line].append(info[2])

    def is_here(self, biosample_term_name):
        biosample_re = re.compile(
            re.escape(biosample_term_name),
            flags=re.IGNORECASE
            )
        for cell_line in self.cell_line2file.keys():
            match = biosample_re.match(cell_line)
            if match:
                return self.cell_line2file[cell_line]
        return []

def main():
    args = vars(parse_args())
    segway_folder = pathlib.Path(args["segway-folder"]).expanduser()
    roadmap_folder = pathlib.Path(args["roadmap-folder"]).expanduser()
    roadmap_check = DirectoryCheck(roadmap_folder)
    encode_check = EncodeCheck(args["encode-map"])
    files_to_match = [x for x in segway_folder.iterdir() if x.is_file()]
    for to_match in files_to_match:
    #onlyfiles = [f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f))]
        base_name_size = to_match.name.index('.')
        file_name = to_match.name[:base_name_size]
        enc_file = encode_check.is_here(file_name)
        road_file = roadmap_check.is_here(file_name)
        if args['enable_dups']:
            for file in enc_file:
                print("{0}\tENCODE\t{1}.bigBed".format(file_name, file))
            for file in road_file:
                print("{0}\tROADMAP\t{1}".format(file_name, file))
        else:
            if enc_file:
                print("{0}\tENCODE\t{1}.bigBed".format(file_name, enc_file[0]))
                continue
            if road_file:
                print("{0}\tROADMAP\t{1}".format(file_name, road_file[0]))
                continue
            if args['report_unmappable']:
                print("{0} could not be found".format(file_name),
                        file=sys.stderr)

if __name__ == "__main__":
    main()
