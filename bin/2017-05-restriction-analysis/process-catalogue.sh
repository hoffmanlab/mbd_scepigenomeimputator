#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l h_vmem=16G
#$ -l mem_requested=16G

# verbose failures
set -o nounset -o pipefail -o errexit

# Requires the following scripts:
#   aggregate.sh reduce.py collapse.py mergeSimilar.sh 
#   split.sh mergeSimilar.py mergeSimilar.sh
CATALOGUE_PREFIX=$1 #Catalogue-EP
MAPPING_FILE=$2 #mappable.txt
TRAPEZOIDAL_SCALING=5000
if [ "$#" -gt 2 ]; then
    TRAPEZOIDAL_SCALING=$3
fi

echo $CATALOGUE_PREFIX
echo $TRAPEZOIDAL_SCALING

module load python3;

#./aggregate.sh $CATALOGUE_PREFIX $MAPPING_FILE
python3 reduce.py reduced_scores.tsv reduced_scores_reduced.tsv
python3 collapse.py reduced_scores_reduced.tsv reduced_scores_collapsed.tsv --rectangularkernel --probability 0.01
./split.sh reduced_scores_collapsed.tsv
./mergeSimilar.sh
mv all.bedGraph all-rectangular.bedGraph

python3 collapse.py reduced_scores_reduced.tsv reduced_scores_collapsed.tsv --trapezoidalkernel --distancescaling $TRAPEZOIDAL_SCALING --probability 0.01
./split.sh reduced_scores_collapsed.tsv
./mergeSimilar.sh
mv all.bedGraph all-trapezoidal.bedGraph
gzip reduced_scores.tsv reduced_scores_reduced.tsv reduced_scores_collapsed.tsv
