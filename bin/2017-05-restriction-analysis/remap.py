#!/usr/bin/env python
# remap.py ####################################################################
# Reduces the columns in the given input file into a less memory-intensive form
# Can be applied to any segway encyclopedia file

import argparse
import gzip
import os
import re

MAPPING = {
    ".": "NonVoter",
    "Quiescent": "Quiescent",
    "Quies": "Quiescent",
    "ConstitutiveHet": "ConstitutiveHet",
    "FacultativeHet": "FacultativeHet",
    "Repr": "FacultativeHet",
    "Transcribed": "Transcribed",
    "Gen5'": "Transcribed",
    "Gen3'": "Transcribed",
    "Gen'": "Transcribed",
    "Tss": "Transcribed",
    "TssF": "Transcribed",
#    "Transcribe0": "Transcribed",
    "Promoter": "Promoter",
    "PromF": "Promoter",
    "PromP": "Promoter",
    "Enhancer": "Enhancer",
    "Enh": "Enhancer",
    "EnhF": "Enhancer",
    "EnhW": "Enhancer",
    "EnhWf": "Enhancer",
#    "Enha0": "Enhancer",
    "RegPermissive": "RegPermissive",
    "Bivalent": "Bivalent",
    "EnhPr": "Bivalent",
    "LowConfidence": "LowConfidence",
    "Low": "LowConfidence",
    "Ctcf": "LowConfidence",
    "CtcfO": "LowConfidence",
    "Elon": "LowConfidence",
    "ElonW": "LowConfidence"
    }

def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Reduce columns in the given input file into a less memory
    intensive form
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('infile', help='input tab delimited file')
    parser.add_argument('outfile', help='output remapped file')
    parser.add_argument(
        'to_remap',
        nargs='+',
        type=int,
        help='columns to remap (other columns will be left as is) \
        Column number is 1-based indexing'
        )
    return vars(parser.parse_args())

def remap_file(infile, outfile, cols_to_remap):
    # This function is in need of a refactor
    reducer = re.compile("\d+_([a-zA-Z']+)")
    reducer2 = re.compile("([a-zA-Z']+)\d+")
    for line in infile:
        to_edit = line.decode('ascii').strip().split()

        # Map columns listed as "to_remap"
        for col_id in range(0,len(to_edit)):
            if col_id+1 in cols_to_remap:
                annot = to_edit[col_id]
                new_annot = reducer.sub("\\1", annot)
                new_annot = reducer2.sub("\\1", new_annot)
                if new_annot not in MAPPING:
                    print("{0} column {1} has strange entry:{3}".format(line.strip(), col_id, new_annot))
                    outfile.write("0")
                    continue
                outfile.write(MAPPING[new_annot])
            else:
                outfile.write(to_edit[col_id])

            if col_id < len(to_edit)-1:
                outfile.write("\t")
        outfile.write("\n")

def main():
    args = parse_args()

    # Convert line by line
    # I wonder how to use gzip with the 'with' construct
    infile = None
    if args['infile'].endswith('.gz'):
        infile = gzip.open(args['infile'], 'rb')
    else:
        infile = open(args['infile'], 'r')

    if args['outfile'].endswith('.gz'):
        outfile = gzip.open(args['outfile'], 'wb')
    else:
        outfile = open(args['outfile'], 'w')

    try:
        remap_file(infile, outfile, args['to_remap'])
    finally:
        outfile.close()
        infile.close()

if __name__ == '__main__':
    main()

