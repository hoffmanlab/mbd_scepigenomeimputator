#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

qsub -N windowing -tc 50 -t 1-164 -q hoffmangroup -l h_vmem=8G -l mem_requested=8G -cwd windower.sh 100 data/segway-annot/
