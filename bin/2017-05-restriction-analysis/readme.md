# Encyclopedia Imputation Scripts
These scripts are designed to guess at the annotation of the given chromatin accessibility signal by comparing it to known datasets. 
The scripts are meant to be called from an SGE cluster via `qsub` on each. How to use them is listed below: 

# Necessary data
To use these scripts, you will need the [Segway encyclopedia cell type annotations](http://noble.gs.washington.edu/proj/encyclopedia/interpreted/). 
You will also require matched chromatin accessibility data for each cell type. This can be, for example, taken from [Roadmap Epigenomics data](http://www.genboree.org/EdaccData/Release-9/experiment-sample/Chromatin_Accessibility) or from [ENCODE experiments](https://www.encodeproject.org/search/?type=Experiment&replicates.library.biosample.biosample_type=immortalized+cell+line&y.limit=&assay_title=DNase-seq&limit=all) 

Mappability.txt is used to map between experiment, Segway annotation file, and cell line name. The file format is as follows: 
`Cell line name \t Data source (not used, for personal reference) \t Chromatin Accessibility narrowPeaks file \t Segway score file name minus the .bed.gz portion`

You will also need the chromosome sizes for your genome assembly of interest (E.G. [hg19.chrom.sizes](https://genome.ucsc.edu/goldenpath/help/hg19.chrom.sizes)) 

# 1) Processing the data
In order to process data consistently, the imputator requires all data to be binned in 100bp bins. For Segway encyclopedia cell type annotation files, this can be accomplished by providing windower.sh with the bin size and the directory of the Segway encyclopedia annotation files, like so: 

`qsub windower.sh 100 data/segway-annot` 

If the file genome.100.sorted.bed does not exist, run it first without using qsub, so that the genome file can be generated (It might throw an error due to not having SGE_TASK_ID set -- need to fix) 

For the chromatin accessibility files, use bin.sh with any number of chromatin accessibility files like so:

`bin.sh segway-data/*` 

Note that chromatin accessibility files are binned to the nearest upstream 100bp -- if you are working with different bin sizes this script will need to be updated.

# 2) Building the catalogue
The "catalogue" is used to refer to a collision map between the input sample and all of the samples within the encyclopedia. This can be accomplished by running the following two scripts: 

`qsub match-region.sh mappable.txt mappable-score/ mappable-data/` (Requires match-region.py to be in the current working directory, and at the moment has its input file hard-coded in "IN_CATLOGUES" -- need to fix) (This will create a directory called collision/ with the collision map) 
`qsub rebuild-catalogue.sh mappable.txt mappable-score/ hg19.chrom.sizes genome.100.sorted.bed` (genome.100.sorted.bed is generated from the windower.sh command, and will be named differently depending on your window size. hg19.chrom.sizes is generated as above) (This will create a directory called score/ with the annotation scores per cell line

# 3) Matching score
If you have run the above, you should have a directory with score/INPUT_FILE-CELL_LINE-score.bed.gz. If you ran match-region with more than one input file, you should move its score files to a score directory within a new directory prior to continuing, like so: 

`mkdir -p EP/score;
mv score/Catalogue-EP-* EP/score;
mkdir -p IM/score;
mv score/Catalogue-IM-* IM/score;` 

From the directory above either of the score files, all that is left is running process-catalogue.sh (with the files collapse.py collapse.sh reduce.py reduce.sh aggregate.sh mergeSimilar.py mergeSimilar.sh split.sh in the same directory), like so: 

`qsub process-catalogue.sh Catalogue-EP ../mappable.txt` (Catalogue-EP will be renamed with whatever the prefix is for the files inside of the score/ directory) 

This script will generated the merged/ directory with your final per-label analysis, all-rectangular.bedGraph with this scripts guess using a rectangular kernel, and all-trapezoidal.bedGraph with this scripts guess using a trapezoidal kernel.

# 4) Validation
Valdation is currently available via validate-jaccard.R, like so:

`mkdir validation; Rscript validate-jaccard.R -l merged/ -t GM12878.labels.bed --genome hg19.chrom.sizes -o validation/`. Output files include various statistics on the correctness of all of the labels, and a total precision/recall/jaccard/F1 score.
