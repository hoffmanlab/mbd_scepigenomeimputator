#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l h_vmem=16G
#$ -l mem_requested=16G

# verbose failures
set -o nounset -o pipefail -o errexit

module load python3

IN_DIR=($1/*.bed.gz)
CNHS_MAPPING=$2 # MappingTable.tsv
FACET_DIR=$3    # facet_expressed_enhancers/
OUT_DIR=$4

IDX=$(($SGE_TASK_ID - 1))
IN_SCORE=${IN_DIR[$IDX]}

# Determine whether or not there's a decent match for the input file
OUT_FN=`basename $IN_SCORE`
CNHS_ID=`echo $OUT_FN | cut -d- -f2`
MATCH=`grep "$CNHS_ID" $CNHS_MAPPING | cut -f4`

echo $MATCH
if [ ! -z "$MATCH" ]; then
    python3 reparse-score.py "$IN_SCORE" $FACET_DIR/$MATCH* "$OUT_DIR/$OUT_FN" --include-intersect
fi
