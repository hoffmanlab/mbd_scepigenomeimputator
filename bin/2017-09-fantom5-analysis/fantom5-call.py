#!/usr/bin/env python
# call.py ######################################################################
# Takes the ConfidentCalls bedGraph and colourizes it according to the call

import argparse

COLORS={
    "Bivalent": "0,0,0",
    "ConstitutiveHet": "0,0,100",
    "Enhancer": "0,100,100",
    "FacultativeHet": "0,100,0",
    "LowConfidence": "100,100,0",
    "NonVoter": "100,0,0",
    "Promoter": "100,0,100",
    "Quiescent": "255,0,0",
    "RegPermissive": "0,255,0",
    "Transcribed": "0,0,255"
    }

def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Takes the ConfidentCalls bedGraph and colourizes it according to the call
    Output will be a .bed file
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('infile')
    parser.add_argument('outfile')
    return vars(parser.parse_args())


def main():
    args = parse_args()

    # Convert line by line
    with open(args['infile']) as infile, open(args['outfile'], 'w') as outfile:
        #header = True
        for line in infile:
            #if header or line == "\n":
            #    outfile.write(line)
            #    header = False
            #    continue
            to_edit = line.strip().split()

            # Keep the genomic coords (chr, start, end)
            outfile.write("\t".join(to_edit[0:3]))

            # Convert this label into key stats
            name = "Quiescent"
            if int(to_edit[3]) > 0:
                name = "Transcribed"
            #name = NUM_TO_LABEL[int(to_edit[3])]
            color = COLORS[name]
            thick_start = to_edit[1]
            thick_end = to_edit[2]

            # Write all of the required elements of the .bed file
            outfile.write("\t" + "\t".join(
                [
                    name,
                    "1000",   # score
                    ".",    # strand
                    thick_start,
                    thick_end,
                    color
                ]))
            outfile.write("\n")

if __name__ == "__main__":
    main()
