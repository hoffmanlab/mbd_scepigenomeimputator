#!/bin/bash
#$ -q hoffmangroup
#$ -cwd
#$ -l h_vmem=8G
#$ -l mem_requested=8G

# verbose failures
set -o nounset -o pipefail -o errexit

module load python3

IN_DIR=(${1:-individual-runs}/*.gz)
MAPPABLE_FILE=${2:-mappable_long.tsv}
FACET_DIR=${3:-facet_expressed_enhancers/}    # facet_expressed_enhancers/
OUT_DIR=${4:-binned-score-raw-evenhundred/} # Unsure if I got the right one

IDX=$(($SGE_TASK_ID - 1))
IN_SCORE=${IN_DIR[$IDX]}
echo $IN_DIR

# Determine whether or not there's a decent match for the input file
OUT_FN=`basename $IN_SCORE`
echo $OUT_FN
CNHS_ID=`echo $OUT_FN | cut -d- -f2`
echo $CNHS_ID
MATCH=`grep -P "^${CNHS_ID}\t" $MAPPABLE_FILE | cut -f 5`
echo $MATCH

mkdir -p $OUT_DIR
echo python3 reparse-score.py "$IN_SCORE" "$FACET_DIR/$MATCH" "$OUT_DIR/$OUT_FN" --include_intersect
