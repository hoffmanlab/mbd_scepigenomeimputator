#!/usr/bin/env python
import argparse
import gzip
import sys

from pybedtools import BedTool

LABEL_COLUMN = -2
NUM_INTERSECT_COLUMN = -1  # Inserted during bedtools intersection

# PARSE ARGS ###################################################################
def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Takes a FANTOM5 score file from rebuild-catalogue.sh and converts it to
    a NoSignal/Enh/TSS format
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "in_file",
        nargs=1,
        help="""Score file from rebuild-catalogue.sh"""
        )
    parser.add_argument(
        "slidebase_file",
        nargs=1,
        help="""Slidebase file of enhancers for this cell type"""
        )
    parser.add_argument(
        "out_file",
        nargs=1,
        help="""Output file to write"""
        )
    parser.add_argument(
        "--include_intersect",
        action='store_true',
        help="""Number of columns to keep in the output file"""
        )
    return vars(parser.parse_args())


# MAIN #########################################################################
def open_by_suffix(in_fn, mode='r'):
    """
    Opens a file, returning its file handle.
    Handles .gzipped files via wrapping through gzip.open
    """
    if in_fn.endswith(".gz"):
        return gzip.open(in_fn, '{}t'.format(mode))
    return open(in_fn, mode)


def main():
    # Read input data
    args = parse_args()
    score_bed = BedTool(args["in_file"][0])
    slide_bed = BedTool(args["slidebase_file"][0])
    intersection = score_bed.intersect(b=slide_bed, c=True, stream=True)

    # Determine how many columns there are
    num_cols = 4
    if args["include_intersect"]:
        num_cols += 1

    with open_by_suffix(args["out_file"][0], 'w') as out_file:
        for line in intersection:
            cols = line.fields

            # Sanity check
            #if len(cols) != num_cols+1:
            #    #print("Wrong number of columns")
            #    print(len(cols))

            # Modify the last column according to whether or not it is >0
            if cols[LABEL_COLUMN] == "0" or cols[LABEL_COLUMN] == "Quiescent":
                cols[LABEL_COLUMN] = "1_Quiescent"
            elif int(cols[NUM_INTERSECT_COLUMN]) == 0:
                cols[LABEL_COLUMN] = "2_Transcribed"
            else:
                cols[LABEL_COLUMN] = "3_RegPermissive"

            # Print out the first four columns
            out_file.write("\t".join(cols[0:num_cols]) + "\n")

if __name__ == "__main__":
    sys.exit(main())
