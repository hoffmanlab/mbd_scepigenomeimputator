#!/usr/bin/env python
import argparse
import os
import os.path
import sys

import matplotlib.pyplot as plt
import numpy as np
import numpy.random
import pandas as pd
import pybedtools
import rpy2.robjects as ro
import sklearn
from rpy2.robjects.packages import importr
from rpy2.robjects import pandas2ri
from sklearn import svm, ensemble, neighbors, naive_bayes, semi_supervised
from sklearn import discriminant_analysis, linear_model, neural_network
from sklearn.externals import joblib
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import precision_recall_curve, average_precision_score
from sklearn.preprocessing import Imputer

import train

# PARSE ARGS ###################################################################
def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Predicts the annotation status of the given locations
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "score_folder",
        help="""Annotation folder"""
        )
    parser.add_argument(
        "varying_locations",
        help="""bed file of locations to learn from"""
        )
    parser.add_argument(
        "data_folder",
        help="""Folder of ATAC/DNase-seq runs"""
        )
    parser.add_argument(
        "validation",
        help="""Validation mapping file"""
        )
    parser.add_argument(
        "feature_width",
        type=int,
        default=20000,
        help="""Area (in bp) around features to consider"""
        )
    parser.add_argument(
        "input_model_fn",
        default="model.pkl",
        help="Input joblibbed model filename"
        )
    parser.add_argument(
        "--start-idx",
        type=int,
        default=0,
        help="""Which line of the mapping file to start operating on"""
        )
    parser.add_argument(
        "--end-idx",
        type=int,
        default=-1,
        help="""Which line of the mapping file to end operating on (-1 for all)"""
        )
    parser.add_argument(
        "--by",
        type=int,
        default=1,
        help="""How many lines of the mapping file to do at once"""
        )
    parser.add_argument(
        "--processing-dir",
        default='processed/',
        help="""Directory to place intermediate files in (enables caching)"""
        )
    parser.add_argument(
        "--output-folder",
        default="output/",
        help="""Output folder name"""
        )
    parser.add_argument(
        "--rna-dir",
        help="""Directory with RNA-seq files"""
        )
    parser.add_argument(
        "--exclude-cached-rna",
        help="""Ignore cached RNA values""",
        action='store_true'
        )
    return parser.parse_args()

def grabPos(feature):
    """
    Grabs the position of the input feature and puts it into a tuple
    """
    return (feature.chrom, feature.start, feature.stop)

def predict(model, X, cell_types, features_fn, out_folder):
    """
    Predict each region according to the input model
    """
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)

    # Run the validation
    num_obs = X.shape[0] / len(cell_types)
    score = model.predict_proba(X)

    # Read the features bed file into a numpy array
    features = pybedtools.BedTool(features_fn)
    positions = map(grabPos, features)
    positions = np.array(
            positions
            )
    positions = positions[0:num_obs,:]

    for (i, cell_type) in enumerate(cell_types):
        out_fn = os.path.join(out_folder, cell_type+".bed")
        # Append the position information
        score_info = score[i*num_obs:(i+1)*num_obs,:]
        predictions = np.concatenate(
                (positions, score_info),
                axis = 1
                )
        np.savetxt(out_fn, predictions, fmt="%s", delimiter="\t")

# MAIN #########################################################################
def main():
    args = parse_args()

    v_mapping = train.read_mapping(args.validation)
    (v_cell_types, v_chrom_access, v_rna_seq, v_scores) = train.read_data(
            v_mapping,
            args.data_folder,
            args.score_folder,
            args.varying_locations,
            rna_folder=args.rna_dir
            )
    (val_X, _) = train.construct_training(
            v_chrom_access,
            v_scores,
            100,
            args.feature_width,
            v_cell_types,
            "validation/",
            start_idx=args.start_idx,
            end_idx=args.end_idx,
            by=args.by,
            rna_seq=v_rna_seq
            )
    if args.exclude_cached_rna:
        val_X = train.exclude_rna(val_X)

    model = joblib.load(args.input_model_fn)
    predict(model, val_X, v_cell_types, args.varying_locations, args.output_folder)

if __name__ == "__main__":
    sys.exit(main())

