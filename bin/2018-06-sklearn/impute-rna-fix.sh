#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

# Quickly fix the missing chromosome position columns

ID=$((SGE_TASK_ID - 1))

IN_FOLDER=(${1:-imputed-evenhundred-rna}/*.tsv.gz)
BASE_FILE=(${2:-norm-evenhundred-data/astrocyte.bedGraph.gz}) # File to steal genome locations from

IN_FILE=${IN_FOLDER[$ID]}
TMP_FILE=${IN_FILE}.tmp

# Use process substitution to handle gzipped files
paste <(zcat $BASE_FILE) <(zcat $IN_FILE) | awk -F $"\t" 'BEGIN {OFS=FS} {print $1,$2,$3,$5}' >$TMP_FILE
