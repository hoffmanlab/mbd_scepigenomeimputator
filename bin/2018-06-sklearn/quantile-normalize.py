#!/usr/bin/env python
import argparse
import os.path
import sys

import numpy as np
import pandas as pd
from sklearn.preprocessing import quantile_transform


# PARSE ARGS ###################################################################
def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Quantile-normalizes the input bedGraphs.
    Assumes that the input bedGraphs have the same genomic locations in them,
    and differ only by score.
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "out_dir",
        help="output folder (will be created if it does not exist)"
        )
    parser.add_argument(
        "in_files",
        help="bedGraph files to normalize",
        nargs='+'
        )
    parser.add_argument(
        "--combined-track",
        help="Preparsed combined bedGraph file"
        )
    return parser.parse_args()


def load_signals(in_files):
    """
    Load the inpute signal files into two arrays:
    - one containing the (assumed) shared genomic coordinates for the data
    - one containing the signal at each genomic coordinate
    """
    chrom_coords = None
    signals = None
    for in_file in in_files:
        print(in_file)
        if chrom_coords is None:
            signal = np.genfromtxt(
                in_file,
                delimiter="\t",
                dtype=[('chr', 'S5'), ('start', int),
                       ('end', int), ('score', float)]
                )
            chrom_coords = signal[['chr', 'start', 'end']]
            signals = signal['score'][np.newaxis].T
        else:
            signal = np.loadtxt(
                in_file,
                delimiter="\t",
                usecols=3
                )
            signals = np.concatenate((signals, signal[np.newaxis].T), axis=1)
    return (chrom_coords, signals)


def load_combined_signals(in_file):
    """
    Read all of the signals from a combined file.
    load_signals kept crashing due to out of memory
    """

    # Load the file twice because I can't figure this out
    chrom_coords = np.genfromtxt(
        in_file,
        delimiter="\t",
        dtype=[('chr', 'S5'), ('start', int),
               ('end', int)],
        usecols=(0, 1, 2)
        )
    signals = np.loadtxt(
        in_file,
        delimiter="\t",
        usecols=(range(3, 25))
        )
    return (chrom_coords, signals)


def cache_data(chrom_coords, signals):
    """
    Please stop crashing due to out of memory
    """
    np.save("signals.pkl", signals)
    np.save("chrom_coords.pkl", chrom_coords)


def quantile_normalize(signals):
    """
    Quantile normalize the input array on axis 1
    """
    return(quantile_transform(signals, axis=1))


def quantileNormalize(df_input):
    """
    From https://github.com/ShawnLYU/Quantile_Normalize/blob/master/quantile_norm.py
    A more memory efficient quantile normalization technique
    """
    df = pd.DataFrame(df_input)

    # compute rank
    print("Computing rank")
    dic = {}
    for col in df:
        dic.update({col: sorted(df[col])})
    sorted_df = pd.DataFrame(dic)
    rank = sorted_df.mean(axis=1).tolist()

    # sort
    print("Sorting")
    for col in df:
        t = np.searchsorted(np.sort(df[col]), df[col])
        df[col] = [rank[i] for i in t]
    return df


def write_output(folder, filenames, position, signals):
    """
    Combine input signal+position, and output signals as a bedgraph.
    (Should be split into two functions)
    """
    print("Writing")
    out_df = pd.DataFrame(position)
    for (i, out_fn) in enumerate(filenames):
        out_fn = os.path.basename(out_fn)
        out_fn = os.path.join(folder, out_fn)
        out_df['signal'] = signals.iloc[:, i]
        out_df.to_csv(
                out_fn,
                sep="\t",
                header=False,
                index=False,
                compression="gzip"
                )


def main():
    """
    Run quantile normalization from the command line
    """
    args = parse_args()
    try:
        with open("signals.pkl.npy") as signalfile:
            signals = np.load(signalfile)
        with open("chrom_coords.pkl.npy") as chromfile:
            chrom_coords = np.load(chromfile)
        print("Using cached data")
    except IOError:  # File not found
        if args.combined_track:
            (chrom_coords, signals) = load_combined_signals(args.combined_track)
        else:
            (chrom_coords, signals) = load_signals(args.in_files)
        cache_data(chrom_coords, signals)

    signals = quantileNormalize(signals)

    write_output(args.out_dir, args.in_files, chrom_coords, signals)

if __name__ == "__main__":
    sys.exit(main())
