#!/usr/bin/env python
import argparse
import os
import sys

import matplotlib.pyplot as plt
import numpy as np
import sklearn
from sklearn import ensemble
from sklearn.externals import joblib


# PARSE ARGS ###################################################################
def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Get the feature importance of the given model (default rf-model.pkl)
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "--rna-seq",
        help="""Model was trained on RNA-seq data""",
        action='store_true'
        )
    parser.add_argument(
        "--model-fn",
        default='rf-model.pkl',
        help="Output joblibbed model filename"
        )
    parser.add_argument(
        "--feature-width",
        type=int,
        default=20300,
        help="""Area (in bp) around features to consider"""
        )
    parser.add_argument(
        "--output-fn",
        default="PRC.svg",
        help="""Output plot filename"""
        )
    parser.add_argument(
        "--n",
        type=int,
        default=20,
        help="""Number of features"""
        )
    return parser.parse_args()


def train(X, y):
    """
    Train the scikit-learn Random Forest model, using known best values
    """
    model = ensemble.RandomForestClassifier(
            max_features=None,
            n_estimators=100,
            criterion='entropy',
            max_depth=None,
            n_jobs=4
            )
    model.fit(X, y)
    return(model)


def scatterplot_feature_importance(model, feature_width, window_length, output_fn,
                            n=20, dpi=100, rna_seq=None):
    """
    Plots feature importance on a scatter plot
    """

    importances = model.feature_importances_

    # Used in plotting - the color for each bar and the position of the
    # latest rna-seq bar and chromatin accessibility bar
    colors = []

    # Convert to an upstream/downstream mapping
    total_length = importances.shape[0]
    window_length = total_length // 2   # Half the points are RNA-seq data
    features_downstream = window_length // 2
    for idx in range(len(importances)):
        # Everything past one window-length is RNA-seq data
        if idx < window_length:
            feature_type = "Accessibility"
            colors.append('#1f77b4')
        else:
            feature_type = "RNA-seq"
            colors.append('#b71f1f')
    np.savetxt('importances.tsv', np.argsort(importances), delimiter="\t")

    # Save index ranges for the chromatin accessibility and RNA-seq data
    chrom_access_idx = np.s_[0:window_length]
    rna_seq_idx = np.s_[(window_length+1):total_length]

    # Setup plot
    plt.switch_backend('agg')
    fig = plt.figure(figsize=(12, 12))
    font = {'size': 20}
    plt.rc('font', **font)
    #fig.subplots_adjust(bottom=0.5)

    plt_x = np.tile(np.arange(window_length), 2)
    chrom_scatter = plt.scatter(plt_x[chrom_access_idx], importances[chrom_access_idx], c=colors[chrom_access_idx])
    rna_scatter = plt.scatter(plt_x[rna_seq_idx], importances[rna_seq_idx], c=colors[rna_seq_idx])
    plt.xticks([0, window_length//2, window_length],
               [r"[$-20350$,$-20250$)", r"[$-50$, $50$)", r"[$20250$, $20350$)"])
               #rotation=45, rotation_mode='anchor', horizontalalignment='right')
    plt.ylim(top=0.02, bottom=0)
    plt.tick_params(axis='x', pad=20)
    plt.xlabel("Relative window to midpoint (bp)")
    plt.ylabel("Fraction mean decrease in impurity")
    plt.legend((chrom_scatter, rna_scatter), ('Chromatin accessibility', 'RNA-seq'))

    # Plot any points that are over 0.006
    for (scatter, indices) in [(chrom_scatter, chrom_access_idx),
                               (rna_scatter, rna_seq_idx)]:
        for i in range(indices.start, indices.stop):
            if importances[i] > 0.006:
                pos_idx = i % window_length
                print(pos_idx, features_downstream, total_length)
                window_start = int((pos_idx - features_downstream - 0.5) * 100)
                window_end = window_start + 100
                text = r"[${:d}$, ${:d}$)".format(window_start, window_end)
                x_jitter = [10, -10][pos_idx < features_downstream]
                halign = ['left','right'][pos_idx < features_downstream]
                valign = 'center'
                if 199 == pos_idx:
                    valign = 'top'
                elif 200 == pos_idx:
                    valign = 'bottom'
                weight = 'normal'
                if window_start == -50:
                    weight = 'black'
                x = plt_x[i] + x_jitter
                y = importances[i]
                plt.annotate(text,
                            (x, y),
                            horizontalalignment=halign,
                            verticalalignment=valign,
                            weight=weight
                            )
    fig1 = plt.gcf()
    fig1.savefig(output_fn, dpi=dpi)


def plot_feature_importance(model, feature_width, window_length, output_fn,
                            n=20, dpi=100, rna_seq=None):
    """
    Plots the n most important features from the given random forest model
    """
    importances = model.feature_importances_
    most_important = np.argsort(importances)[len(importances)-n:]

    str_idx = []

    # Used in plotting - the color for each bar and the position of the
    # latest rna-seq bar and chromatin accessibility bar
    colors = []
    rna_pos = None
    chrom_pos = None

    # Convert this into US or DS (UpStream & DownStream) mapping
    if rna_seq:
        rna_seq = importances.shape[0] // 2
        features_downstream = feature_width // window_length
        for (x, idx) in enumerate(most_important):
            if idx < rna_seq:
                feature_type = "Accessibility"
                colors.append('#1f77b4')
                chrom_pos = x
            else:
                feature_type = "RNA-seq"
                colors.append('#b71f1f')
                rna_pos = x
            pos_idx = idx % rna_seq
            window_start = int((pos_idx - features_downstream - 0.5) * window_length)
            window_end = window_start + window_length
            str_idx.append("{0} [{1:d}, {2:d})".format(feature_type, window_start, window_end))
            print(x, idx, importances[idx], feature_type, str_idx[-1])
        np.savetxt('test.tsv', np.argsort(importances), delimiter="\t")
    else:
        features_downstream = feature_width//window_length
        for idx in most_important:
            if idx < features_downstream:
                str_idx.append("Chrom Access @ {} Windows Before".format(features_downstream-idx))
            elif idx == features_downstream:
                str_idx.append("Chrom Access @ Containing window")
            else:
                str_idx.append("Chrom Access @ {} Windows After".format(idx-features_downstream))

    # Setup plot
    plt.switch_backend('agg')
    fig = plt.figure(figsize=(9, 9))
    font = {'size': 20}
    plt.rc('font', **font)
    fig.subplots_adjust(bottom=0.5)

    plt_x = np.arange(n)
    plt_x = np.flip(plt_x, 0)
    bars = plt.bar(plt_x, importances[most_important], color=colors)
    plt.xticks(plt_x, str_idx, rotation=45, rotation_mode='anchor', horizontalalignment='right')
    plt.xlabel("Feature: signal type and window (bp)")
    plt.ylabel("Fraction mean decrease in impurity")
    if chrom_pos is not None and rna_pos is not None:
        plt.legend((bars[chrom_pos], bars[rna_pos]), ('Chromatin accessibility', 'RNA-seq'))
    fig1 = plt.gcf()
    fig1.savefig(output_fn, dpi=dpi)


def flip_obs(X, y):
    # Double the size of X, but flip its observations horizontally
    X = np.concatenate((X, np.flip(X, axis=1)), axis=0)
    y = np.concatenate((y, y), axis=0)
    return(X, y)


# JOBLIB FREAKS OUT IF THIS ISN'T HERE FOR SOME REASON
def custom_scoring(estimator, X, y):
    """
    Score the estimator based on precision at 50% FDR.
    Used as the argument to scoring= in GridSearchCV
    """
    y_proba = estimator.predict_proba(X)

    # Find the point at which we have 50% FDR for the nonzero classes
    # Assume a single inflection point, going backwards
    (precision, recall, thres) = sklearn.metrics.precision_recall_curve(y, y_proba[:, 1])
    inflection = np.where(precision > 0.5)[0][0]    # First [0] is to get at the
    if inflection:
        return(recall[inflection])
    else:
        return(0)


# MAIN #########################################################################
def main():
    args = parse_args()

    if os.path.isfile(args.model_fn):
        model = joblib.load(args.model_fn)

        # If this is a GridSearchCV object, get the actual estimator
        if hasattr(model, "best_estimator_"):
            model = model.best_estimator_   # .feature_importances_
    else:
        sys.exit("ERROR: No model found")

    #plot_feature_importance(model, args.feature_width, 100, args.output_fn,
    #                        n=args.n, rna_seq=args.rna_seq)
    scatterplot_feature_importance(model, args.feature_width, 100, args.output_fn,
                            n=args.n, rna_seq=args.rna_seq)

if __name__ == "__main__":
    sys.exit(main())

