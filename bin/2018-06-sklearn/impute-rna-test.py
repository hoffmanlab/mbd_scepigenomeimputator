#!/usr/bin/env python
import argparse
import os
import sys

import numpy as np
import pybedtools

import train

# PARSE ARGS ###################################################################
def parse_args():
    """Parse command-line arguments via argparse. Returns dict of args"""
    description = """
    Impute data tracks from the other cell types.
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "data_folder",
        help="""Folder of sequencing runs (must be pre-binned to same length)"""
        )
    parser.add_argument(
        "mapping",
        help="""Mapping file"""
        )
    parser.add_argument(
        "output_folder",
        help="Folder to output imputed data into"
        )
    return parser.parse_args()

def impute_missing(X, cell_types, save_imputed=None):
    # For each row in X, grab the non-NA values, and construct a Gaussian model
    mean = np.nanmean(X, axis=0)
    std = np.nanstd(X, axis=0)

    # Determine the indices of the NA values
    missing = np.nonzero(np.isnan(X))
    repl = np.zeros(len(missing[1]))
    for (i, col_id) in np.ndenumerate(missing[1]):
        # Grab a point from a Gaussian N(mean, std) given the other points
        # at this location. Truncate negative values to zero.
        repl[i] = max(np.random.normal(loc=mean[col_id], scale=std[col_id]),0)

    X[missing] = repl

    if save_imputed:
        # Determine which cell lines to save
        to_save = np.unique(missing[0])
        to_save = to_save[to_save < len(cell_types)]
        first_rna = missing[1].min()
        last_rna = missing[1].max()

        # Write the imputed cell types to the given directory
        if not os.path.exists(save_imputed):
            os.makedirs(save_imputed)

        for row in to_save:
            out_fn = os.path.join(save_imputed, cell_types[row] + ".tsv.gz")
            np.savetxt(out_fn,
                    X[row, first_rna:last_rna],
                    delimiter="\t",
                    fmt="%5f"
                    )

    return X

def grabScore(feature):
    return float(feature.name)

def load_data(mapping, data_folder, suffix=".bedGraph.gz"):
    """
    Load each data file from data_folder using pyBedTools
    There is an assumption that each file in data_folder has the same
    number of lines. If not, this will raise an exception
    """
    seq_data = None
    to_redo = []
    cell_types = []
    for (i, sample) in mapping.iterrows():
        cell_type = sample[0]
        cell_types.append(cell_type)
        print(cell_type)

        # Determine if this rna file exists
        data_fn = os.path.join(data_folder, cell_type + suffix)
        try:
            data_bt = np.loadtxt(data_fn, delimiter="\t", usecols=3)
        except:
            # If not, we redo seq_data
            if seq_data is None:
                to_redo.append(i)
            else:
                seq_data[i,:] = np.tile(np.nan, seq_data.shape[1])
            continue

        if seq_data is None:
            seq_data = np.zeros((len(mapping), data_bt.shape[0]))

        # Populate the score column
        seq_data[i,:] = data_bt

    for i in to_redo:
        seq_data[i,:] = np.tile(np.nan, seq_data.shape[1])
    return(seq_data, cell_types)

# MAIN #########################################################################
def main():
    args = parse_args()
    mapping = train.read_mapping(args.mapping)
    (X, cell_types) = load_data(mapping, args.data_folder)
    X = impute_missing(X, cell_types, args.output_folder)

if __name__ == "__main__":
    sys.exit(main())

