#!/bin/bash
#$ -q hoffmangroup
#$ -cwd

# verbose failures
set -o nounset -o pipefail -o errexit

MAPPABLE_FILE=${1:-"mappable_wide_noheader.tsv"}
SCORE_DIR=${2:-"binned-score/"}
ALL_FACET_FN=${3:-"all-facet-merged.bed"}

SCORE_FN_ROOT=(`cut -f4 $MAPPABLE_FILE`)
SCORE_FN=( "${SCORE_FN_ROOT[@]/#/$SCORE_DIR/}" ) # Prepend the score_dir to every score_fn
SCORE_FN=( "${SCORE_FN[@]/%/.bed.gz}" ) # Append the score file extension to every score_fn

mkdir -p voteratios
bedtools intersect -wb -sorted -a $ALL_FACET_FN -b ${SCORE_FN[@]} >voteratios/all.bed
sort -k1,1 -k2,2n voteratios/all.bed >voteratios/all-sorted.bed
bedtools merge -i voteratios/all-sorted.bed -d -1 -c 16 -o count_distinct >voteratios/all-merged.bed
grep -P "[^1]$" voteratios/all-merged.bed | cut -f 1-3 >varying_locations.bed
