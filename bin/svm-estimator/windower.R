# windower.R ##################################################################
# Reads the output of windower.sh, which is a combined 
# scATAC-Seq/scRNA-Seq/Segway annotation file, and parse out the necessary
# columns to be parsed by sklearn

# PREAMBLE ####################################################################
library(optparse)

COLS_TO_KEEP <- c(1,2,3,10,14,18)
CHR_COL <- 1
RNA_COL <- 14
RNA_NA_VAL <- -1
SEGWAY_COL <- 18

SEGWAY_MAPPING <- 1:26
names(SEGWAY_MAPPING) <- c(
    ".",
    "Ctcf",
    "CtcfO",
    "Elon1",
    "Elon2",
    "ElonW",
    "Enh",
    "EnhF",
    "EnhWf",
    "Gen3'",
    "Gen5'",
    "Low1",
    "Low2",
    "Low3",
    "Low4",
    "Low5",
    "Low6",
    "PromF",
    "Quies",
    "Repr1",
    "Repr2",
    "Repr3",
    "Repr4",
    "Repr5",
    "Tss",
    "TssF"
    )

# READ COMMAND-LINE ARGUMENTS #################################################
# get command line options, if help option encountered print help and exit,
opt_parser <- OptionParser(
    usage = "%prog unparsed_window_file out_file"
    )
opt <- parse_args(opt_parser, positional_arguments = 2)
file_unparsed <- opt$args[1]
file_out <- opt$args[2]

# TRUNCATE INPUT FILES ########################################################
# Read input file
parsed <- read.table(file_unparsed, stringsAsFactors = FALSE)

# Preprocess necessary columns
parsed[[CHR_COL]] <- gsub("chr(.+)", parsed[[CHR_COL]], rep = "\\1")
parsed[[CHR_COL]] <- gsub("X", parsed[[CHR_COL]], rep = "24")
parsed[parsed[[RNA_COL]] == ".", RNA_COL] <- RNA_NA_VAL
parsed[[SEGWAY_COL]] <- SEGWAY_MAPPING[parsed[[SEGWAY_COL]]]
parsed <- parsed[,COLS_TO_KEEP]
write.table(
    format(parsed, scientific = FALSE),
    file = file_out,
    quote = FALSE,
    row.names = FALSE,
    col.names = FALSE
    )
