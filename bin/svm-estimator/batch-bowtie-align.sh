#!/bin/bash

# verbose failures
set -o nounset -o pipefail -o errexit

IN_DIR=$1
OUT_DIR=$(cd "$2"; pwd)
START=$3
END=$4
IDX=0;

for file in $IN_DIR/SRR*.sra; do
    filename=$(basename "$file")
    filepath="$(cd "$(dirname "$file")"; pwd)/$filename"
    qsub -q hoffmangroup -N $filename -cwd ./bowtie-align.sh $filepath $OUT_DIR
done
