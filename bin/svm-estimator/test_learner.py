"""
Unit test Module for learner.py
"""

# PREAMBLE ####################################################################
import pytest

import learner

# UNIT TESTS ##################################################################
def test_config():
    with pytest.raises(ValueError):
        learner.safe_load_config("")
    assert isinstance(learner.safe_load_config(), dict)

#class TestLearner(unittest.TestCase):
#    def test_config_load(self):
#        with self.assertRaises(ValueError):
#            learner.safe_load_config("")
#
#if __name__ == '__main__':
#    unittest.main()
